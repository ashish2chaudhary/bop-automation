<<<<<<< HEAD
package com.paytm.framework.ui.element;

import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.core.ExecutionConfig;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class Table extends UIElement {

    @Deprecated
    public Table(By by, String pageName) {
        super(by, pageName);
    }

    public Table(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public int getBodyRowCount() {
        return getRows("BODY").size();
    }

    public int getColumnCount() {
        return getWrappedElement().findElements(By.cssSelector("th")).size();
    }

    public WebElement getCellAtIndex(int rowIdx, int colIdx, String section) { 
        WebElement row = getRows(section).get(rowIdx);

        if(section.equalsIgnoreCase("HEADER")) {        
        	return row.findElements(By.tagName("th")).get(colIdx);
        } 
        else if (section.equalsIgnoreCase("BODY")) {
        	return row.findElements(By.tagName("td")).get(colIdx);
        } 
        else {
            final String error = String.format("Could not find cell at row: %s column: %s", rowIdx, colIdx);
            throw new RuntimeException(error);
        }
    }

    public String getCellTextAtIndex(int rowIdx, int colIdx, String section) {  
        return getCellAtIndex(rowIdx, colIdx, section).getText();
    }
    
    public void clickCell(int rowIdx, int colIdx, String section) {    	
    	Boolean found = false;
		String stackTrace = "Unable to click";
	    Reporter.log("<br>Click on element [" + rowIdx + "] [" + colIdx + "] in the table section [" + section + "]");
	    long oneMinuteTick = System.currentTimeMillis() + ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME*1000;
		while(System.currentTimeMillis() <= oneMinuteTick) { 
			try {		    	
				getCellAtIndex(rowIdx, colIdx, section).click();
			   	found = true;
			   	break;
			} catch (WebDriverException e) {
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);
			    pw.flush();
			    stackTrace = sw.toString();
			  } 		  
		}
		if(!found) {
			Reporter.log("<br>Click on element [" + rowIdx + "] [" + colIdx + "] in the table section [" + section + "]");
			throw new WebDriverException(stackTrace);
		}	
    }
    
    public void clickCell(int rowIdx, String colHeader, String section) {
    	Boolean found = false;
		String stackTrace = "Unable to click";
	    Reporter.log("<br>Click on element [" + rowIdx + "] [" + colHeader + "] in the table section [" + section + "]");
	    long oneMinuteTick = System.currentTimeMillis() + ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME*1000;
		while(System.currentTimeMillis() <= oneMinuteTick) { 
			try {		    	
				getCellAtIndex(rowIdx, getColIdx(colHeader), section);
			   	found = true;
			   	break;
			} catch (WebDriverException e) {
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);
			    pw.flush();
			    stackTrace = sw.toString();
			  } 		  
		}
		if(!found) {
			Reporter.log("<br>Click on element [" + rowIdx + "] [" + colHeader + "] in the table section [" + section + "]");
			throw new WebDriverException(stackTrace);
		}	
    }

    private List<WebElement> getRows(String section) {
        List<WebElement> rows = new ArrayList<WebElement>();
        
        if(section.equalsIgnoreCase("HEADER")) {  
        	List<WebElement> headerRows = getWrappedElement().findElements(By.cssSelector("thead tr"));
        	if(headerRows.size() > 0){
        		rows.add(headerRows.get(0));
        	}else{
        		rows.add(null);
        	}
        }        
        else if (section.equalsIgnoreCase("BODY")) {
        	List<WebElement> bodyRows = getWrappedElement().findElements(By.cssSelector("tbody tr"));
        	if (bodyRows.size() > 0) {
        		rows.addAll(bodyRows);
        	}
        }
        else if (section.equalsIgnoreCase("FOOTER")) {
        	List<WebElement> footerRows = getWrappedElement().findElements(By.cssSelector("tfoot tr"));
        	if (footerRows.size() > 0) {
        		rows.addAll(footerRows);
        	}
        }
        return rows;
    }
    

    public int getColIdx(String colHeader) {
        try {
            for (int idx = 0; ; idx++) {
                if (getCellTextAtIndex(0, idx, "HEADER").equalsIgnoreCase(colHeader)) {
                    return idx;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException("Column header not found");
        }

    }

    public void assertCellText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] text equals [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).isEqualToIgnoringCase(text);
    }


    public void assertCellText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] text equals [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).isEqualToIgnoringCase(text);
    }


    public void assertCellContainsText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] contains text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).containsIgnoringCase(text);
    }

    public void assertCellContainsText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] contains text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).containsIgnoringCase(text);
    }


    public void assertCellDoesNotContainText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] doesn't contain text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).doesNotContain(text);
    }

    public void assertCellDoesNotContainText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] doesn't contain text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).doesNotContain(text);
    }

    private boolean isSortIconDisplayed(int colIdx) {
        try {
            return getCellAtIndex(0, colIdx, "HEADER").findElement(By.xpath(".//span[contains(@class, 'DataTables_sort_icon')]")).isDisplayed();
        } catch (NoSuchElementException e) {
            //do nothing
        }
        return false;
    }

    public void assertColumnSortable(int colIdx) {
        Reporter.log("<br>Assert column " + colIdx + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(colIdx)).isTrue();
    }

    public void assertColumnSortable(String colHeader) {
        Reporter.log("<br>Assert column " + colHeader + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(getColIdx(colHeader))).isTrue();
    }

    public void assertColumnNotSortable(int colIdx) {
        Reporter.log("<br>Assert column " + colIdx + " is not sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(colIdx)).isFalse();
    }

    public void assertColumnNotSortable(String colHeader) {
        Reporter.log("<br>Assert column " + colHeader + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(getColIdx(colHeader))).isFalse();
    }
=======

package com.paytm.framework.ui.element;

import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.core.ExecutionConfig;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class Table extends UIElement {

    @Deprecated
    public Table(By by, String pageName) {
        super(by, pageName);
    }

    public Table(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public int getBodyRowCount() {
        return getRows("BODY").size();
    }

    public int getColumnCount() {
        return getWrappedElement().findElements(By.cssSelector("th")).size();
    }

    public WebElement getCellAtIndex(int rowIdx, int colIdx, String section) { 
        WebElement row = getRows(section).get(rowIdx);

        if(section.equalsIgnoreCase("HEADER")) {        
        	return row.findElements(By.tagName("th")).get(colIdx);
        } 
        else if (section.equalsIgnoreCase("BODY")) {
        	return row.findElements(By.tagName("td")).get(colIdx);
        } 
        else {
            final String error = String.format("Could not find cell at row: %s column: %s", rowIdx, colIdx);
            throw new RuntimeException(error);
        }
    }

    public String getCellTextAtIndex(int rowIdx, int colIdx, String section) {  
        return getCellAtIndex(rowIdx, colIdx, section).getText();
    }
    
    public void clickCell(int rowIdx, int colIdx, String section) {    	
    	Boolean found = false;
		String stackTrace = "Unable to click";
	    Reporter.log("<br>Click on element [" + rowIdx + "] [" + colIdx + "] in the table section [" + section + "]");
	    long oneMinuteTick = System.currentTimeMillis() + ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME*1000;
		while(System.currentTimeMillis() <= oneMinuteTick) { 
			try {		    	
				getCellAtIndex(rowIdx, colIdx, section).click();
			   	found = true;
			   	break;
			} catch (WebDriverException e) {
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);
			    pw.flush();
			    stackTrace = sw.toString();
			  } 		  
		}
		if(!found) {
			Reporter.log("<br>Click on element [" + rowIdx + "] [" + colIdx + "] in the table section [" + section + "]");
			throw new WebDriverException(stackTrace);
		}	
    }
    
    public void clickCell(int rowIdx, String colHeader, String section) {
    	Boolean found = false;
		String stackTrace = "Unable to click";
	    Reporter.log("<br>Click on element [" + rowIdx + "] [" + colHeader + "] in the table section [" + section + "]");
	    long oneMinuteTick = System.currentTimeMillis() + ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME*1000;
		while(System.currentTimeMillis() <= oneMinuteTick) { 
			try {		    	
				getCellAtIndex(rowIdx, getColIdx(colHeader), section);
			   	found = true;
			   	break;
			} catch (WebDriverException e) {
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);
			    pw.flush();
			    stackTrace = sw.toString();
			  } 		  
		}
		if(!found) {
			Reporter.log("<br>Click on element [" + rowIdx + "] [" + colHeader + "] in the table section [" + section + "]");
			throw new WebDriverException(stackTrace);
		}	
    }

    private List<WebElement> getRows(String section) {
        List<WebElement> rows = new ArrayList<WebElement>();
        
        if(section.equalsIgnoreCase("HEADER")) {  
        	List<WebElement> headerRows = getWrappedElement().findElements(By.cssSelector("thead tr"));
        	if(headerRows.size() > 0){
        		rows.add(headerRows.get(0));
        	}else{
        		rows.add(null);
        	}
        }        
        else if (section.equalsIgnoreCase("BODY")) {
        	List<WebElement> bodyRows = getWrappedElement().findElements(By.cssSelector("tbody tr"));
        	if (bodyRows.size() > 0) {
        		rows.addAll(bodyRows);
        	}
        }
        else if (section.equalsIgnoreCase("FOOTER")) {
        	List<WebElement> footerRows = getWrappedElement().findElements(By.cssSelector("tfoot tr"));
        	if (footerRows.size() > 0) {
        		rows.addAll(footerRows);
        	}
        }
        return rows;
    }
    

    public int getColIdx(String colHeader) {
        try {
            for (int idx = 0; ; idx++) {
                if (getCellTextAtIndex(0, idx, "HEADER").equalsIgnoreCase(colHeader)) {
                    return idx;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException("Column header not found");
        }

    }

    public void assertCellText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] text equals [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).isEqualToIgnoringCase(text);
    }


    public void assertCellText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] text equals [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).isEqualToIgnoringCase(text);
    }


    public void assertCellContainsText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] contains text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).containsIgnoringCase(text);
    }

    public void assertCellContainsText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] contains text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).containsIgnoringCase(text);
    }


    public void assertCellDoesNotContainText(int rowIdx, int colIdx, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colIdx + "] in section[" +section+ "] doesn't contain text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, colIdx, section).getText().trim()).doesNotContain(text);
    }

    public void assertCellDoesNotContainText(int rowIdx, String colHeader, String section, String text) {
        Reporter.log("<br>Assert [" + getElementName() + "] cell[" + rowIdx + "][" + colHeader + "] in section[" +section+ "] doesn't contain text [" + text + "] on [" + getPageName() + "]");
        Assertions.assertThat(getCellAtIndex(rowIdx, getColIdx(colHeader), section).getText().trim()).doesNotContain(text);
    }

    private boolean isSortIconDisplayed(int colIdx) {
        try {
            return getCellAtIndex(0, colIdx, "HEADER").findElement(By.xpath(".//span[contains(@class, 'DataTables_sort_icon')]")).isDisplayed();
        } catch (NoSuchElementException e) {
            //do nothing
        }
        return false;
    }

    public void assertColumnSortable(int colIdx) {
        Reporter.log("<br>Assert column " + colIdx + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(colIdx)).isTrue();
    }

    public void assertColumnSortable(String colHeader) {
        Reporter.log("<br>Assert column " + colHeader + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(getColIdx(colHeader))).isTrue();
    }

    public void assertColumnNotSortable(int colIdx) {
        Reporter.log("<br>Assert column " + colIdx + " is not sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(colIdx)).isFalse();
    }

    public void assertColumnNotSortable(String colHeader) {
        Reporter.log("<br>Assert column " + colHeader + " is sortable on [" + getPageName());
        Assertions.assertThat(isSortIconDisplayed(getColIdx(colHeader))).isFalse();
    }
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282

}