package com.paytm.framework.ui.base.page;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.core.ExecutionConfig;
import com.paytm.framework.ui.MoreExpectedConditions;
import com.paytm.framework.ui.element.UIElement;
import org.apache.commons.io.FileUtils;
import org.fest.assertions.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * This is the base class for every webpage.
 *
 */
public abstract class BasePage {

    protected String pageName;
    protected String pageURL;
    
    public BasePage(String pageName) {
        this.pageName = pageName;
    }

    /**
     * This method is used to set the page URL.
     *
     * @return Page URL
     */
    public String getPageURL() {
        return this.pageURL;
    }

    /**
     * This method is used to get the page name.
     *
     * @return Page name
     */
    public String getPageName() {
        return this.pageName;
    }

    /**
     * This method is used to set the page name.
     *
     * @param Page name
     */
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }  

    /**
     * This method is used to launch the web driver on current page.
     *
     */
    public void launch(){
        Reporter.log("<br>Launch URL [" + pageURL + "]");
        DriverManager.getDriver().get(this.pageURL);
    }
    
    
    public void navigate(String url){
        Reporter.log("<br>Launch URL [" + url + "]");
        DriverManager.getDriver().get(url);
    }
    
    

    public void assertContainsText(String text) {
        Reporter.log("<br>Assert [" + pageName + "] contains text [" + text + "]");
        Assertions.assertThat(DriverManager.getDriver().findElement(By.tagName("body")).getText()).containsIgnoringCase(text);
    }

    public void assertDoesNotContainText(String text) {
        Reporter.log("<br>Assert [" + pageName + "] doesn't contain text [" + text + "]");
        Assertions.assertThat(DriverManager.getDriver().findElement(By.tagName("body")).getText()).doesNotContain(text);
    }

    public void assertContainsTitle(String title) {
        Reporter.log("<br>Assert [" + pageName + "] title contains [" + title + "]");
        Assertions.assertThat(DriverManager.getDriver().getTitle()).containsIgnoringCase(title);
    }

    public void waitUntilLoads() {
        Reporter.log("<br>Wait until [" + pageName + "] loads");
        int timeToWaitTillDocumentStartsLoading = 5;
        try {
            // It is important to first wait till the document has started loading otherwise tests fail intermittently
            // when the execution is fast on some browser/environment. e.g. if clicking on a link results in a
            // page load & we have a waitForPageLoad after click then if the document hasn't started to load, it will
            // assume that the document is ready
            DriverManager.setWebDriverPageWait(timeToWaitTillDocumentStartsLoading);
            DriverManager.getWebDriverPageWait().until(MoreExpectedConditions.documentIsLoading());
            DriverManager.setWebDriverPageWait(ExecutionConfig.MAX_PAGE_LOAD_WAIT_TIME - timeToWaitTillDocumentStartsLoading);
            DriverManager.getWebDriverPageWait().until(MoreExpectedConditions.documentIsReady());
        } catch (Throwable e) {
            // Do nothing since we don't want to fail a test if the page hasn't loaded completely
        } finally {
            DriverManager.resetWebDriverPageWait();
        }
    }

    public void waitUntilContainsTitle(final String title) {
        Reporter.log("<br>Wait until [" + pageName + "] contains title [" + title + "]");
        DriverManager.getWebDriverPageWait().until(ExpectedConditions.titleContains(title));
    }

    public void waitUntilContainsText(final String text) {
        Reporter.log("<br>Wait until [" + pageName + "] contains text [" + text + "]");
        DriverManager.getWebDriverPageWait().until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"), text));
    }

    public void waitUntilDoesNotContainText(final String text) {
        Reporter.log("<br>Wait until [" + pageName + "] doesn't contain text [" + text + "]");
        DriverManager.getWebDriverPageWait().until(ExpectedConditions.not
                (ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"), text)));
    }

    public void waitUntilAllAJAXCallsFinish() {
        Reporter.log("<br>Wait until all ajax requests complete on [" + pageName + "]");
        WebDriverWait wait = DriverManager.getWebDriverPageWait();
        long timeOutInMilliSeconds = ExecutionConfig.MAX_PAGE_LOAD_WAIT_TIME * 1000;
        long startTime = System.currentTimeMillis();
        try {
            if (timeOutInMilliSeconds > 0) {
                wait.withTimeout(timeOutInMilliSeconds, TimeUnit.MILLISECONDS).until(MoreExpectedConditions.jQueryAJAXCallsHaveCompleted());
            }
            pause(1);

            timeOutInMilliSeconds = timeOutInMilliSeconds - (System.currentTimeMillis() - startTime);
            startTime = System.currentTimeMillis();
            if (timeOutInMilliSeconds > 0) {
                wait.withTimeout(timeOutInMilliSeconds, TimeUnit.MILLISECONDS).until(MoreExpectedConditions.jQueryAJAXCallsHaveCompleted());
            }
            pause(1);

            timeOutInMilliSeconds = timeOutInMilliSeconds - (System.currentTimeMillis() - startTime);
            if (timeOutInMilliSeconds > 0) {
                wait.withTimeout(timeOutInMilliSeconds, TimeUnit.MILLISECONDS).until(MoreExpectedConditions.jQueryAJAXCallsHaveCompleted());
            }

        } catch (Throwable e) {
            // Do nothing since we don't want to fail a test case in case of ongoing request
        }
    }

    public void waitUntilAngularProcessingFinish() {
        Reporter.log("<br>Wait until angular js has finished processing on [" + pageName + "]");
        DriverManager.getWebDriverPageWait().until(MoreExpectedConditions.angularHasFinishedProcessing());
    }

    public void waitUntilFrameAppearsAndSwitchToIt(String frameLocator) {
        Reporter.log("<br>Wait until frame with locator [" + frameLocator + "] appears and switch to it on [" + pageName + "]");
        DriverManager.getWebDriverPageWait().until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
    }

    public void waitUntilFrameAppearsAndSwitchToIt(UIElement frameElement) {
        Reporter.log("<br>Wait until frame element [" + frameElement.getElementName() + "] appears and switch to it on [" + pageName + "]");
        DriverManager.getWebDriverPageWait().until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameElement.getWrappedElement()));
    }

    public void pause(int seconds) {
        Reporter.log("<br>Pause for [" + seconds + "] seconds");
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void takeScreenshot() {
        final DateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy HH-mm-ss");
        final String fileName = Reporter.getCurrentTestResult().getMethod().getMethodName() + "_" + timeFormat.format(new Date()) + ".png";
        final WebDriver driver = DriverManager.getCurrentWebDriver();

        if (driver != null) {
            try {
                File scrFile;

                if (driver.getClass().equals(RemoteWebDriver.class)) {
                    scrFile = ((TakesScreenshot) new Augmenter().augment(driver))
                            .getScreenshotAs(OutputType.FILE);
                } else {
                    scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                }

                String outputDir = Reporter.getCurrentTestResult().getTestContext().getOutputDirectory();
                outputDir = outputDir.substring(0, outputDir.lastIndexOf(File.separator)) + "/html";
                final File saved = new File(outputDir, fileName);
                FileUtils.copyFile(scrFile, saved);
                Reporter.log("<br><a href=\"" + fileName + "\" target=\"_blank\"><b>Screenshot</b></a>");
            } catch (IOException e) {
                //TODO
            }
        } else {
            Reporter.log("<br>Couldn't capture screen-shot as WebDriver is null");
        }
    }

    public static synchronized Object executeJavaScript(String javaScript, Object... args){
        Reporter.log("<br>Execute javascript [" + javaScript + "]");
        WebDriver driver = DriverManager.getDriver();
        try{
            return ((JavascriptExecutor) driver).executeScript(javaScript, args);
        }catch(Throwable e){
            throw (RuntimeException) e;
        }
    }

    public void assertPageURL(String url) {
        Reporter.log("<br>Assert [" + pageName + "] URL [" + url + "]");
        Assertions.assertThat(DriverManager.getDriver().getCurrentUrl()).isEqualToIgnoringCase(url);
    }

    public void assertPageContainsURL(String url) {
        Reporter.log("<br>Assert [" + pageName + "] URL contains [" + url + "]");
        Assertions.assertThat(DriverManager.getDriver().getCurrentUrl()).containsIgnoringCase(url);
    }

    public void assertPageURL() {
        String url = DriverManager.getDriver().getCurrentUrl();
        Reporter.log("<br>Assert [" + pageName + "] URL [" + url + "]");
        Assertions.assertThat(DriverManager.getDriver().getCurrentUrl()).isEqualToIgnoringCase(getPageURL());
    }

    public void assertPageContainsURL() {
        String url = DriverManager.getDriver().getCurrentUrl();
        Reporter.log("<br>Assert [" + pageName + "] URL contains [" + url + "]");
        Assertions.assertThat(DriverManager.getDriver().getCurrentUrl()).containsIgnoringCase(getPageURL());
    }

    public void refresh() {
        Reporter.log("<br>Refresh [" + pageName + "]");
        DriverManager.getDriver().navigate().refresh();
        waitUntilLoads();
    }

}