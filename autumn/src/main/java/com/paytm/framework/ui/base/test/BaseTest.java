package com.paytm.framework.ui.base.test;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.core.ExecutionConfig;
import com.paytm.framework.utils.CommonUtils;
import com.paytm.framework.utils.DatabaseUtil;
import com.paytm.framework.utils.SpecializedScreenRecorder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.awt.*;

import org.monte.media.Format;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import static org.monte.media.AudioFormatKeys.*;
import static org.monte.media.VideoFormatKeys.*;

public abstract class BaseTest {
	 	 
	 private static ExtentReports extent;
	 protected ExtentTest logger;
	 private ScreenRecorder screenRecorder;
	 
	 static {
<<<<<<< HEAD
		 final String extentReportName =  System.getProperty("user.dir") + ExecutionConfig.EXTENT_REPORT_PATH 
=======
		 final String extentReportName =  ExecutionConfig.EXTENT_REPORT_PATH 
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	        		+ ExecutionConfig.EXTENT_REPORT_NAME + ".html";           
	        extent = new ExtentReports(extentReportName, true);        
	        extent
	        	.addSystemInfo("Browser", ExecutionConfig.BROWSER)
	        	.addSystemInfo("Headless", ExecutionConfig.HEADLESS.toString().toUpperCase());
	        extent.loadConfig(new File(System.getProperty("user.dir") + ExecutionConfig.EXTENT_CONFIG_PATH));
	 }
	 
	
    @AfterSuite
    public void closeAllBrowsers() {
        DatabaseUtil.getInstance().closeAllConnections();
        DriverManager.closeDriverObjects();
    }

    @Parameters({"browser", "platform", "headless"})
    @BeforeSuite
    public void setBrowserBeforeSuite(@Optional("") String browser, @Optional("") String platform, @Optional("") String headless) {
        setEnv(browser, platform, headless);
    }
    
    @BeforeSuite
    public void startToRecord() throws IOException, AWTException {
        startRecording();        
    }

    @Parameters({"browser", "platform", "headless"})
    @BeforeTest
    public void setBrowserBeforeTest(@Optional("") String browser, @Optional("") String platform, @Optional("") String headless) {
        setEnv(browser, platform, headless);        
    }

    @Parameters({"browser", "platform", "headless"})
    @BeforeClass
    public void setBrowserBeforeClass(@Optional("") String browser, @Optional("") String platform, @Optional("") String headless) {
    	setEnv(browser, platform, headless);
    }

    @Parameters({"browser", "platform", "headless"})
    @BeforeMethod
    public void setBrowserBeforeMethods(@Optional("") String browser, @Optional("") String platform, @Optional("") String headless) {
    	setEnv(browser, platform, headless);    
    }
    
    @BeforeMethod
    public void setupExtentReport(Method method) {        
    	logger = extent.startTest((this.getClass().getSimpleName() + " :: " + method.getName()), method.getName());
    }


    @AfterMethod
    public void setScreenCaptureTrue() {
        DriverManager.setCaptureScreenShot(true);
    }

    @AfterMethod
    public void resetElementWait() {
        DriverManager.resetWebDriverElementWait();
    }

    @AfterMethod
    public void resetPageLoadWait() {
        DriverManager.resetWebDriverPageWait();
    }
    
    @AfterMethod
    public void addLogsIntoExtentReport(ITestResult result) {    	
    	for (String logs : Reporter.getOutput(result)) {
    		logger.log(LogStatus.INFO, logs);    		
    	}
    }
    
    @AfterMethod
    public void getResult(ITestResult result) throws IOException {
    	if(result.getStatus() == ITestResult.SUCCESS){
    		logger.log(LogStatus.PASS, "Test Case Passed: "+result.getName());	
    	}
    	else if(result.getStatus() == ITestResult.FAILURE){
    		logger.log(LogStatus.FAIL, "Test Case Failed: "+result.getName());
    		logger.log(LogStatus.FAIL, "Test Case Failed: "+result.getThrowable());    		
    		logger.log(LogStatus.FAIL, "Snapshot: " 
    				+logger.addScreenCapture(CommonUtils.createScreenshot(result)));
    	}
    	else if(result.getStatus() == ITestResult.SKIP){
    		logger.log(LogStatus.SKIP, "Test Case Skipped: "+result.getName());
    	}
    	extent.endTest(logger);
    }
    
    @AfterSuite
    public void endReport() {
    	extent.flush();
        extent.close();
    }
    
    @AfterSuite
    public void stopToRecord() throws IOException{
        stopRecording();
    }

    /**
     * This method is used to set the environment for test execution.
     *
     * @param The browser.
     * @param The platform.
     * @param True to run the browser in headless mode.
     */
    private static void setEnv(String browser, String platform, String headless) {
        if(browser == null || browser.isEmpty()){
            DriverManager.setBrowserName(ExecutionConfig.BROWSER);            
        }else{
            DriverManager.setBrowserName(browser);            
        }
        
        if(headless == null || headless.isEmpty()){       	
        	DriverManager.setHeadless(ExecutionConfig.HEADLESS);	
        }else{
        	DriverManager.setHeadless(headless);
        }

        if(platform == null || platform.isEmpty()){
            DriverManager.setPlatformName(ExecutionConfig.PLATFORM);
        }else{
            DriverManager.setPlatformName(platform);
        }
    }
    
    public void startRecording() throws IOException, AWTException {
    	File file = new File(System.getProperty("user.dir") + ExecutionConfig.EXECUTION_RECORDING_PATH);              
        
    	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = screenSize.width;
        int height = screenSize.height;
                          
        Rectangle captureSize = new Rectangle(0,0, width, height);
                          
        GraphicsConfiguration gc = GraphicsEnvironment
        		.getLocalGraphicsEnvironment()
        		.getDefaultScreenDevice()
        		.getDefaultConfiguration();

        this.screenRecorder = new SpecializedScreenRecorder(gc, captureSize, 
        		new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
        		new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, 
        				CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
        				DepthKey, 24, FrameRateKey, Rational.valueOf(15),
        				QualityKey, 1.0f,
        				KeyFrameIntervalKey, 15 * 60),
        		new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black",
        				FrameRateKey, Rational.valueOf(30)),
        		null, file, "ExecutionRecording");
       this.screenRecorder.start();
    }

    public void stopRecording() throws IOException 
    {
      this.screenRecorder.stop();
    }
    
}