package com.paytm.framework.ui.element;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.ui.MoreExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import java.util.List;

public class Select extends UIElement {

    @Deprecated
    public Select(By by, String pageName) {
        super(by, pageName);
    }

    public Select(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public boolean isMultiple() {
        return new org.openqa.selenium.support.ui.Select(getWrappedElement()).isMultiple();
    }

    public void deselectByIndex(int index) {
        Reporter.log("<br>De-select [" + getElementName() + "] option at position [" + index + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).deselectByIndex(index);
    }

    public void selectByValue(String value) {
        Reporter.log("<br>Select [" + getElementName() + "] option with value [" + value + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).selectByValue(value);
    }

    public WebElement getFirstSelectedOption() {
        return new org.openqa.selenium.support.ui.Select(getWrappedElement()).getFirstSelectedOption();
    }

    public void selectByVisibleText(String text) {
        Reporter.log("<br>Select [" + getElementName() + "] option with text [" + text + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).selectByVisibleText(text);
    }

    public void deselectByValue(String value) {
        Reporter.log("<br>De-Select [" + getElementName() + "] option with value [" + value + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).deselectByValue(value);
    }

    public void deselectAll() {
        Reporter.log("<br>De-select [" + getElementName() + "] all options on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).deselectAll();
    }

    public List<WebElement> getAllSelectedOptions() {
        return new org.openqa.selenium.support.ui.Select(getWrappedElement()).getAllSelectedOptions();
    }

    public List<WebElement> getOptions() {
        return new org.openqa.selenium.support.ui.Select(getWrappedElement()).getOptions();
    }

    public void deselectByVisibleText(String text) {
        Reporter.log("<br>De-select [" + getElementName() + "] option with text [" + text + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).deselectByVisibleText(text);
    }

    public void selectByIndex(int index) {
        Reporter.log("<br>Select [" + getElementName() + "] option at position [" + index + "] on [" + getPageName() + "]");
        new org.openqa.selenium.support.ui.Select(getWrappedElement()).selectByIndex(index);
    }

    public void waitUntilSelected() {
        Reporter.log("<br>Wait until [" + getElementName() + "] option is selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), true));
    }

    public void waitUntilDeSelected() {
        Reporter.log("<br>Wait until [" + getElementName() + "] option is de-selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), false));
    }

    public void waitUntilOptionToBeSelectedByVisibeText(String optionText) {
        Reporter.log("<br>Wait until [" + getElementName() + "] option with text [" + optionText + "] is selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(MoreExpectedConditions.optionToBeSelectedInElement(new org.openqa.selenium.support.ui.Select(getWrappedElement()), optionText, true));
    }

    public void waitUntilOptionToBeSelectedByValue(String optionValue) {
        Reporter.log("<br>Wait until [" + getElementName() + "] option with value [" + optionValue + "] is selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(MoreExpectedConditions.optionToBeSelectedInElement(new org.openqa.selenium.support.ui.Select(getWrappedElement()), optionValue, false));
    }
}