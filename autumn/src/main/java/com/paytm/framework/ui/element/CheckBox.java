package com.paytm.framework.ui.element;

import com.paytm.framework.core.DriverManager;
import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

public class CheckBox extends UIElement {

    @Deprecated
    public CheckBox(By by, String pageName) {
        super(by, pageName);
    }

    public CheckBox(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public void toggle() {
        Reporter.log("<br>Click [" + getElementName() + "] on [" + getPageName() + "]");
        click();
    }

    public void check() {
        Reporter.log("<br>Check [" + getElementName() + "] on [" + getPageName() + "]");
        if (!isChecked()) {
            toggle();
        }
    }

    public void unCheck() {
        Reporter.log("<br>Uncheck [" + getElementName() + "] on [" + getPageName() + "]");
        if (isChecked()) {
            toggle();
        }
    }

    public boolean isChecked() {
        return getWrappedElement().isSelected();
    }

    public void assertChecked() {
        Reporter.log("<br>Assert [" + getElementName() + "] is checked on [" + getPageName() + "]");
        Assertions.assertThat(isChecked()).isEqualTo(true);
    }

    public void assertUnChecked() {
        Reporter.log("<br>Assert [" + getElementName() + "] is un-checked on [" + getPageName() + "]");
        Assertions.assertThat(isChecked()).isEqualTo(false);
    }

    public void waitUntilChecked() {
        Reporter.log("<br>Wait until [" + getElementName() + "] is checked on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), true));
    }

    public void waitUntilUnChecked() {
        Reporter.log("<br>Wait until [" + getElementName() + "] is un-checked on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), false));
    }
}
