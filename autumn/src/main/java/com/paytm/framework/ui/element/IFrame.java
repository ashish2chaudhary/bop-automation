package com.paytm.framework.ui.element;

import org.openqa.selenium.By;

public class IFrame extends UIElement {

    @Deprecated
    public IFrame(By by, String pageName) {
        super(by, pageName);
    }

    public IFrame(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public void switchToIFrame() {
    	switchToFrame();
    }
    
    public void switchToParentIFrame() {
    	switchToParentFrame();
    }

    
}
