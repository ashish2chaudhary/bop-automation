package com.paytm.framework.core;

import com.paytm.framework.utils.CommonUtils;

public class ExecutionConfig {

    public static final String PLATFORM;
    public static final String BROWSER;
    public static final String HEADLESS; 
    public static final int MAX_PAGE_LOAD_WAIT_TIME;
    public static final int MAX_ELEMENT_LOAD_WAIT_TIME;
    public static final int TEST_CASE_RETRY_COUNT;
    public static final String EXECUTION_ENVIRONMENT;
    public static final String HUB_NODE_URL;
    public static final String SEND_EXEC_REPORT_EMAIL;
    public static final String SMTP_HOSTNAME;
    public static final String SMTP_PORT;
    public static final String SMTP_USERNAME;
    public static final String SMTP_PASSWORD;
    public static final String TEMP_DATA_PATH;
    public static final String EXEC_REPORT_EMAIL_RECEIVER;
    public static final String EXTENT_REPORT_PATH;
    public static final String EXTENT_REPORT_NAME;
    public static final String EXTENT_CONFIG_PATH;
    public static final String FAILED_TESTCASE_SCREENSHOT_PATH;
    public static final String EXECUTION_RECORDING_PATH;
    

    static {
        try {
            PLATFORM = System.getProperty("PLATFORM", "linux");
            BROWSER = System.getProperty("BROWSER", "chrome");
            HEADLESS = System.getProperty("HEADLESS", "false");            
            MAX_PAGE_LOAD_WAIT_TIME = Integer.parseInt(System.getProperty("MAX_PAGE_LOAD_WAIT_TIME", "60"));
            MAX_ELEMENT_LOAD_WAIT_TIME = Integer.parseInt(System.getProperty("MAX_ELEMENT_LOAD_WAIT_TIME", "60"));
            TEST_CASE_RETRY_COUNT =Integer.parseInt( System.getProperty("TEST_CASE_RETRY_COUNT","0"));
            EXECUTION_ENVIRONMENT = System.getProperty("EXECUTION_ENVIRONMENT", "local");
            HUB_NODE_URL = System.getProperty("HUB_NODE_URL", "");
            SMTP_HOSTNAME = System.getProperty("SMTP_HOSTNAME", "");
            SMTP_PORT = System.getProperty("SMTP_PORT", "");
            SMTP_USERNAME = System.getProperty("SMTP_USERNAME", "");
            SMTP_PASSWORD = System.getProperty("SMTP_PASSWORD", "");
            TEMP_DATA_PATH = CommonUtils.getPathWithValidSeperator(System.getProperty("TEMP_DATA_DIRECTORY", ""));
            SEND_EXEC_REPORT_EMAIL = System.getProperty("SEND_EXEC_REPORT_EMAIL", "");
            EXEC_REPORT_EMAIL_RECEIVER = System.getProperty("EXEC_REPORT_EMAIL_RECEIVER", "");     
            EXTENT_REPORT_PATH = System.getProperty("EXTENT_REPORT_PATH", "");
            EXTENT_REPORT_NAME = System.getProperty("EXTENT_REPORT_NAME", "");
            EXTENT_CONFIG_PATH = System.getProperty("EXTENT_CONFIG_PATH", "");
            FAILED_TESTCASE_SCREENSHOT_PATH = System.getProperty("FAILED_TESTCASE_SCREENSHOT_PATH", "");
            EXECUTION_RECORDING_PATH = System.getProperty("EXECUTION_RECORDING_PATH", "");
            CommonUtils.createDirectory(TEMP_DATA_PATH);         
        }catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException("Something wrong !!! Check configurations.", e);
        }
    }
}