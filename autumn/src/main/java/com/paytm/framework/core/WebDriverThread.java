package com.paytm.framework.core;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * This class contains WebDriver, DesiredCapabilities, Browser, Platform and Headless property.
 *
 */
class WebDriverThread {

    private WebDriver webDriver;
    private DesiredCapabilities capabilities;
    private String browser;
    private String platform;
    private Boolean headless;

    /**
     * This method is used to set the platform for test execution.
     *
     */
    private void setPlatform() {
        if (ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
            switch (platform.toUpperCase()) {
                case "LINUX":
                    capabilities.setPlatform(Platform.LINUX);
                    break;
                case "MAC":
                    capabilities.setPlatform(Platform.MAC);
                    break;
                case "WINDOWS":
                    capabilities.setPlatform(Platform.WINDOWS);
                    break;
                default:
                    throw new RuntimeException("Invalid execution environment: " + platform);
            }
        }
    }

    /**
     * This method returns the web driver instance after setting the Browser and DesiredCapabilities.
     *
     * @return WebDriver
     */
    public WebDriver getDriver() {

        if (null == webDriver || ((RemoteWebDriver) webDriver).getSessionId() == null) {

            browser = DriverManager.getBrowserName();            
            platform = DriverManager.getPlatformName(); 
            headless = DriverManager.isHeadless();
         
            switch (browser.toUpperCase()) {

                case "FIREFOX":
                    capabilities = DesiredCapabilities.firefox();
                    setPlatform();
                    FirefoxProfile profile = new FirefoxProfile();                 
                    FirefoxOptions option = new FirefoxOptions();
                    if(headless) option.setHeadless(true);
                    profile.setAcceptUntrustedCertificates(true);
                    profile.setPreference("browser.download.folderList", 2);
                    profile.setPreference("browser.download.manager.showWhenStarting", false);
                    profile.setPreference("browser.download.dir", ExecutionConfig.TEMP_DATA_PATH);
                    profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"
                                    + "application/pdf;"
                                    + "application/vnd.openxmlformats-officedocument.wordprocessingml.document;"
                                    + "text/plain;"
                                    + "text/csv"
                                    + "application/zip");
                    capabilities.setAcceptInsecureCerts(true);
                    capabilities.setCapability(FirefoxDriver.PROFILE, profile);  
                    capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, option);
                    if (!ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
                        FirefoxDriverManager.getInstance().setup();
                        webDriver = new FirefoxDriver(capabilities);
                    }
                    break;

                case "CHROME":
                    capabilities = DesiredCapabilities.chrome();
                    setPlatform();
                    capabilities.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
                    HashMap<String, String> chromePreferences = new HashMap<String, String>();
                    chromePreferences.put("profile.password_manager_enabled", "false");
                    chromePreferences.put("profile.default_content_settings.popups", "0");
                    chromePreferences.put("download.prompt_for_download", "false");
                    chromePreferences.put("download.default_directory", ExecutionConfig.TEMP_DATA_PATH);
                    capabilities.setCapability("chrome.prefs", chromePreferences);
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--test-type");
                    options.addArguments("start-maximized");
                    options.addArguments("--disable-web-security");
                    options.addArguments("--allow-running-insecure-content");
                    if(headless) options.addArguments("headless");
                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                    if (!ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
                        ChromeDriverManager.getInstance().setup();
                        webDriver = new ChromeDriver(capabilities);
                    }
                    break;

                case "SAFARI":
                    capabilities = DesiredCapabilities.safari();
                    setPlatform();
                    if (!ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
                        webDriver = new SafariDriver(capabilities);
                    }
                    break;

                case "IE":
                    setPlatform();
                    capabilities = DesiredCapabilities.internetExplorer();
                    capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
                    capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
                    capabilities.setCapability("requireWindowFocus", true);
                    if (!ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
                        InternetExplorerDriverManager.getInstance().setup();
                        webDriver = new InternetExplorerDriver(capabilities);
                    }
                    break;
                default:
                    throw new RuntimeException("Invalid browser: " + browser);
            }

            if (ExecutionConfig.EXECUTION_ENVIRONMENT.equalsIgnoreCase("remote")) {
                try {
                    webDriver = new RemoteWebDriver(new URL(ExecutionConfig.HUB_NODE_URL), capabilities);
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Malformed HUB_URL", e);
                }
            }
            webDriver.manage().timeouts().pageLoadTimeout(120000L, TimeUnit.MILLISECONDS);
            webDriver.manage().timeouts().setScriptTimeout(10000L, TimeUnit.MILLISECONDS);
            webDriver.manage().timeouts().implicitlyWait(30000L,TimeUnit.MILLISECONDS);
            if (!browser.equalsIgnoreCase("chrome")) {
                webDriver.manage().window().maximize();
            }
        }
        return webDriver;
    }

    /**
     * This method is used to quit the web driver instance.
     *
     */
    public void quitDriver() {
        if (null != webDriver) {
            webDriver.quit();
            webDriver = null;
        }
    }

    /**
     * This method is used to get the current running instance of the web driver.
     *
     * @return WebDriver
     */
    public WebDriver getWebDriver() {
        return webDriver;
    }

    /**
     * This method is used to get the current Browser name.
     *
     * @return Browser name.
     */
    public String getBrowser() {
        return browser;
    }

    /**
     * This method is used to get the current Platform name.
     *
     * @return Platform name.
     */
    public String getPlatform() {
        return platform;
    }
}