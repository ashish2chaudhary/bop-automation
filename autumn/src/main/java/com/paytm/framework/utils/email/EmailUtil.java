package com.paytm.framework.utils.email;

import com.paytm.framework.core.ExecutionConfig;
import org.fest.assertions.api.Assertions;
import org.testng.Reporter;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Arrays;
import java.util.Properties;

/**
 * This class is used to send email.
 *
 */
public class EmailUtil{

	/**
     * This method is used to retrieve all emails.
     *
     *
     * @param The email address.
     * @param The password.
     * @return All emails.
     */
	public static EmailMessage[] getAllEmails(String emailAddress, String password){

		Message messages[] = null;
		Folder inbox = null;
		EmailMessage emailMessages[] = null;
		Store store = null;

		try{
			store = StoreType.valueOf(getDomain(emailAddress).toUpperCase()).getStore(emailAddress, password);
			store.connect(emailAddress, password);
			inbox = store.getFolder("inbox");
			inbox.open(Folder.READ_ONLY);
			messages = inbox.getMessages();
			if(messages.length == 0){
				throw new RuntimeException("Inbox empty.");
			}
			else{
				emailMessages = new EmailMessage[messages.length];
				int i = 0;
				for(int j = messages.length-1; j >= 0; j--){
					emailMessages[i] = new EmailMessage();
					emailMessages[i].setSubject(getEmailSubjectTemp(messages[j]));
					emailMessages[i].setFrom(getEmailFromTemp(messages[j]));
					emailMessages[i].setTo(getEmailToTemp(messages[j]));
					emailMessages[i].setCc(getEmailCcTemp(messages[j]));
					emailMessages[i].setPlainText(getEmailPlainTextTemp(messages[j]));
					emailMessages[i].setHtmlText(getEmailHTMLTextTemp(messages[j]));
					i++;
				}
			}
		}catch(Throwable e){
			throw (RuntimeException) e;
		}finally{
			try{
				if(inbox != null)
					inbox.close(true);
				if(store != null)
					store.close();
			}catch(Throwable e1){
				throw (RuntimeException) e1;
			}
		}
		return emailMessages;
	}

	/**
     * This method is used to send email.
     *
     *
     * @param The from email address.
     * @param The to email address.
     * @param The cc email address.
     * @param The bcc email address.
     * @param The subject.
     * @param The body.
     * @param The attachment path.
     */
	public static void sendEmail(String from, String to, String cc, String bcc, String subject, String body,
								 String attachmentPath){

		Reporter.log("<br>Send email:");
		Reporter.log("<br>From: " + from);
		Reporter.log("<br>to: " + to);
		Reporter.log("<br>cc: " + cc);
		Reporter.log("<br>bcc: " + bcc);
		Reporter.log("<br>subject: " + subject);
		Reporter.log("<br>body: " + body);
		Reporter.log("<br>attachment: " + attachmentPath);

		System.out.println("Send email:");
		System.out.println("\tFrom: " + from);
		System.out.println("\tTo: " + to);
		System.out.println("\tcc: " + cc);
		System.out.println("\tbcc: " + bcc);
		System.out.println("\tsubject: " + subject);
		System.out.println("\tbody: " + body);
		System.out.println("\tattachment: " + attachmentPath);

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", ExecutionConfig.SMTP_HOSTNAME);
		props.put("mail.smtp.port", ExecutionConfig.SMTP_PORT);
		props.put("mail.smtp.timeout", "30000");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(ExecutionConfig.SMTP_USERNAME, ExecutionConfig.SMTP_PASSWORD);
					}
				});

		try{
			Multipart multipart = new MimeMultipart();
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from, null));
			message.addRecipients(Message.RecipientType.TO, to);
			if(null == cc){
				cc = "";
			}
			message.addRecipients(RecipientType.CC, cc);
			if(null == bcc){
				bcc = "";
			}
			message.addRecipients(RecipientType.BCC, bcc);
			message.setSubject(subject);

			MimeBodyPart textBodyPart = new MimeBodyPart();
			textBodyPart.setContent(body, "text/html");
			multipart.addBodyPart(textBodyPart);

			if(attachmentPath != null || attachmentPath.trim().isEmpty()) {
				MimeBodyPart attachmentBodyPart= new MimeBodyPart();
				DataSource source = new FileDataSource(attachmentPath);
				attachmentBodyPart.setDataHandler(new DataHandler(source));
				attachmentBodyPart.setFileName(new File(attachmentPath).getName());
				multipart.addBodyPart(attachmentBodyPart);
			}

			message.setContent(multipart);

			Transport.send(message);
		}catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	/**
     * This method is used to delete all emails.
     *
     *
     * @param The email address.
     * @param The password.
     */
	public static void deleteAllEmails(String emailAddress, String password){

		Reporter.log("<br>Delete all emails [" + emailAddress + "]");
		delete(emailAddress, password, 1, null, -1);
	}

	/**
     * This method is used to delete a particular email based on email subject.
     *
     *
     * @param The email address.
     * @param The password.
     * @param The subject of the email.
     */
	public static void deleteEmail(String emailAddress, String password, String emailSubject){
		Reporter.log("<br>Delete email with subject [" + emailSubject + "]");
		delete(emailAddress, password, 2, emailSubject, -1);
	}

	/**
     * This method is used to delete a particular email based on index.
     *
     *
     * @param The email address.
     * @param The password.
     * @param The index.
     */
	public static void deleteEmail(String emailAddress, String password, int emailPosition){
		Reporter.log("<br>Delete email at position [" + emailPosition + "]");
		delete(emailAddress, password, 3, null, emailPosition);
	}

	/**
     * This method is used to get the domain.
     *
     *
     * @param The email address.
     * @return The domain.
     */
	private static String getDomain(String emailAddress){
		String abc = emailAddress.split("@")[1];
		abc = abc.split("\\.")[0];
		return abc; // abc@def.gh.ij returns "def"
	}

	private static void delete(String emailAddress, String password, int type, String emailSubject, int emailPosition){

		Store store = null;
		try {
			store = StoreType.valueOf(getDomain(emailAddress).toUpperCase()).getStore(emailAddress, password);
			store.connect(emailAddress,password);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}

		Folder inbox = null;
		boolean emailPresent = false;
		try{
			inbox = store.getFolder("inbox");
			inbox.open(Folder.READ_WRITE);
			Message messages[] = inbox.getMessages();
			if(messages.length != 0){
				if(type == 1){
					emailPresent = true;
					for(Message m: messages){
						m.setFlag(Flags.Flag.DELETED, true);
					}
				}else if(type == 2){
					for(Message m: messages){
						if(m.getSubject().equalsIgnoreCase(emailSubject)){
							emailPresent = true;
							m.setFlag(Flags.Flag.DELETED, true);
							break;
						}
					}
				}else{
					if(emailPosition >= 0 && emailPosition < messages.length){
						emailPresent = true;
						messages[emailPosition].setFlag(Flags.Flag.DELETED, true);
					}
				}
			}else{
				// do nothing
			}
		}catch(Throwable e){
			throw (RuntimeException) e;
		}finally{
			try{
				if(inbox != null)
					inbox.close(true);
				if(store != null)
					store.close();
			}catch(Throwable e1){
				throw (RuntimeException) e1;
			}
		}
	}

	/**
     * This method is used to get a particular email based on index.
     *
     *
     * @param The email address.
     * @param The password.
     * @param The index.
     * @return The email.
     */
	public static EmailMessage getEmail(String emailAddress, String password, int emailPosition){
		Reporter.log("<br>Read email at position [" + emailPosition + "] from inbox [" + emailAddress + "]");
		EmailMessage messages[] = getAllEmails(emailAddress, password);
		if(emailPosition < 0 || emailPosition >= messages.length){
			throw new RuntimeException("Email not present.");
		}
		return messages[emailPosition];
	}

	/**
     * This method is used to get a particular email based on email subject.
     *
     *
     * @param The email messages.
     * @param The email subject.
     * @return The email.
     */
	public static EmailMessage getEmail(EmailMessage messages[], String emailSubject){
		Reporter.log("<br> Get email with subject [" + emailSubject + "]");
		int emailPosition = getEmailPosition(messages, emailSubject);
		return messages[emailPosition];
	}

	/**
     * This method is used to get a particular email based on email subject.
     *
     *
     * @param The email address.
     * @param The password.
     * @param The email subject.
     * @return The email.
     */
	public static EmailMessage getEmail(String emailAddress, String password, String emailSubject){
		Reporter.log("<br>Read email with subject [" + emailSubject + "] from inbox [" + emailAddress + "]");
		EmailMessage messages[] = getAllEmails(emailAddress, password);
		return messages[getEmailPosition(messages, emailSubject)];
	}

	/**
     * This method is used to get the position of a particular email based on email subject.
     *
     *
     * @param The email messages.
     * @param The email subject.
     * @return The position.
     */
	private static int getEmailPosition(EmailMessage messages[], String emailSubject){
		int emailPosition = -1;
		try {
			if(messages.length != 0){
				for(int i = 0; i <= messages.length; i++){
					if(messages[i].getSubject().equalsIgnoreCase(emailSubject)){
						emailPosition = i;
						break;
					}
				}
			}
			if(emailPosition == -1){
				throw new RuntimeException("Email not present.");
			}
		}catch (Throwable e) {
			throw (RuntimeException) e;
		}
		return emailPosition;
	}

	/**
     * This method is used to get the subject of the email.
     *
     *
     * @param The email messages.
     * @return The email subject.
     */
	private static String getEmailSubjectTemp(Message message){

		String emailSubject = null;
		try {
			emailSubject = message.getSubject();
		} catch (Throwable e) {
			throw (RuntimeException) e;
		}
		return emailSubject;
	}

	/**
     * This method is used to get the value of From from message.
     *
     *
     * @param The message.
     * @return The value of From.
     */
	private static String getEmailFromTemp(Message message) {

		Address from[];
		String emailFrom = null;
		try {
			from = message.getFrom();
			emailFrom = from[0].toString();
			if(emailFrom.contains("<")){
				emailFrom = emailFrom.substring(emailFrom.indexOf("<")+1, emailFrom.indexOf(">"));
			}
		} catch (Throwable e) {
			throw (RuntimeException) e;
		}
		return emailFrom;
	}

	/**
     * This method is used to get the value of To from message.
     *
     *
     * @param The message.
     * @return The value of To.
     */
	private static String[] getEmailToTemp(Message message) {

		Address to[];
		String emailTo[] = null;
		int i = 0;
		try {
			to = message.getRecipients(RecipientType.TO);
			if(to != null){
				emailTo = new String[to.length];
				for(Address a: to){
					emailTo[i] = a.toString();
					if(emailTo[i].contains("<")){
						emailTo[i] = emailTo[i].substring(emailTo[i].indexOf("<")+1, emailTo[i].indexOf(">"));
						i++;
					}
				}
			}
		} catch (Throwable e) {
			throw (RuntimeException) e;
		}
		return emailTo;
	}

	/**
     * This method is used to get the value of cc from message.
     *
     *
     * @param The message.
     * @return The value of cc.
     */
	private static String[] getEmailCcTemp(Message message) {

		Address cc[];
		String emailCc[] = null;
		int i = 0;
		try {
			cc = message.getRecipients(RecipientType.CC);
			if(cc != null){
				emailCc = new String[cc.length];
				for(Address a: cc){
					emailCc[i] = a.toString();
					if(emailCc[i].contains("<")){
						emailCc[i] = emailCc[i].substring(emailCc[i].indexOf("<")+1, emailCc[i].indexOf(">"));
						i++;
					}
				}
			}
		} catch (Throwable e) {
			throw (RuntimeException) e;
		}
		return emailCc;
	}

	/**
     * This method is used to get the plain text from message.
     *
     *
     * @param The message.
     * @return The plain text.
     */
	private static String getEmailPlainTextTemp(Message message){

		String emailText = null;
		try{
			if(message.getContent() instanceof String){
				emailText = message.getContent().toString();
			}else{
				Multipart multipart = (Multipart) message.getContent();
				for (int x = 0; x < multipart.getCount(); x++){
					BodyPart bodyPart = multipart.getBodyPart(x);
					String disposition = bodyPart.getDisposition();
					if(disposition != null && (disposition.equals(BodyPart.ATTACHMENT))){
					}else{
						if(bodyPart.getContentType().toUpperCase().contains("TEXT/PLAIN")){
							emailText = bodyPart.getContent().toString();
							break;
						}
					}
				}
			}
		}catch(Throwable e){
			throw (RuntimeException) e;
		}
		return emailText;
	}
	
	/**
     * This method is used to get the HTML text from message.
     *
     *
     * @param The message.
     * @return The HTML text.
     */
	private static String getEmailHTMLTextTemp(Message message){

		String emailText = null;
		try{
			if(message.getContent() instanceof String){
				emailText = message.getContent().toString();
			}else{
				Multipart multipart = (Multipart) message.getContent();
				for (int x = 0; x < multipart.getCount(); x++){
					BodyPart bodyPart = multipart.getBodyPart(x);
					String disposition = bodyPart.getDisposition();
					if(disposition != null && (disposition.equals(BodyPart.ATTACHMENT))){
					}else{
						if (bodyPart.getContent() instanceof String) {
							if (bodyPart.getContentType().toUpperCase().contains("TEXT/HTML")) {
								emailText = bodyPart.getContent().toString();
								break;
							}
						} else {
							Multipart multipart1 = (Multipart) bodyPart.getContent();
							BodyPart bodyPart1 = multipart1.getBodyPart(0);
							if (bodyPart1.getContent() instanceof String) {
								if (bodyPart1.getContentType().toUpperCase().contains("TEXT/HTML")) {
									emailText = bodyPart1.getContent().toString();
									break;
								}
							}
						}

					}
				}
			}
		}catch(Throwable e){
			throw (RuntimeException) e;
		}
		return emailText;
	}
	
	/**
     * This method performs assertion on the email subject to be equal to the expected subject.
     *
     *
     * @param The message.
     * @param The expected email subject.
     */
	public static void assertEmailSubject(EmailMessage message, String expectedEmailSubject){
		Reporter.log("<br>Assert email subject [" + expectedEmailSubject + "]");
		String emailSubject = message.getSubject();
		Assertions.assertThat(emailSubject).isEqualToIgnoringCase(expectedEmailSubject.trim());
	}

	/**
     * This method performs assertion on the email subject to contain the expected subject.
     *
     *
     * @param The message.
     * @param The expected email subject.
     */
	public static void assertEmailSubjectContains(EmailMessage message, String expectedEmailSubject){
		Reporter.log("<br>Assert email subject contains [" + expectedEmailSubject + "]");
		String emailSubject = message.getSubject();
		Assertions.assertThat(emailSubject).containsIgnoringCase(expectedEmailSubject.trim());
	}

	/**
     * This method performs assertion on the value of the From field to be equal to the expected From value.
     *
     *
     * @param The message.
     * @param The expected value of the From field.
     */
	public static void assertEmailFrom(EmailMessage message, String expectedEmailFrom){
		Reporter.log("<br>Assert email from [" + expectedEmailFrom + "]");
		String emailFrom = message.getFrom();
		Assertions.assertThat(emailFrom.toLowerCase()).isEqualTo(expectedEmailFrom.toLowerCase().trim());
	}

	/**
     * This method performs assertion on the value of the To field to be equal to the expected To value.
     *
     *
     * @param The message.
     * @param The expected value of the To field.
     */
	public static void assertEmailTo(EmailMessage message, String ...expectedEmailTo){
		Reporter.log("<br>Assert email to [" + expectedEmailTo + "]");
		String emailTo[] = message.getTo();
		emailTo = convertStringArrayCase(emailTo, false);
		Arrays.sort(emailTo);

		expectedEmailTo = trimStringArray(expectedEmailTo);
		expectedEmailTo = convertStringArrayCase(expectedEmailTo, false);
		Arrays.sort(expectedEmailTo);

		Assertions.assertThat(emailTo).isEqualTo(expectedEmailTo);
	}

	/**
     * This method converts a stringArray to upper or lower case.
     *
     *
     * @param The stringArray.
     * @param The toUpper{1 for upper case, 0 for lower case}.
     * @return The processed stringArray
     */
	private static String[] convertStringArrayCase(String stringArray[], boolean toUpper){
		if(toUpper){
			for(int i = 0; i < stringArray.length; i++){
				stringArray[i] = stringArray[i].toUpperCase();
			}
		}else{
			for(int i = 0; i < stringArray.length; i++){
				stringArray[i] = stringArray[i].toLowerCase();
			}
		}
		return stringArray;
	}

	/**
     * This method trims a stringArray.
     *
     *
     * @param The stringArray.
     * @return @return The processed stringArray
     */
	private static String[] trimStringArray(String stringArray[]){
		for(int i = 0; i < stringArray.length; i++){
			stringArray[i] = stringArray[i].trim();
		}
		return stringArray;
	}

	/**
     * This method performs assertion on the value of the CC field to be equal to the expected CC value.
     *
     *
     * @param The message.
     * @param The expected value of the CC field.
     */
	public static void assertEmailCc(EmailMessage message, String ...expectedEmailCc){
		Reporter.log("<br>Assert email cc [" + expectedEmailCc + "]");
		String emailCc[] = message.getCc();
		emailCc = convertStringArrayCase(emailCc, false);
		Arrays.sort(emailCc);

		expectedEmailCc = trimStringArray(expectedEmailCc);
		expectedEmailCc = convertStringArrayCase(expectedEmailCc, false);
		Arrays.sort(expectedEmailCc);
		Assertions.assertThat(emailCc).isEqualTo(expectedEmailCc);
	}

	/**
     * This method performs assertion on the value of the BCC field to be equal to the expected BCC value.
     *
     *
     * @param The message.
     * @param The expected value of the BCC field.
     */
	public static void assertEmailBcc(EmailMessage message, String ...expectedEmailBcc){
		Reporter.log("<br>Assert email bcc [" + expectedEmailBcc + "]");
		String emailBcc[] = message.getBcc();
		emailBcc = convertStringArrayCase(emailBcc, false);
		Arrays.sort(emailBcc);

		expectedEmailBcc = trimStringArray(expectedEmailBcc);
		expectedEmailBcc = convertStringArrayCase(expectedEmailBcc, false);
		Arrays.sort(expectedEmailBcc);
		Assertions.assertThat(emailBcc).isEqualTo(expectedEmailBcc);
	}

	/**
     * This method is used to reply to a particular email.
     *
     *
     * @param The value of the From field.
     * @param The password.
     * @param The subject.
     * @param The body.
     */
	public static void replyEmail(String from, String password, String subject, String body){
		Reporter.log("<br>Reply to email");
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", ExecutionConfig.SMTP_HOSTNAME);
		props.put("mail.smtp.port", ExecutionConfig.SMTP_PORT);

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(ExecutionConfig.SMTP_USERNAME, ExecutionConfig.SMTP_PASSWORD);
					}
				});

		Message messages[];
		Folder inbox = null;
		Message replyToMessage = null;
		Store store = null;

		try{
			store = StoreType.valueOf(getDomain(from).toUpperCase()).getStore(from, password);
			store.connect(from, password);
			inbox = store.getFolder("inbox");
			inbox.open(Folder.READ_ONLY);
			messages = inbox.getMessages();
			if(messages.length == 0){
				throw new RuntimeException("Inbox empty.");
			}
			else{
				for(Message message: messages){
					if(message.getSubject().contains(subject)){
						replyToMessage = message;
						break;
					}
				}
			}

			String messageFrom = InternetAddress.toString(replyToMessage.getFrom());
			String messageTo = InternetAddress.toString(replyToMessage.getRecipients(Message.RecipientType.TO));
			String messageCC = InternetAddress.toString(replyToMessage.getRecipients(Message.RecipientType.CC));
			String messageText = getEmailHTMLTextTemp(replyToMessage);

			if(null == messageCC || messageCC.isEmpty()){
				messageCC = messageTo;
			}else{
				messageCC = messageCC + "," + messageTo;
			}

			String []ccList = messageCC.split(",");
			messageCC = "";
			for(String cc: ccList){
				if(!cc.toLowerCase().contains(from.toLowerCase())){
					messageCC = messageCC + cc + ", ";
				}
			}

			MimeMessage message2 = new MimeMessage(session);
			MimeMessage message3 = (MimeMessage) replyToMessage.reply(true);
			message2.setFrom(new InternetAddress(from));
			message2.addRecipients(RecipientType.TO, messageFrom);
			message2.addRecipients(RecipientType.CC, messageCC);
			message2.setSubject(message3.getSubject());
			message2.setContent(body + "<br/><br/>" + messageText, "text/html" );

			Transport.send(message2);

		}catch(Exception e){
			e.printStackTrace();
			throw (RuntimeException) e;
		}finally{
			try{
				if(inbox != null) {
					inbox.close(true);
				}
				if(store != null) {
					store.close();
				}
			}catch(Throwable e1){
				throw (RuntimeException) e1;
			}
		}
	}
}