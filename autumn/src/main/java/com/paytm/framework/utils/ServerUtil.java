package com.paytm.framework.utils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.HashMap;
import java.util.Map;

public class ServerUtil {

    private static Map<String, Session> sshMap = new HashMap<>();
    public Session getSession(String serverProperty){

        String[] schemeSplit = serverProperty.split(":");
        String host = schemeSplit[0];
        String user = schemeSplit[1];
        String password = schemeSplit[2];

        try {
            Session session1 = null;
            if (sshMap.containsKey(host + ":" + user)) {
                session1 = sshMap.get(host + ":" + user);
                if (session1.isConnected()) {
                    return session1;
                } else
                    sshMap.remove(host + ":" + user);
            }
            JSch jschObj = new JSch();
            session1 = jschObj.getSession(user, host);
            session1.setPassword(password);
            session1.setConfig("StrictHostKeyChecking", "no");
            session1.connect();
            sshMap.put(host + ":" + user, session1);
            return session1;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
