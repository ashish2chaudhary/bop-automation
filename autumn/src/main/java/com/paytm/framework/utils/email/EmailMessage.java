package com.paytm.framework.utils.email;

import com.paytm.framework.utils.CommonUtils;

/**
 * EmailMessage is used to set/get the content of the email.
 *
 */
public class EmailMessage {
	private String subject;
	private String from;
	private String to[];
	private String cc[];
	private String bcc[];
	private String plainText;
	private String htmlText;

	public EmailMessage(){
		this.subject = "";
		this.from = "";
		this.plainText = "";
		this.htmlText = "";
		this.to = new String[0];
		this.cc = new String[0];
		this.bcc = new String[0];
	}
	
	/**
     * This method returns the subject of the email.
     *
     * @return The subject of the email.
     */
	public String getSubject() {
		return subject;
	}

	/**
     * This method is used to set the subject of the email.
     *
     * @param The subject of the email.
     */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
     * This method returns the value of the from field of the email.
     *
     * @return The value of the from field of the email.
     */
	public String getFrom() {
		return from;
	}

	/**
     * This method is used to set the from field of the email.
     *
     * @param The value of the from field of the email.
     */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
     * This method returns the value of the to field of the email.
     *
     * @return The value of the to field of the email.
     */
	public String[] getTo() {
		return to;
	}

	/**
     * This method is used to set the value of the to field of the email.
     *
     * @param The value of the to field of the email.
     */
	public void setTo(String[] to) {
		this.to = to;
	}

	/**
     * This method returns the value of the cc field of the email.
     *
     * @return The value of the cc field of the email.
     */
	public String[] getCc() {
		return cc;
	}

	/**
     * This method is used to set the value of the cc field of the email.
     *
     * @param to- The value of the cc field of the email.
     */
	public void setCc(String[] cc) {
		this.cc = cc;
	}

	/**
     * This method returns the value of the bcc field of the email.
     *
     * @return The value of the bcc field of the email.
     */
	public String[] getBcc() {
		return bcc;
	}

	/**
     * This method is used to set the value of the bcc field of the email.
     *
     * @param to- The value of the bcc field of the email.
     */
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}

	/**
     * This method is used to get the plain text.
     *
     * @return The plain text.
     */
	public String getPlainText() {
		return plainText;
	}

	/**
     * This method is used to set the plain text.
     *
     * @param The plain text.
     */
	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

	/**
     * This method is used to get the html text.
     *
     * @return The html text.
     */
	public String getHtmlText() {
		return (CommonUtils.getCompressedHTML(htmlText));
	}

	/**
     * This method is used to set the html text.
     *
     * @param The html text.
     */
	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}

}
