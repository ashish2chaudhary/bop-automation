package test_scripts;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.paytm.framework.datareader.DataProviderParams;
import com.paytm.framework.datareader.DataReaderUtil;
import com.paytm.framework.ui.base.test.BaseTest;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.utils.CommonUtils;

import backofficepanel.module.AccountManagement_Module;
import backofficepanel.module.CommonModule;
import backofficepanel.module.LoginModule;
import backofficepanel.pages.AccountManagement_Page;
import backofficepanel.utilities.ReadXML;

public class AccountManagement extends BaseTest{
	private LoginModule loginModule;
	private CommonModule commonModule;
	private AccountManagement_Module acman_module;
<<<<<<< HEAD
	
=======

>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=AccountManagement_PresenceofElement" })
	@Test(description = "Verify elements present on Account Management page", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void presenceOfElementstest(String userName, String password,String credentialsStatus , String Searchmode , String value , String username , String mobile , String emailid)
<<<<<<< HEAD
	{
=======
	{		
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		loginModule = new LoginModule();
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		loginModule.login(userName, password);	
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		System.out.println("Actual :"+commonModule.confirmLoginStatus());
		System.out.println("Expected : "+credentialsStatus);
		Assert.assertEquals(credentialsStatus, commonModule.confirmLoginStatus());
		commonModule.search(Searchmode, value);
		acman_module.verifyAccountInfo(value,mobile,emailid);
	} 
	
=======
		commonModule.clickAccountManagementLink();
		System.out.println("Actual :"+commonModule.confirmLoginStatus());
		System.out.println("Expected : "+credentialsStatus);
		Assert.assertEquals(credentialsStatus, commonModule.confirmLoginStatus());
		commonModule.searchBy(Searchmode, value);
		acman_module.verifyAccountInfo(value,mobile,emailid);
	} 
	
	
	
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SAINF_AccountDetails_SavingAccount" })
	@Test(description = "Verify SAINF Account Saving Accounts  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingSainfAcc_Details(String username , String custid ,String mobile , String emailid ,
			String AC_No ,String bal , String AC_Type , String AC_Status , String branchname, 
			String branchid ,String branch_add ,String create_date , String update_date , 
			String see_deessed , String consent_stat,String NPCI_seed_date , 
			String reject , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(Searchmode, value);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifySAINFAcc_Details(username ,custid ,mobile ,emailid,
				AC_No,bal ,AC_Type,AC_Status ,branchname, branchid ,
				branch_add ,create_date  ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	} 
	
<<<<<<< HEAD
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({ "fileName=InputData.csv", "tableName=NomineeDetails" })
	@Test(description = "Verify SAINF Account  Nominee Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingNominee_Details(String nomineename , String nominee_id , String relation , String email,
			String DOB , String aadhar , String percent , String address , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(Searchmode, value);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifyNomineeDetail( nomineename ,  nominee_id ,  relation ,  email,
			 DOB ,  aadhar ,  percent ,  address);
	}
	
<<<<<<< HEAD
	
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SAINF_AccountDetails_SavingAccount" })
	@Test(description = "Verify SAIND Account Saving Accounts  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingSaindAcc_Details(String username , String custid , String mobile , String emailid ,
			String AC_No ,String bal , String AC_Type , 
			String AC_Status , String branchname, String branchid ,String branch_add ,
			String create_date , String update_date , String see_deessed , String consent_stat,
			String NPCI_seed_date , String reject , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(Searchmode, value);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifySAINFAcc_Details(username ,custid ,mobile ,emailid ,AC_No, bal , AC_Type, 
				AC_Status ,branchname, branchid ,branch_add ,create_date  ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	} 
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=KYC_Details" })
	@Test(description = "Verify KYC  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingKYCDetails(String kyc_stat , String kyc_type , String kyc_details , String pan_updated ,
			String pan_verified ,String aadhar_updated , String aadhar_verified , 
			String form_60 , String Searchmode , String value)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(Searchmode, value);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifyKYC_DetailsACMANMod(kyc_stat,kyc_type,kyc_details, pan_updated ,
				pan_verified,aadhar_updated ,aadhar_verified ,form_60 );
	} 
	
<<<<<<< HEAD
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({ "fileName=InputData.csv", "tableName=Actions_Debit_Card" })
	@Test(description = "Block/Unblock PDC ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingBlock_Unblock(String cust_id ,String debitcard_number , String alias_number , String variant
			, String qr_code , String awb , String pre_del , String delivery ,String order_id , String stat_nor ,
			String stat_tb,String stat_rb, String searchmode)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant);  		// Verifying when status is normal
		acman_module.clickActionLink();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_tb ,variant);	
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectUnblock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant); 
	}  
	
<<<<<<< HEAD
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Debit_Card" })
	@Test(description = "RestrictBlock/Unblock", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingRestrictBlock_Unblock(String cust_id ,String debitcard_number , String alias_number , String variant
			, String qr_code , String awb , String pre_del , String delivery ,String order_id , String stat_nor ,
			String stat_tb,String stat_rb, String searchmode)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant);  		// Verifying when status is normal
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectrestrictBlock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_rb ,variant);	
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectrestrictUnblock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant); 
	}
	
<<<<<<< HEAD
	
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Credit Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Credit_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(credit_freeze);
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}
	
<<<<<<< HEAD
	
	
	
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Debit Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Debit_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();	
		acman_module.typeInCommentSection_DebitActions();
		acman_module.openActionsDropdown();
		acman_module.selectDebitFreeze();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(debit_freeze);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}
	
<<<<<<< HEAD
	
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Total Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Total_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalFreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(total_freeze);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}  
	
<<<<<<< HEAD
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=VerifyBasicUserDetails" })
	@Test(description = "VerifyBasicUserDetails", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyBasicUserDetails(String gender ,String dob , String marital_status, String nationality ,String occupation,
			 String empstatus ,String pan_card ,String address ,String doctype , String issuanceCountry , String issuanceplace ,
			 String docnumber , String issuedate , String searchmode , String cust_id)
	{	
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.search(searchmode, cust_id);
		acman_module.clickMoreDetails();
		acman_module.verifyBasicInformation(gender, dob, marital_status, nationality, occupation, empstatus, pan_card, address);
		acman_module.verifyProofOfIdentity( doctype ,  issuanceCountry ,  issuanceplace , docnumber ,  issuedate);
		acman_module.click_less_details();
	}  
	
=======
		commonModule.clickAccountManagementLink();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickMoreDetails();
		acman_module.verifyBasicInformation(gender, dob, marital_status, nationality, occupation, empstatus, pan_card, address);
		acman_module.verifyProofOfIdentity( doctype ,  issuanceCountry ,  issuanceplace , docnumber ,  issuedate);
	}  
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=PredeliveryStatus" })
	@Test(description = "Verifying_PredeliveryStatus Debit Card", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifying_PredeliveryStatus(String cust_id,String searchmode , String pre_del_status)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+cust_id);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.clickDebitCardTab();
		acman_module.verifyPreDelStatus(pre_del_status);
		
		// FIS received remaining
	}  
<<<<<<< HEAD
	
=======

>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	@DataProviderParams({"fileName=InputData.csv", "tableName=FixedDepositCheck" })
	@Test(description = "Fixed Deposit Check", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyFixedDeposutCheck(String cust_id,String searchmode )
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+cust_id);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
<<<<<<< HEAD
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.search(searchmode, cust_id);
=======
		commonModule.clickAccountManagementLink();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		acman_module.clickFixedDepositTab();
		acman_module.verifyFixedDepositTable();
		
		// Add more against close nothing  amount is zero
<<<<<<< HEAD
	} 	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=Search_All" })
	@Test(description = "Verifying all types of search", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyFixedDeposutCheck(String value,String searchmode , String custid, String mobile ,
			String  Email , String acno)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		acman_module.verifyAccountInfo(custid,mobile ,Email);
	} 
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=DeseededAcccont" })
	@Test(description = "Verify desseded account information", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyDeseededAccountInfo(String username , String custid ,String mobile , String emailid ,
			String AC_No ,String bal , String AC_Type , String AC_Status , String branchname, 
			String branchid ,String branch_add ,String create_date , String update_date , 
			String see_deessed , String consent_stat,String NPCI_seed_date , 
			String reject , String searchmode , String value)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		acman_module.verifySAINFAcc_Details(username ,custid ,mobile ,emailid,
				AC_No,bal ,AC_Type,AC_Status ,branchname, branchid ,
				branch_add ,create_date  ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	} 
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=VerifyOnlyVDCisPresent" })
	@Test(description = "Verify user has only Digital Debit Card", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyDigitalDebitCard(String value ,String searchmode )
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		acman_module.clickDebitCardTab();
		acman_module.verifyPDCnotPresent();
	} 
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=VeriyAllSearchErrorMessage" })
	@Test(description = "Verify all search error message", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void veriyAllSearchErrorMessage(String searchmode ,String value ,String error )
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		commonModule.verifyErrorMessage(error);
	} 
	
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=CardBlockHistory" })
	@Test(description = "Verify Card Block History", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyCardBloackHistory(String searchmode ,String value)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		acman_module.clickDebitCardTab();
		acman_module.clickBlockHist();
		acman_module.presenceOfCardBlockHistTable();
		acman_module.verifyHeaderBlockHistTable();
		commonModule.paginationForward();
		acman_module.pause(5);
		acman_module.verifyHeaderBlockHistTable();
	} 
	
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=NoFD" })
	@Test(description = "Verify Customer with no FD", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyCustomerWithNoFD(String searchmode ,String value)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value);
		acman_module.clickFixedDepositTab();
		acman_module.noFDMessage();
	}
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=PanelClosingConnsecutiveSearch" })
	@Test(description = "verifyPanelOpeningClosingDiffAcc", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyViewBalanceFD(String searchmode ,String value1 , String value2)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value1);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value1);
		acman_module.pause(5);
		acman_module.openAllpanel();
		commonModule.search(searchmode, value2);
		acman_module.verifyAllPanelAreClosed();
	}  
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=PanelClosingConnsecutiveSearch" })
	@Test(description = "verifyPanelOpeningClosingSameAcc", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyPanelOpeningClosingSameAcc(String searchmode ,String value1 , String value2)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+value1);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, value1);
		acman_module.pause(8);
		acman_module.openAllpanel();
		commonModule.search(searchmode, value1);
		acman_module.verifyAllPanelAreClosed();
	}  
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SAIND_KYC" })
	@Test(description = "Verify Saind KYC  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingSAINDKYCDetails(String kyc_stat , String kyc_type , String kyc_details , String pan_updated ,
			String pan_verified ,String aadhar_updated , String aadhar_verified , 
			String form_60 , String Searchmode , String value)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);
		acman_module.verifyKYC_DetailsACMANMod(kyc_stat,kyc_type,kyc_details, pan_updated ,
				pan_verified,aadhar_updated ,aadhar_verified ,form_60 );
	} 


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*
	@DataProviderParams({ "fileName=InputData.csv", "tableName=KYC_Messages" })
	@Test(description = "Verify Saind KYC  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingKYCDetails(String Searchmode , String value ,String message)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(Searchmode, value);

	} 
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=RefreshComment" })
	@Test(description = "Block/Unblock PDC ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingBlock_Unblock(String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.search(searchmode, cust_id);
		acman_module.clickDebitCardTab();
		acman_module.clickActionLink();
		acman_module.pause(5);
		acman_module.typeInCommentSection_DebitActions();
		acman_module.pause(5);
		acman_module.openActionsDropdown();
		acman_module.pause(5);
		acman_module.selectUnblock();
		acman_module.pause(5);
		acman_module.verifycommentSectionISEmpty();
	}  
	*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} 
=======
	}   
}
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
