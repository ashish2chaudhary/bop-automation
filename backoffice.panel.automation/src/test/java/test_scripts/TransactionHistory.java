package test_scripts;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.paytm.framework.datareader.DataProviderParams;
import com.paytm.framework.datareader.DataReaderUtil;
import com.paytm.framework.ui.base.test.BaseTest;

import backofficepanel.module.CommonModule;
import backofficepanel.module.LoginModule;
import backofficepanel.module.TransactionHistoryModule;


public class TransactionHistory extends BaseTest {
	private LoginModule loginModule;
	private CommonModule commonModule;
	private TransactionHistoryModule transactionHistoryModule;
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=LoginData_Valid_Invalid" })
	@Test(description = "Performs login", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void loginTest(String userName, String password, String credentialsStatus) {
		loginModule = new LoginModule();
		commonModule = new CommonModule();
		transactionHistoryModule = new TransactionHistoryModule();
		loginModule.login(userName, password);	
		Assert.assertEquals(commonModule.confirmLoginStatus(), credentialsStatus);
	}	
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=FindTransactions_Data_CustomerID_MobileNo_AccountNo_EmailID" })
	@Test(description = "Find Transactions using Customer ID, Account No, Mobile No, Email ID", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingCustomerIDAccountNoMobileNoAndEmailID(String searchmode, String value, String fullName, String customerID, 
			String mobileNumber, String emailID)	{
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.verifyAccountDetails(fullName, customerID, mobileNumber, emailID);
		transactionHistoryModule.clickBalanceAndFDAmount();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.viewBalanceAndFDAmount();
		transactionHistoryModule.clickFilterButton();
		commonModule.setDateFilter("05/01/2018", "31/03/2018");
		transactionHistoryModule.verifyTransactionTableHeaderForGeneralSearch("ID", "SERIAL NO.", "DATE", "REMARK", "AMOUNT", "MODE", 
				"EXTERNAL ID", "STATUS");
		transactionHistoryModule.assertTransactionTableDoesNotContain("No Records found!");
		/* todo: Add code to check download functionality */
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=FindTransactions_Data_RRN" })
	@Test(description = "Find Transactions using RRN", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingRRN(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.verifyTransactionTableHeaderForRRNSearch("CUSTOMER NAME", "CUSTOMER ID", "ID", "SERIAL NO.", 
				"DATE", "REMARK", "AMOUNT", "MODE", "EXTERNAL ID", "STATUS");
		transactionHistoryModule.assertTransactionTableDoesNotContain("No Records found!");
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=FindTransactions_Data_TransactionID" })
	@Test(description = "Find Transactions using Transaction ID", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingTransactionID(String transactionID, String serialNo, String date) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchByTransactionID(transactionID, serialNo, date);
		transactionHistoryModule.verifyTransactionTableHeaderForTransactionIDSearch("ACCOUNT ID", "ID", "SERIAL NO.", 
				"DATE", "REMARK", "AMOUNT", "MODE", "EXTERNAL ID", "STATUS");
		transactionHistoryModule.assertTransactionTableDoesNotContain("No Records found!");
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=FindTransactions_InvalidData" })
	@Test(description = "Find Transactions using invalid data", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingInvalidData(String searchBy, String value, String errorMessage) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchBy, value);
		commonModule.verifyErrorMessage(errorMessage);
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=FindTransactions_InvalidTransactionData" })
	@Test(description = "Find Transactions using invalid transaction data", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingInvalidTransactionData(String transactionID, String serialNo, String date, String errorMessage) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchByTransactionID(transactionID, serialNo, date);
		commonModule.verifyErrorMessage(errorMessage);
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=AdvanceSearch_VisibleInvisible" })
	@Test(description = "Checks that the Advance Search option is visible/invisible", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  checkAdvanceSearch(String searchBy, String text) {
		commonModule.clickTransactionHistoryLink();
		commonModule.selectSearchBy(searchBy);
		commonModule.checkAdvanceSearchIsVisible(text);
	}
	
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=Search_Placeholder" })
	@Test(description = "Verifies the different placeholders for different search options", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifySearchPlaceholders(String searchBy, String placeholderText) {
		commonModule.clickTransactionHistoryLink();
		commonModule.selectSearchBy(searchBy);
		commonModule.checkSearchBarPlaceholderText(placeholderText);
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=Search_Placeholder_TransactionID" })
	@Test(description = "Verifies the different placeholders for Transaction ID search option", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifySearchPlaceholdersForTransactionID(String searchBy, String transactionIDPlaceholder, String serialNoPlaceholder, 
			String datePlaceholder) {
		commonModule.clickTransactionHistoryLink();
		commonModule.selectSearchBy(searchBy);
		commonModule.checkTransactionIDSearchBarsPlaceholdersText(transactionIDPlaceholder, serialNoPlaceholder, datePlaceholder);
	}
	
	@Test(description = "Verifies the UI of the Advance Search")
	public void  verifyAdvanceSearchUI() {
		commonModule.clickTransactionHistoryLink();
		transactionHistoryModule.clickAdvanceSearch();
		transactionHistoryModule.verifyTransactionDateHeader("Transaction Date");
		commonModule.verifyDateFilter("01/02/2018", "31/03/2018");
		transactionHistoryModule.verifyTransactionTypesSection("Transaction Types", "All", "Credit", "Debit");
		transactionHistoryModule.verifyTransactionModesSection("Transaction Modes", "Debit Card", "Net Banking", "IMPS", "NEFT", "RTGS", 
				"UPI", "SLFD", "BC App", "POS", "ATM/Recycler", "NACH", "APY");
		transactionHistoryModule.verifyAdHocReportCodesSection("Ad-Hoc Report Codes", "Service Tax", "TDS", "GST", "Interest Received", 
				"NEFT Income", "RTGS Charges", "Salary", "Flexi Salary", "Reimbursement");	
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SearchByCustomerID" })
	@Test(description = "Verifies the UI of the Rearrange Table Coumns", dataProviderClass = DataReaderUtil.class, 
			dataProvider = "CsvDataProvider")
	public void  verifyRearrangeTableColumns(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.verifyRearrangeTableColumnsSection("ID", "SERIAL NO.", "DATE", "REMARK", "AMOUNT", "MODE", "EXTERNAL ID", 
				"STATUS", "No column has been selected");
		transactionHistoryModule.verifyTransactionTableHeaderForGeneralSearch("ID", "SERIAL NO.", "DATE", "REMARK", "AMOUNT", "MODE", 
				"EXTERNAL ID", "STATUS");
		transactionHistoryModule.verifyRearrangeTableFewColumnsSection("ID", "SERIAL NO.", "DATE", "REMARK");
		transactionHistoryModule.verifyTransactionTableHeader("Amount", "MODE", "EXTERNAL ID", "STATUS");
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SearchByCustomerID" })
	@Test(description = "Find Transactions using Customer ID and advance search options", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  findTransactionsUsingCustomerIDAndAdvanceSearchOptions(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.clickFilterButton();
		commonModule.setDateFilter("05/01/2018", "31/03/2018");
		transactionHistoryModule.selectTransactionModes("Debit Card");
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.assertTransactionTableContains("No Records found!");
		transactionHistoryModule.assertDownloadButtonIsNotPresent();
		transactionHistoryModule.selectTransactionModes("Net Banking");
		transactionHistoryModule.assertTransactionTableDoesNotContain("No Records found!");
		transactionHistoryModule.assertDownloadButtonIsPresent();
		//transactionHistoryModule.verifyModeOfTransactionsInColumn(5, "E-comm");
		commonModule.assertTotalPagesCount((transactionHistoryModule.getTransactions_TotalNoOfSearchResults()/50)+1);
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=VerifyTransactionData" })
	@Test(description = "Verfies Transaction Details after clicking on the Transaction ID Link", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifyTransactionDetails(String transactionID, String serialNo, String date, String postedDateValue, 
			String valueDateValue, String descriptionValue, String rrnValue, String utrValue, String cifIDValue, String cbsPartTranTypeValue, 
			String reportCordValue, String remitterNameValue, String remitterAccountNumberValue, String referenceNumberValue, 
			String remitterIFSCValue, String beneficiaryIFSCValue, String beneficiaryAccountNumberValue, String beneficiaryNameValue, 
			String merchantNameValue, String merchantIDValue, String remittanceMMIDValue, String dccIDValue, 
			String merchantLocationValue, String dcMerchantIDValue) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchByTransactionID(transactionID, serialNo, date);
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.clickViewAmount();
		transactionHistoryModule.clickTransactionID(0, 1);
		transactionHistoryModule.verifyTransactionDetails("Transaction Details ( ID: " + transactionID + " )", "ID", "Posted Date", "Value Date", 
				"Description", "RRN", "UTR", "Cif ID", "CBS Part Tran Type", "Report Code", "User Details", "Remitter Name", "Remitter Account Number", 
				"Reference Number", "Remitter IFSC", "Beneficiary IFSC", "Beneficiary Account Number", "Beneficiary Name", "Merchant Details", 
				"Merchant Name", "Merchant ID", "Remittance MMID", "DCCID", "Merchant Location", "DC Merchant ID", transactionID, postedDateValue, 
				valueDateValue, descriptionValue, rrnValue, utrValue, cifIDValue, cbsPartTranTypeValue, reportCordValue, remitterNameValue, 
				remitterAccountNumberValue, referenceNumberValue, remitterIFSCValue, beneficiaryIFSCValue, beneficiaryAccountNumberValue, 
				beneficiaryNameValue, merchantNameValue, merchantIDValue, remittanceMMIDValue, dccIDValue, merchantLocationValue, 
				dcMerchantIDValue, "Ok");
		transactionHistoryModule.checkViewAmount();	
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SearchByCustomerID" })
	@Test(description = "Verifies Pagination for Transactions tab", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifyTransactionsPagination(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.clickFilterButton();
		commonModule.setDateFilter("01/01/2018", "31/03/2018");
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyTransactionsTableAmountColumn("View Amount");
		commonModule.verifyPageNo("1");
		commonModule.clickNextPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyTransactionsTableAmountColumn("View Amount");
		commonModule.verifyPageNo("2");
		commonModule.clickNextPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyTransactionsTableAmountColumn("View Amount");
		commonModule.verifyPageNo("3");
		commonModule.clickPreviousPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyTransactionsTableAmountColumn("View Amount");
		commonModule.verifyPageNo("2");
		commonModule.clickPreviousPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyTransactionsTableAmountColumn("View Amount");
		commonModule.verifyPageNo("1");
		commonModule.clickPreviousPage();
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SearchByCustomerID" })
	@Test(description = "Verifies the Failed/Pending Transactions tab", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifyFailedPendingTransactions(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.clickOnFailedPendingTransactionsTab();
		commonModule.setDateFilter("01/01/2018", "31/03/2018");
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.verifyFailedPendingTransactionTableHeader("CUSTOMER ID", "EXTERNAL ID", "INTERNAL ID", "TRANSACTION TYPE","SUB TRANSACTION TYPE",
				"AMOUNT", "RESPONSE CODE", "TRANSACTION STATUS", "CREATION DATE", "MODIFIED DATE", "SETTLED DATE", "OTHER DETAILS");
		transactionHistoryModule.checkFailedPendinTransactionsAmount();		
		commonModule.assertTotalPagesCount((transactionHistoryModule.getFailedPendingTransactions_TotalNoOfSearchResults()/20)+1);
	}
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SearchByCustomerID" })
	@Test(description = "Verifies Pagination for Failed/Pending Transactions tab", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  verifyFailedPendingTransactionsPagination(String searchmode, String value) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, value);
		transactionHistoryModule.clickOnFailedPendingTransactionsTab();
		commonModule.setDateFilter("01/01/2018", "31/03/2018");
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.checkFailedPendinTransactionsAmount();	
		commonModule.verifyPageNo("1");
		commonModule.clickNextPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.checkFailedPendinTransactionsAmount();	
		commonModule.verifyPageNo("2");
		commonModule.clickNextPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.checkFailedPendinTransactionsAmount();	
		commonModule.verifyPageNo("3");
		commonModule.clickPreviousPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.checkFailedPendinTransactionsAmount();	
		commonModule.verifyPageNo("2");
		commonModule.clickPreviousPage();
		commonModule.waitForLoaderToDisappear();
		transactionHistoryModule.checkFailedPendinTransactionsAmount();	
		commonModule.verifyPageNo("1");
		commonModule.clickPreviousPage();
	}
	
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=ConsecutiveSearch" })
	@Test(description = "Verifies search results based on consecutive search", 
	dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  consecutiveSearch(String searchmode, String valueFirstSearch, String fullNameFirstSearch, String customerIDFirstSearch,
			String mobileNumberFirstSearch, String emailIDFirstSearch, String valueSecondSearch, String fullNameSecondSearch, 
			String customerIDSecondSearch, String mobileNumberSecondSearch, String emailIDSecondSearch) {
		commonModule.clickTransactionHistoryLink();
		commonModule.searchBy(searchmode, valueFirstSearch);
		transactionHistoryModule.verifyAccountDetails(fullNameFirstSearch, customerIDFirstSearch, mobileNumberFirstSearch, emailIDFirstSearch);
		commonModule.clearSearchBoxAndType(valueSecondSearch);
		transactionHistoryModule.verifyAccountDetails(fullNameSecondSearch, customerIDSecondSearch, mobileNumberSecondSearch, emailIDSecondSearch);
	}
	
	
}
