package backofficepanel.pages;

import java.util.NoSuchElementException;

import javax.sound.midi.VoiceStatus;

import org.apache.commons.io.serialization.ValidatingObjectInputStream;
import org.fest.assertions.api.Assertions;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import com.paytm.framework.ui.base.page.BasePage;
import com.paytm.framework.ui.base.page.BaseWebPage;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.ui.element.ElementList;
import com.paytm.framework.ui.element.Label;
import com.paytm.framework.ui.element.Link;
import com.paytm.framework.ui.element.Table;

import backofficepanel.appconstants.Constants.PagePath;
import backofficepanel.automation.LocalConfig;
import backofficepanel.utilities.ReadXML;


public class TransactionHistoryPage extends BasePage{

	private Button transactionsTabButton;
	private Button failedPendingTransactionsTabButton;
	private Link viewBalanceLink;
	private Link viewFDAmountLink;
	private Label savingsAccountBalanceLabel;
	private Label fdAmountLabel;
	private Button filterButton;
	private Button downloadButton;
	private Label fullNameLabel;
	private Label customerIDLabel;
	private Label mobileNumberLabel;
	private Label emailIDLabel;
	private Table transactionsTable;
	private Link advanceSearchLink;	
	private Label transactionDateLabel;
	private Button transactionType_AllButton;
	private Button transactionType_CreditButton;
	private Button transactionType_DebitButton;
	private Label transactionTypesLabel;
	private Label transactionModesLabel;
	private Button transactionModes_DebitCardButton;
	private Button transactionModes_NetBankingButton;
	private Button transactionModes_IMPSButton;
	private Button transactionModes_NEFTButton;
	private Button transactionModes_RTGSButton;
	private Button transactionModes_UPIButton;
	private Button transactionModes_SLFDButton;
	private Button transactionModes_BCAppButton;
	private Button transactionModes_POSButton;
	private Button transactionModes_ATMRecyclerButton;
	private Button transactionModes_NACHButton;
	private Button transactionModes_APYButton;
	private Label adHocReportCodesLabel;
	private Button adHocReportCodes_ServiceTaxButton;
	private Button adHocReportCodes_TDSButton;
	private Button adHocReportCodes_GSTButton;
	private Button adHocReportCodes_InterestReceivedButton;
	private Button adHocReportCodes_NEFTIncomeButton;
	private Button adHocReportCodes_RTGSChargesButton;
	private Button adHocReportCodes_SalaryButton; 
	private Button adHocReportCodes_FlexiSalaryButton;
	private Button adHocReportCodes_ReimbursementButton;
	private Button rearrangeTableColumnsButton;
	private ElementList rearrangeTableColumnsOptions;
	private Button rearrangeTableColumnsApplyButton;
	private Button rearrangeTableColumnsResetButton;
	private Label rearrangeTableColumnsErrorLabel;
	private Label transaction_TransactionDetailsHeaderLabel;
	private ElementList transaction_TransactionDetailsElementList;
	private Label transaction_UserDetailsHeaderLabel;
	private ElementList transaction_UserDetailsElementList;
	private Label transaction_MerchantDetailsHeaderLabel;
	private ElementList transaction_MerchantDetailsElementList;
	private Button transaction_OkButton;
	private Label transactions_ResultsFoundLabel;
	private Label failedPendingTransactions_ResultsFoundLabel;
	
	
	public TransactionHistoryPage() {
		super(TransactionHistoryPage.class.getSimpleName()); 
        this.pageURL = LocalConfig.BOP_URL + PagePath.TRANSACTIONHISTORY_PATH;
        System.out.println(this.pageURL);
        transactionsTabButton = new Button(ReadXML.getElementLocator(getPageName(), "TransactionsTab"), getPageName(), "TransactionsTab");
        failedPendingTransactionsTabButton = new Button(ReadXML.getElementLocator(getPageName(), "FailedPendingTransactionsTab"), getPageName(), "FailedPendingTransactionsTab");
        viewBalanceLink = new Link(ReadXML.getElementLocator(getPageName(), "ViewBalance"), getPageName(), "ViewBalance");
        viewFDAmountLink = new Link(ReadXML.getElementLocator(getPageName(), "ViewFDAmount"), getPageName(), "ViewFDAmount");
        savingsAccountBalanceLabel = new Label(ReadXML.getElementLocator(getPageName(), "SavingsAccountBalance"), getPageName(), "SavingsAccountBalance");
        fdAmountLabel = new Label(ReadXML.getElementLocator(getPageName(), "FDAmount"), getPageName(), "fdAmount");        
        downloadButton = new Button(ReadXML.getElementLocator(getPageName(), "Download"), getPageName(), "Download");
        filterButton = new Button(ReadXML.getElementLocator(getPageName(), "Filter"), getPageName(), "Filter");
        fullNameLabel = new Label(ReadXML.getElementLocator(getPageName(), "FullName"), getPageName(), "FullName");
        customerIDLabel = new Label(ReadXML.getElementLocator(getPageName(), "CustomerID"), getPageName(), "CustomerID");
        mobileNumberLabel = new Label(ReadXML.getElementLocator(getPageName(), "MobileNumber"), getPageName(), "MobileNumber");
        emailIDLabel = new Label(ReadXML.getElementLocator(getPageName(), "EmailID"), getPageName(), "EmailID");
        transactionsTable = new Table(ReadXML.getElementLocator(getPageName(), "Transactions"), getPageName(), "Transactions");
        advanceSearchLink = new Link(ReadXML.getElementLocator(getPageName(), "AdvanceSearch"), getPageName(), "AdvanceSearch");
        transactionDateLabel = new Label(ReadXML.getElementLocator(getPageName(), "TransactionDate"), getPageName(), "TransactionDate");
        transactionTypesLabel = new Label(ReadXML.getElementLocator(getPageName(), "TransactionTypes"), getPageName(), "TransactionTypes");
        transactionType_AllButton = new Button(ReadXML.getElementLocator(getPageName(), "TransactionType_All"), getPageName(), "TransactionType_All");
        transactionType_CreditButton = new Button(ReadXML.getElementLocator(getPageName(), "TransactionType_Credit"), getPageName(), "TransactionType_Credit");
        transactionType_DebitButton = new Button(ReadXML.getElementLocator(getPageName(), "TransactionType_Debit"), getPageName(), "TransactionType_Debit");
        transactionModesLabel = new Label(ReadXML.getElementLocator(getPageName(), "TransactionModes"), getPageName(), "TransactionModes");
        transactionModes_DebitCardButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_DebitCard"), getPageName(), "TransactionModes_DebitCard");
        transactionModes_NetBankingButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_NetBanking"), getPageName(), "TransactionModes_NetBanking");
        transactionModes_IMPSButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_IMPS"), getPageName(), "TransactionModes_IMPS");
        transactionModes_NEFTButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_NEFT"), getPageName(), "TransactionModes_NEFT");
        transactionModes_RTGSButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_RTGS"), getPageName(), "TransactionModes_RTGS");
        transactionModes_UPIButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_UPI"), getPageName(), "TransactionModes_UPI");
        transactionModes_SLFDButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_SLFD"), getPageName(), "TransactionModes_SLFD");
        transactionModes_BCAppButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_BCApp"), getPageName(), "TransactionModes_BCApp");
        transactionModes_POSButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_POS"), getPageName(), "TransactionModes_POS");
        transactionModes_ATMRecyclerButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_ATMRecycler"), getPageName(), "TransactionModes_ATMRecycler");
        transactionModes_NACHButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_NACH"), getPageName(), "TransactionModes_NACH");
        transactionModes_APYButton= new Button(ReadXML.getElementLocator(getPageName(), "TransactionModes_APY"), getPageName(), "TransactionModes_APY");
        adHocReportCodesLabel= new Label(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes"), getPageName(), "AdHocReportCodes");
        adHocReportCodes_ServiceTaxButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_ServiceTax"), getPageName(), "AdHocReportCodes_ServiceTax");
        adHocReportCodes_TDSButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_TDS"), getPageName(), "AdHocReportCodes_TDS");
        adHocReportCodes_GSTButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_GST"), getPageName(), "AdHocReportCodes_GST");
        adHocReportCodes_InterestReceivedButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_InterestReceived"), getPageName(), "AdHocReportCodes_InterestReceived");
        adHocReportCodes_NEFTIncomeButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_NEFTIncome"), getPageName(), "AdHocReportCodes_NEFTIncome");
        adHocReportCodes_RTGSChargesButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_RTGSCharges"), getPageName(), "AdHocReportCodes_RTGSCharges");
        adHocReportCodes_SalaryButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_Salary"), getPageName(), "AdHocReportCodes_Salary");
        adHocReportCodes_FlexiSalaryButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_FlexiSalary"), getPageName(), "AdHocReportCodes_FlexiSalary");
        adHocReportCodes_ReimbursementButton= new Button(ReadXML.getElementLocator(getPageName(), "AdHocReportCodes_Reimbursement"), getPageName(), "AdHocReportCodes_Reimbursement");
        rearrangeTableColumnsButton = new Button(ReadXML.getElementLocator(getPageName(), "RearrangeTableColumns"), getPageName(), "RearrangeTableColumns");
        rearrangeTableColumnsOptions = new ElementList(ReadXML.getElementLocator(getPageName(), "RearrangeTableColumnsOptions"), getPageName(), "RearrangeTableColumnsOptions");
        rearrangeTableColumnsApplyButton = new Button(ReadXML.getElementLocator(getPageName(), "RearrangeTableColumnsApply"), getPageName(), "RearrangeTableColumnsApply");
        rearrangeTableColumnsResetButton = new Button(ReadXML.getElementLocator(getPageName(), "RearrangeTableColumnsReset"), getPageName(), "RearrangeTableColumnsReset");
        rearrangeTableColumnsErrorLabel = new Label(ReadXML.getElementLocator(getPageName(), "RearrangeTableColumnsError"), getPageName(), "RearrangeTableColumnsError");
        transaction_TransactionDetailsHeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "Transaction_TransactionDetailsHeader"), getPageName(), "Transaction_TransactionDetailsHeader");
        transaction_TransactionDetailsElementList = new ElementList(ReadXML.getElementLocator(getPageName(), "Transaction_TransactionDetails"), getPageName(), "Transaction_TransactionDetails");
        transaction_UserDetailsHeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "Transaction_UserDetailsHeader"), getPageName(), "Transaction_UserDetailsHeader");
        transaction_UserDetailsElementList = new ElementList(ReadXML.getElementLocator(getPageName(), "Transaction_UserDetails"), getPageName(), "Transaction_UserDetails");
        transaction_MerchantDetailsHeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "Transaction_MerchantDetailsHeader"), getPageName(), "Transaction_MerchantDetailsHeader");
        transaction_MerchantDetailsElementList = new ElementList(ReadXML.getElementLocator(getPageName(), "Transaction_MerchantDetails"), getPageName(), "Transaction_MerchantDetails");
        transaction_OkButton = new Button(ReadXML.getElementLocator(getPageName(), "Transaction_Ok"), getPageName(), "Transaction_Ok");
        transactions_ResultsFoundLabel = new Label(ReadXML.getElementLocator(getPageName(), "Transactions_ResultsFound"), getPageName(), "Transactions_ResultsFound");
        failedPendingTransactions_ResultsFoundLabel = new Label(ReadXML.getElementLocator(getPageName(), "FailedPendingTransactions_ResultsFound"), getPageName(), "FailedPendingTransactions_ResultsFound");
	}
	
	public void clickViewBalanceLink() {
		viewBalanceLink.click();
	}
	
	public void clickViewFDAmountLink() {
		viewFDAmountLink.click();
	}
	
	public void clickFilterButton() {
		filterButton.click();
	}
	
	public void assertFullNameLabelText(String text) {
		fullNameLabel.assertText(text);
	}
	
	public void assertCustomerIDLabelText(String text) {
		customerIDLabel.assertText(text);
	}
	
	public void assertMobileNumberLabelText(String text) {
		mobileNumberLabel.assertText(text);
	}
	
	public void assertEmailIDLabelText(String text) {
		emailIDLabel.assertText(text);
	}
	
	public int getTransactionsTableRowCount() {
		return transactionsTable.getBodyRowCount();
	}
	
	public String getTransactionsTableCellText(int rowIdx, int colIdx, String section) {
		return transactionsTable.getCellTextAtIndex(rowIdx, colIdx, section);
	}
	
	public void assertTransactionsTableText(int rowIdx, int colIdx, String section, String text) {
		transactionsTable.assertCellText(rowIdx, colIdx, section, text);
	}
	
	public void assertTransactionsTableContainsText(int rowIdx, int colIdx, String section, String text) {
		transactionsTable.assertCellContainsText(rowIdx, colIdx, section, text);
	}
	
	public void assertTransactionsTableDoesNotContainText(int rowIdx, int colIdx, String section, String text) {
		transactionsTable.assertCellDoesNotContainText(rowIdx, colIdx, section, text);
	}
	
	public void clickTransactionTableCell(int rowIdx, int colIdx, String section) {
		transactionsTable.clickCell(rowIdx, colIdx, section);
	}
	
	public void clickAdvanceSearchLink() {
		advanceSearchLink.click();
	}
	
	public void assertTransactionDateLabelText(String text) {
		transactionDateLabel.assertText(text);
	}
	
	public void assertTransactionModesLabelText(String text) {
		transactionModesLabel.assertText(text);
	}
	
	public void assertTransactionTypesLabelText(String text) {
		transactionTypesLabel.assertText(text);
	}
	
	public void assertAdHocReportCodesLabelText(String text) {
		adHocReportCodesLabel.assertText(text);
	}
	
	public void selectTransactionType_AllButton() {
		transactionType_AllButton.click();
	}
	
	public void selectTransactionType_CreditButton() {
		transactionType_CreditButton.click();
	}
	
	public void selectTransactionType_DebitButton() {
		transactionType_DebitButton.click();
	}
	
	public void assertTransactionType_AllButtonText(String text) {
		transactionType_AllButton.assertText(text);
	}
	
	public void assertTransactionType_CreditButtonText(String text) {
		transactionType_CreditButton.assertText(text);
	}
	
	public void assertTransactionType_DebitButtonText(String text) {
		transactionType_DebitButton.assertText(text);
	}
	
	public void selectTransactionModes_DebitCardButton() {
		transactionModes_DebitCardButton.click();
	}
	
	public void selectTransactionModes_NetBankingButton() {
		transactionModes_NetBankingButton.click();
	}
	
	public void selectTransactionModes_IMPSButton() {
		transactionModes_IMPSButton.click();
	}
	
	public void selectTransactionModes_NEFTButton() {
		transactionModes_NEFTButton.click();
	}
	
	public void selectTransactionModes_RTGSButton() {
		transactionModes_RTGSButton.click();
	}
	
	public void selectTransactionModes_UPIButton() {
		transactionModes_UPIButton.click();
	}
	
	public void selectTransactionModes_SLFDButton() {
		transactionModes_SLFDButton.click();
	}
	
	public void selectTransactionModes_BCAppButton() {
		transactionModes_BCAppButton.click();
	}
	
	public void selectTransactionModes_POSButton() {
		transactionModes_POSButton.click();
	}
	
	public void selectTransactionModes_ATMRecyclerButton() {
		transactionModes_ATMRecyclerButton.click();
	}
	
	public void selectTransactionModes_NACHButton() {
		transactionModes_NACHButton.click();
	}
	
	public void selectTransactionModes_APYButton() {
		transactionModes_APYButton.click();
	}
	
	public void assertTransactionModes_DebitCardButtonText(String text) {
		transactionModes_DebitCardButton.assertText(text);
	}
	
	public void assertTransactionModes_NetBankingButtonText(String text) {
		transactionModes_NetBankingButton.assertText(text);
	}
	
	public void assertTransactionModes_IMPSButtonText(String text) {
		transactionModes_IMPSButton.assertText(text);
	}
	
	public void assertTransactionModes_NEFTButtonText(String text) {
		transactionModes_NEFTButton.assertText(text);
	}
	
	public void assertTransactionModes_RTGSButtonText(String text) {
		transactionModes_RTGSButton.assertText(text);
	}
	
	public void assertTransactionModes_UPIButtonText(String text) {
		transactionModes_UPIButton.assertText(text);
	}
	
	public void assertTransactionModes_SLFDButtonText(String text) {
		transactionModes_SLFDButton.assertText(text);
	}
	
	public void assertTransactionModes_BCAppButtonText(String text) {
		transactionModes_BCAppButton.assertText(text);
	}
	
	public void assertTransactionModes_POSButtonText(String text) {
		transactionModes_POSButton.assertText(text);
	}
	
	public void assertTransactionModes_ATMRecyclerButtonText(String text) {
		transactionModes_ATMRecyclerButton.assertText(text);
	}
	
	public void assertTransactionModes_NACHButtonText(String text) {
		transactionModes_NACHButton.assertText(text);
	}
	
	public void assertTransactionModes_APYButtonText(String text) {
		transactionModes_APYButton.assertText(text);
	}
	
	public void selectAdHocReportCodes_ServiceTaxButton() {
		adHocReportCodes_ServiceTaxButton.click();
	}
	
	public void selectAdHocReportCodes_TDSButton() {
		adHocReportCodes_TDSButton.click();
	}
	
	public void selectAdHocReportCodes_GSTButton() {
		adHocReportCodes_GSTButton.click();
	}
	
	public void selectAdHocReportCodes_InterestReceivedButton() {
		adHocReportCodes_InterestReceivedButton.click();
	}
	
	public void selectAdHocReportCodes_NEFTIncomeButton() {
		adHocReportCodes_NEFTIncomeButton.click();
	}
	
	public void selectAdHocReportCodes_RTGSChargesButton() {
		adHocReportCodes_RTGSChargesButton.click();
	}
	
	public void selectAdHocReportCodes_SalaryButton() {
		adHocReportCodes_SalaryButton.click();
	}
	
	public void selectAdHocReportCodes_FlexiSalaryButton() {
		adHocReportCodes_FlexiSalaryButton.click();
	}
	
	public void selectAdHocReportCodes_ReimbursementButton() {
		adHocReportCodes_ReimbursementButton.click();
	}
	
	public void assertAdHocReportCodes_ServiceTaxButtonText(String text) {
		adHocReportCodes_ServiceTaxButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_TDSButtonText(String text) {
		adHocReportCodes_TDSButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_GSTButtonText(String text) {
		adHocReportCodes_GSTButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_InterestReceivedButtonText(String text) {
		adHocReportCodes_InterestReceivedButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_NEFTIncomeButtonText(String text) {
		adHocReportCodes_NEFTIncomeButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_RTGSChargesButtonText(String text) {
		adHocReportCodes_RTGSChargesButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_SalaryButtonText(String text) {
		adHocReportCodes_SalaryButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_FlexiSalaryButtonText(String text) {
		adHocReportCodes_FlexiSalaryButton.assertText(text);
	}
	
	public void assertAdHocReportCodes_ReimbursementButtonText(String text) {
		adHocReportCodes_ReimbursementButton.assertText(text);
	}
	
	public void clickRearrangeTableColumnsButton() {
		rearrangeTableColumnsButton.click();
	}
	
	public void clickRearrangeTableColumnsOptionsByIndex(int idx) {
		rearrangeTableColumnsOptions.clickElement(idx);
	}
	
	public void clickRearrangeTableColumnsOptionsByText(String text) {
		rearrangeTableColumnsOptions.clickElement(text);
	}
	
	public void assertRearrangeTableColumnsOptionsText(String text) {
		rearrangeTableColumnsOptions.assertListContainsText(text);
	}
	
	public void clickRearrangeTableColumnsApplyButton() {
		rearrangeTableColumnsApplyButton.click();
	}
	
	public void clickRearrangeTableColumnsResetButton() {
		rearrangeTableColumnsResetButton.click();
	}
	
	public void assertRearrangeTableColumnsErrorLabelText(String text) {
		rearrangeTableColumnsErrorLabel.assertText(text);
	}

	public void assertDownloadButtonIsNotPresent() {
		downloadButton.assertIsNotPresent();
	}
	
	public void assertDownloadButtonIsPresent() {
		downloadButton.assertIsPresent();
	}
	
	public void assertTransaction_TransactionDetailsHeaderLabelText(String text) {
		transaction_TransactionDetailsHeaderLabel.assertText(text);
	}
	
	public void assertTransaction_UserDetailsHeaderLabelText(String text) {
		transaction_UserDetailsHeaderLabel.assertText(text);
	}
	
	public void assertTransaction_MerchantDetailsHeaderLabelText(String text) {
		transaction_MerchantDetailsHeaderLabel.assertText(text);
	}
	
	public void clickTransaction_OkButton() {
		transaction_OkButton.click();
	}
	
	public void assertTransaction_OkButtonText(String text) {
		transaction_OkButton.assertText(text);
	}
	
	public void assertTransaction_TransactionDetailsElementListElementContainsText(Integer idx, String text) {
		transaction_TransactionDetailsElementList.assertListContainsText(idx, text);
	}
	
	public void assertTransaction_UserDetailsElementListElementContainsText(Integer idx, String text) {
		transaction_UserDetailsElementList.assertListContainsText(idx, text);
	}
	
	public void assertTransaction_MerchantDetailsElementListElementContainsText(Integer idx, String text) {
		transaction_MerchantDetailsElementList.assertListContainsText(idx, text);
	}
	
	public String getTransactions_ResultsFoundLabelText() {
		return transactions_ResultsFoundLabel.getText();
	}
	
	public String getFailedPendingTransactions_ResultsFoundLabelText() {
		return failedPendingTransactions_ResultsFoundLabel.getText();
	}
	
	public void assertFailedPendingTransactions_ResultsFoundLabelText(String text) {
		failedPendingTransactions_ResultsFoundLabel.assertText(text);
	}
	
	public void clickTransactionsTabButton() {
		transactionsTabButton.click();
	}
	
	public void clickFailedPendingTransactionsTabButton() {
		failedPendingTransactionsTabButton.click();
	}
	
	public void assertSavingsAccountBalanceLabel(String text) {
		savingsAccountBalanceLabel.assertContainsText(text);
	}
	
	public void assertFDAmountLabel(String text) {
		fdAmountLabel.assertContainsText(text);
	}
	
}


