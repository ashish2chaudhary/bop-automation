package backofficepanel.pages;

import java.util.List;
<<<<<<< HEAD

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.paytm.framework.ui.base.page.BasePage;
import com.paytm.framework.ui.element.Button;
=======
import org.openqa.selenium.WebElement;
import com.paytm.framework.ui.base.page.BasePage;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.ui.element.ElementList;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
import com.paytm.framework.ui.element.Label;
import com.paytm.framework.ui.element.Link;
import com.paytm.framework.ui.element.TextBox;

<<<<<<< HEAD
=======

>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
import backofficepanel.appconstants.Constants.PagePath;
import backofficepanel.automation.LocalConfig;
import backofficepanel.utilities.ReadXML;

public class CommonPage extends BasePage{

	private Button logoutButton;
<<<<<<< HEAD
	private Link  AccountManagementTab_Link;
	private Link Account_Number  , MobileNumber , EmailID , RRN , Cust_ID;
	private TextBox Searchbar ; 
	private Button SearchButton , DropDown; 
	private Button cancelButton;
	private Link Logout_Link , PaginationForward_Link , PaginationBackward_Link;
	private Label ACMAN_Placeholder_Label , SearchErrorMessage_Label;
=======
	private Link transactionHistoryLink;
	private Link accountManagementLink;
	private Link reconcillationLink;
	private Link uploadsLink;
	private Link pendingProductsLink;
	private Link centralOperationsLink;
	private Link dropDownButton;
	private Button customerIDButton;
	private Button accountNumberButton;
	private Button mobileNumberButton;
	private Button emailIDButton;
	private Button rRNButton;
	private Button transactionIDButton;
	private TextBox searchbarTextBox; 
	private Button searchButton;
	private Button dateFilterButton;
	private Label startDateLabel;;
	private Label endDateLabel;
	private Label currentMonthLabel;
	private Button previousMonthButton;
	private Button nextMonthButton;
	private ElementList currentMonthDatesElementList;
	private TextBox tranIDTextBox;
	private TextBox serialNoTextBox;
	private Label errorMessageLabel;
	private Link advanceSearchLink;
	private Button selectDateButton;
	private Label loaderLabel;
	private Label pagination_PageNoLabel;
	private Label pagination_TotalPagesLabel;
	private Button pagination_PreviousPageButton;
	private Button pagination_NextPageButton;	
	private Button cancelButton;
	private Link Logout_Link;
	private Label ACMAN_Placeholder_Label ;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		
	public CommonPage() {
		super(CommonPage.class.getSimpleName()); 
        this.pageURL = LocalConfig.BOP_URL + PagePath.COMMONPAGE_PATH;
        System.out.println(this.pageURL);
<<<<<<< HEAD
        
        PaginationForward_Link =new Link(ReadXML.getElementLocator(getPageName(), "PaginationBackward"), getPageName(), "PaginationBackward");
        PaginationBackward_Link =new Link(ReadXML.getElementLocator(getPageName(), "PaginationBackward"), getPageName(), "PaginationBackward");
        
        logoutButton = new Button(ReadXML.getElementLocator(getPageName(), "Logout"), getPageName(), "Logout");
        DropDown = new Button(ReadXML.getElementLocator(getPageName(), "DropDown"), getPageName(), "DropDown");
        Cust_ID = new Link(ReadXML.getElementLocator(getPageName(), "CustomerID"), getPageName(), "CustomerID");
        Account_Number = new Link(ReadXML.getElementLocator(getPageName(), "AccountNumber_Text"), getPageName(), "AccountNumber_Text");
        MobileNumber = new Link(ReadXML.getElementLocator(getPageName(), "MobileNumber"), getPageName(), "MobileNumber");
        EmailID = new Link(ReadXML.getElementLocator(getPageName(), "EmailID"), getPageName(), "EmailID");
        RRN = new Link(ReadXML.getElementLocator(getPageName(), "RRN"), getPageName(), "RRN");
        Searchbar = new TextBox(ReadXML.getElementLocator(getPageName(), "SearchBar"), getPageName(), "SearchBar");
        SearchButton = new Button(ReadXML.getElementLocator(getPageName(), "SearchButton"), getPageName(), "SearchButton");
        cancelButton = new Button(ReadXML.getElementLocator(getPageName(), "Cancel"), getPageName(), "Cancel");
        AccountManagementTab_Link = new Link(ReadXML.getElementLocator(getPageName(), "AccountManagementTab"), getPageName(), "AccountManagementTab");
        ACMAN_Placeholder_Label = new Label(ReadXML.getElementLocator(getPageName(), "ACMAN_Placeholder"), getPageName(), "ACMAN_Placeholder");
        Logout_Link = new Link(ReadXML.getElementLocator(getPageName(), "Logout"), getPageName(), "Logout");  
        SearchErrorMessage_Label = new Label(ReadXML.getElementLocator(getPageName(), "SearchErrorMessage"), getPageName(), "SearchErrorMessage"); 
    }
	
	
	public void paginationForward()
	{
		PaginationForward_Link.click();
	}
	
	public void paginationBackward()
	{
		PaginationBackward_Link.click();
	}
=======
        logoutButton = new Button(ReadXML.getElementLocator(getPageName(), "Logout"), getPageName(), "Logout");
        transactionHistoryLink = new Link(ReadXML.getElementLocator(getPageName(), "TransactionHistory"), getPageName(), "TransactionHistory");
        accountManagementLink = new Link(ReadXML.getElementLocator(getPageName(), "AccountManagement"), getPageName(), "AccountManagement");
        reconcillationLink = new Link(ReadXML.getElementLocator(getPageName(), "Reconcillation"), getPageName(), "Reconcillation");
        uploadsLink = new Link(ReadXML.getElementLocator(getPageName(), "Uploads"), getPageName(), "Uploads");
        pendingProductsLink = new Link(ReadXML.getElementLocator(getPageName(), "PendingProducts"), getPageName(), "PendingProducts");
        centralOperationsLink = new Link(ReadXML.getElementLocator(getPageName(), "CentralOperations"), getPageName(), "CentralOperations");           
        dropDownButton = new Link(ReadXML.getElementLocator(getPageName(), "DropDown"), getPageName(), "DropDown");
        customerIDButton = new Button(ReadXML.getElementLocator(getPageName(), "CustomerID"), getPageName(), "Cust_ID");
        accountNumberButton = new Button(ReadXML.getElementLocator(getPageName(), "AccountNumber"), getPageName(), "Account_Number");
        mobileNumberButton = new Button(ReadXML.getElementLocator(getPageName(), "MobileNumber"), getPageName(), "Mobile_Number");
        emailIDButton = new Button(ReadXML.getElementLocator(getPageName(), "EmailID"), getPageName(), "Email_ID");
        rRNButton = new Button(ReadXML.getElementLocator(getPageName(), "RRN"), getPageName(), "RRN");
        transactionIDButton = new Button(ReadXML.getElementLocator(getPageName(), "TransactionID"), getPageName(), "TransactionID");
        searchbarTextBox = new TextBox(ReadXML.getElementLocator(getPageName(), "SearchBar"), getPageName(), "SearchBar");
        searchButton = new Button(ReadXML.getElementLocator(getPageName(), "SearchButton"), getPageName(), "SearchButton");
        dateFilterButton = new Button(ReadXML.getElementLocator(getPageName(), "DateFilter"), getPageName(), "DateFilter");
        startDateLabel = new Label(ReadXML.getElementLocator(getPageName(), "StartDate"), getPageName(), "StartDate");
        endDateLabel = new Label(ReadXML.getElementLocator(getPageName(), "EndDate"), getPageName(), "EndDate");
        currentMonthLabel = new Label(ReadXML.getElementLocator(getPageName(), "CurrentMonth"), getPageName(), "CurrentMonth");
        previousMonthButton = new Button(ReadXML.getElementLocator(getPageName(), "PreviousMonth"), getPageName(), "PreviousMonth");
        nextMonthButton = new Button(ReadXML.getElementLocator(getPageName(), "NextMonth"), getPageName(), "NextMonth");
        currentMonthDatesElementList = new ElementList(ReadXML.getElementLocator(getPageName(), "CurrentMonthDates"), getPageName(), "CurrentMonthDates");
        tranIDTextBox = new TextBox(ReadXML.getElementLocator(getPageName(), "TranID"), getPageName(), "TranID");
        serialNoTextBox = new TextBox(ReadXML.getElementLocator(getPageName(), "SerialNo"), getPageName(), "SerialNo");
        errorMessageLabel = new Label(ReadXML.getElementLocator(getPageName(), "ErrorMessage"), getPageName(), "ErrorMessage");
        advanceSearchLink = new Link(ReadXML.getElementLocator(getPageName(), "AdvanceSearch"), getPageName(), "AdvanceSearch");
        selectDateButton = new Button(ReadXML.getElementLocator(getPageName(), "SelectDate"), getPageName(), "SelectDate");
        loaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "Loader"), getPageName(), "Loader");
        pagination_PageNoLabel = new Label(ReadXML.getElementLocator(getPageName(),  "Pagination_PageNo"), getPageName(), "Pagination_PageNo");
        pagination_TotalPagesLabel = new Label(ReadXML.getElementLocator(getPageName(), "Pagination_TotalPages"), getPageName(), "Pagination_TotalPages");
        pagination_PreviousPageButton = new Button(ReadXML.getElementLocator(getPageName(),  "Pagination_PreviousPage"), getPageName(), "Pagination_PreviousPage");
        pagination_NextPageButton = new Button(ReadXML.getElementLocator(getPageName(),  "Pagination_NextPage"), getPageName(), "Pagination_NextPage");
        ACMAN_Placeholder_Label = new Label(ReadXML.getElementLocator(getPageName(), "ACMAN_Placeholder"), getPageName(), "ACMAN_Placeholder");
        Logout_Link = new Link(ReadXML.getElementLocator(getPageName(), "Logout"), getPageName(), "Logout");
	}
		
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	public boolean isLoggedIn()
	{
		return Logout_Link.isPresent();
	}
	
<<<<<<< HEAD
=======
	public void clickTransactionHistoryLink() {
		transactionHistoryLink.click();
	}

	public void scrollToTransactionHistoryLink() {
		transactionHistoryLink.scrollToView();
	}

	public void clickAccountManagementLink() {
		accountManagementLink.click();	
	}

	public void clickReconcillationLink() {
		reconcillationLink.click();
	}

	public void clickUploadsLink() {
		uploadsLink.click();
	}

	public void clickPendingProductsLink() {
		pendingProductsLink.click();
	}

	public void clickcentralOperationsLink() {
		centralOperationsLink.click();
	}
	
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	public List<WebElement> getLogout() {
		return logoutButton.getWrappedElements();
	}
	
	public void clicklogoutButton() {
		logoutButton.click();
	}
	
<<<<<<< HEAD
	public void goToAccountmanagement() {
		refresh();
		navigate("https://dashboard-ite.paytmbank.com/account-management");
		refresh();
		ACMAN_Placeholder_Label.waitUntilVisible();
	}
	
	public void openDropdown()
	{
		DropDown.click();
	}
	
	public void selectCustomerId()
	{
		Cust_ID.click();
	}
	
	public void selectAccountNumber()
	{
		Account_Number.click();
	}
	
	public void selectMobileNumber()
	{
		MobileNumber.click();
	}
	
	public void selectEmailid()
	{
		EmailID.click();
	}
	
	public void selectRRN()
	{
		RRN.click();
	}
	
	public void clickSearchButton()
	{
		SearchButton.sendKeys(Keys.ENTER);
	}
	
	public void typeInsearchBox(String value)
	{
		Searchbar.sendKeys(value);
	}
	
	public void clickSearchCancelButton() {
		cancelButton.click();
	}

	public void verifyErrorMessage(String error) {

		SearchErrorMessage_Label.assertText(error);
	}
	
	public void enterSearchBar()
	{
		Searchbar.sendKeys(Keys.ENTER);
=======
	public void openDropdown() {
		dropDownButton.click();
	}
	
	public void selectCustomerId() {
		customerIDButton.click();
	}
	
	public void selectAccountNumber() {
		accountNumberButton.click();
	}	
	
	public void selectMobileNumber() {
		mobileNumberButton.click();
	}	
	
	public void selectEmailid() {
		emailIDButton.click();
	}
	
	public void selectRRN() {
		rRNButton.click();
	}
	
	public void selectTransactionID() {
		transactionIDButton.click();
	}
	
	public void clickSearchButton() {
		searchButton.click();
	}
	
	public void typeInSearchBox(CharSequence... keysToSend) {
		searchbarTextBox.sendKeys(keysToSend);
	}
	
	public void clearSearchBox() {
		searchbarTextBox.clear();
	}
	
	public void clickDateFilterButton() {
		dateFilterButton.click();
	}
	
	public String getCurrentMonthLabelText() {
		return currentMonthLabel.getText();
	}
	
	public void clickPreviousMonthButton() {
		previousMonthButton.click();
	}
	
	public void clickNextMonthButton() {
		nextMonthButton.click();		
	}
	
	public void clickCurrentMonthDatesElementListElement(String text) {
		currentMonthDatesElementList.clickElement(text);
	}
	
	public void setPageURLAsTransactionHistoryPage() {
		this.pageURL = LocalConfig.BOP_URL + PagePath.TRANSACTIONHISTORY_PATH;
	}
	
	public void typeInTranIDTextBox(CharSequence... keysToSend) {
		tranIDTextBox.sendKeys(keysToSend);
	}
	
	public void typeInSerialNoTextBox(CharSequence... keysToSend) {
		serialNoTextBox.sendKeys(keysToSend);
	}
	
	public void assertErrorMessageLabelText(String text) {
		errorMessageLabel.assertText(text);
	}
	
	public void assertAdvanceSearchLinkText(String text) {
		advanceSearchLink.assertText(text);
	}
	
	public void assertSearchbarTextBoxAttributeValue(String attribute, String value) {
		searchbarTextBox.assertAttribute(attribute, value);
	}
	
	public void assertTranIDTextBoxAttributeValue(String attribute, String value) {
		tranIDTextBox.assertAttribute(attribute, value);
	}
	
	public void assertserialNoTextBoxAttributeValue(String attribute, String value) {
		serialNoTextBox.assertAttribute(attribute, value);
	}
	
	public void assertSelectDateButtonAttributeValue(String attribute, String value) {
		selectDateButton.assertAttribute(attribute, value);
	}
	
	public void assertStartDateLabelText(String text) {
		startDateLabel.assertText(text);
	}
	
	public void assertEndDateLabelText(String text) {
		endDateLabel.assertText(text);
	}
	
	public void waitForLoaderToDisappear() {
		loaderLabel.waitUntilNotVisible();
	}
	
	public void assertPagination_PageNoLabelText(String text) {
		pagination_PageNoLabel.assertText(text);
	}
	
	public void assertPagination_TotalPagesLabelTextContains(String text) {
		pagination_TotalPagesLabel.assertContainsText(text);
	}
	
	public void clickPagination_PreviousPageButton() {
		pagination_PreviousPageButton.click();
	}
	
	public void clickPagination_NextPageButton() {
		pagination_NextPageButton.click();
	}
	
	public void clickSearchCancelButton() {
		cancelButton.click();
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	}
}
