package backofficepanel.pages;

import org.fest.assertions.api.Assertions;

import com.paytm.framework.ui.base.page.BasePage;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.ui.element.IFrame;
import com.paytm.framework.ui.element.Label;
import com.paytm.framework.ui.element.Link;
import com.paytm.framework.ui.element.Table;
import com.paytm.framework.ui.element.TextBox;
import com.paytm.framework.utils.CommonUtils;

import backofficepanel.appconstants.Constants.PagePath;
import backofficepanel.automation.LocalConfig;
import backofficepanel.utilities.ReadXML;

public class AccountManagement_Page extends BasePage{
	
	private IFrame loginViewIFrame;
	private TextBox userNameTextBox;
	private TextBox passwordTextBox;
	private Button loginButton ,Action_Submit_Button;
	private Label  CustomerCustIDHeaderLabel ,CustomerCustIDValueLabel , CustomerNameHeaderLabel ,
	CustomerNameValueLabel ,MobileNumberHeaderLabel , MobileNumberValueLabel, EMAIL_ID_HeaderLabel ,
	EMAIL_ID_ValueLabel , Loader_Label , QR_CODE_Label ,AWB_NUMBER_label , PRE_DELIVERY_STATUS_Label ,DELIVERY_STATUS_Label
	,ORDER_ID_Label ,PDC_Number_Label , PDC_Alias_Label , PDC_Status_Label , PDC_VARIANT_Label;
	private Button SyncInfoButton , SavingsAccount_TabButton , KYC_Details_TabButton , 
	DebitCard_TabButton ,Fixed_Deposit_TabButton;
	
<<<<<<< HEAD
	private Table FixedDepositTable_Table , CardBlockHistTable;
	
	private Label AccountInfoHeader_Label ,AccountNumber_Label , BlockHist_Popup_Label , 
=======
	private Table FixedDepositTable_Table ;
	
	private Label AccountInfoHeader_Label ,AccountNumber_Label , 
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	AccountNumber_Value_Label ,BALANCE_Text_Label , BALANCE_Value_Label ,
	ACCOUNT_TYPEText_Label , ACCOUNT_TYPEValue_Label , ACCOUNT_STATUS_Text_Label , 
	ACCOUNT_STATUS_Value_label ,BRANCH_NAME_Text_Label ,BRANCH_NAME_Value_Label ,
	BRANCH_ID_Text_Label ,BRANCH_ID_Value_Label , BRANCH_ADDRESS_Text_Label , 
	BRANCH_ADDRESS_value_Label , CREATED_DATE_Text_Label , CREATED_DATE_Value_Label ,
	UPDATED_DATE_Text_Label, UPDATED_DATE_Value_Label,SEEDING_DESEEDING_STATUS_Text_Label,
	SEEDING_DESEEDING_STATUS_Value_Label , CONSENT_STATUS_text_Label ,
	CONSENT_STATUS_Value_Label,NPCI_SEEDING_DATE_text_Label , NPCI_SEEDING_DATE_Value_Label , 
	REJECT_REASON_Text_Label ,REJECT_REASON_Value_label,Nominee_Label , NOMINEE_NAME_Text_Label , 
	NOMINEE_NAME_Value_Label , NOMINEE_ID_Text_Label , NOMINEE_ID_Value_Label, 
	RELATIONSHIP_Text_Label ,RELATIONSHIP_Value_Label,EMAIL_ID_text_Label , EMAIL_ID_value_Label , 
	DOB_Text_Label , DOB_Value_Label , AADHAR_CARD_Text_Label,AADHAR_CARD_Value_Label , 
	PERCENTAGE_text_Label , PERCENTAGE_value_Label , ADDRESS_Text_Label , ADDRESS_Value_Label,
	NOMINEE_NAME_Label,NOMINEE_ID_Label,RELATIONSHIP_Label,EMAIL_ID_Label,DOB_Label,Aadhar_Label,
<<<<<<< HEAD
	PERCENTAGE_Label,ADDRESS_Label,CustomerID_Value_Label, MobileNumber_Value_Label ,NoFD_Label;
=======
	PERCENTAGE_Label,ADDRESS_Label,CustomerID_Value_Label, MobileNumber_Value_Label;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	private Link Reason_Dropdown_Link, Action_Link ,Actions_DropDown_Link,Action_Block_Link ,Reason_Block_Link,
	Action_List_Unblock_Link,Action_List_Hotlist_Link ,Action_List_Restrict_Block_Link ,More_Details_Link,
	Action_List_Restrict_UnBlock_Link , Action_List_Totalunfreeze_Link ,Action_List_DebitFreeze_Link , 
<<<<<<< HEAD
	FD_NO_Link ,Action_List_TotalFreeze_Link, View_Block_History_Link,Less_Details_Link ,Total_FD_Link;
=======
	FD_NO_Link ,Action_List_TotalFreeze_Link;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	private Label KYC_STATUS_Label,KYC_TYPE_label , USE_KYC_DETAILS_label,PAN_UPDATED_label , PAN_VERIFIED_label 
	,AADHAAR_UPDATED_label , AADHAAR_VERIFIED_Label ,FORM_60_Label , VDC_Number_Label , VDC_Alias_Label  ,  
	VDC_Status_Label , VDC_VARIANT_Label,Comment_Box_TextBox ,More_GENDER_Label  , More_DOB_Label ,  More_MARITAL_STATUS_Label ,
	More_NATIONALITY_Label , More_OCCUPATION_Label , More_EMPLOYMENT_STATUS_Label , More_PAN_CARD_Label , More_ADDRESS_Label ,
	More_DOCUMENT_TYPE_Label , More_ISSUANCE_COUNTRY_Label , More_ISSUANCE_PLACE_Label , More_DOCUMENT_NUMBER_Label ,
	BOOKING_TRAN_ID_Label ,BOOKING_DATE_TIME_Label ,BOOKING_DATE_TIME_label,TENURE_Label ,INTEREST_Label ,AMOUNT_label, 
<<<<<<< HEAD
	STATUS_Label ,EFFECTIVE_OPEN_DATE_Label, More_ISSUANCE_DATE_Label ,PDC_Details_Label, View_Block_History_Label,
	Basicinfo_Label , ProofOfID_Label ,VDC_Details_Label;
=======
	STATUS_Label ,EFFECTIVE_OPEN_DATE_Label, More_ISSUANCE_DATE_Label;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		
	public AccountManagement_Page() {
		super(AccountManagement_Page.class.getSimpleName()); 
		System.out.println("Inside login page");
        this.pageURL = LocalConfig.BOP_URL + PagePath.LOGINPAGE_PATH;
        System.out.println(this.pageURL);
        
<<<<<<< HEAD
        Basicinfo_Label= new Label(ReadXML.getElementLocator(getPageName(), "Basicinfo"), getPageName(), "Basicinfo");
        ProofOfID_Label= new Label(ReadXML.getElementLocator(getPageName(), "ProofOfID"), getPageName(), "ProofOfID");
        Less_Details_Link= new Link(ReadXML.getElementLocator(getPageName(), "Less_Details"), getPageName(), "Less_Details");
        NoFD_Label= new Label(ReadXML.getElementLocator(getPageName(), "NoFD"), getPageName(), "NoFD");
        BlockHist_Popup_Label= new Label(ReadXML.getElementLocator(getPageName(), "BlockHist_Popup"), getPageName(), "BlockHist_Popup");
        CardBlockHistTable = new Table(ReadXML.getElementLocator(getPageName(), "CardBlockHistTable"), getPageName(), "CardBlockHistTable");
        View_Block_History_Link= new Link(ReadXML.getElementLocator(getPageName(), "View_Block_History"), getPageName(), "View_Block_History");
        PDC_Details_Label= new Label(ReadXML.getElementLocator(getPageName(), "PDC_Details"), getPageName(), "PDC_Details");
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
        CustomerID_Value_Label= new Label(ReadXML.getElementLocator(getPageName(), "CustomerID_Value"), getPageName(), "CustomerID_Value");
        MobileNumber_Value_Label= new Label(ReadXML.getElementLocator(getPageName(), "MobileNumber_Value"), getPageName(), "MobileNumber_Value");
        CustomerNameHeaderLabel= new Label(ReadXML.getElementLocator(getPageName(), "CustomerNameHeader"), getPageName(), "CustomerNameHeader");
        CustomerNameValueLabel = new Label(ReadXML.getElementLocator(getPageName(), "CustomerNameValue"), getPageName(), "CustomerNameValue");
        MobileNumberHeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "MobileNumberHeader"), getPageName(), "MobileNumberHeader");
        MobileNumberValueLabel = new Label(ReadXML.getElementLocator(getPageName(), "MobileNumberValue"), getPageName(), "MobileNumberValue");
        EMAIL_ID_HeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "EMAIL_ID_Header"), getPageName(), "EMAIL_ID_Header");
        EMAIL_ID_ValueLabel = new Label(ReadXML.getElementLocator(getPageName(), "EMAIL_ID_Value"), getPageName(), "EMAIL_ID_Value");
        CustomerCustIDHeaderLabel = new Label(ReadXML.getElementLocator(getPageName(), "Cust_ID_Header"), getPageName(), "Cust_ID_Header");
        CustomerCustIDValueLabel = new Label(ReadXML.getElementLocator(getPageName(), "Cust_ID_Value"), getPageName(), "Cust_ID_Value");
        SavingsAccount_TabButton = new Button(ReadXML.getElementLocator(getPageName(), "SavingsAccount_Tab"), getPageName(), "SavingsAccount_Tab");
        KYC_Details_TabButton = new Button(ReadXML.getElementLocator(getPageName(), "KYC_Details_Tab"), getPageName(), "KYC_Details_Tab");
        DebitCard_TabButton = new Button(ReadXML.getElementLocator(getPageName(), "DebitCard_Tab"), getPageName(), "DebitCard_Tab");
        Fixed_Deposit_TabButton = new Button(ReadXML.getElementLocator(getPageName(), "Fixed_Deposit_Tab"), getPageName(), "Fixed_Deposit_Tab");
        AccountInfoHeader_Label = new Label(ReadXML.getElementLocator(getPageName(), "AccountInfoHeader"), getPageName(), "AccountInfoHeader");
        AccountNumber_Label = new Label(ReadXML.getElementLocator(getPageName(), "AccountNumber_Text"), getPageName(), "AccountNumber_Text");
        AccountNumber_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "AccountNumber_Value"), getPageName(), "AccountNumber_Value");
        BALANCE_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "BALANCE_Text"), getPageName(), "BALANCE_Text");
        BALANCE_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "BALANCE_Value"), getPageName(), "BALANCE_Value");
        ACCOUNT_TYPEText_Label = new Label(ReadXML.getElementLocator(getPageName(), "ACCOUNT_TYPEText"), getPageName(), "ACCOUNT_TYPEText");
        ACCOUNT_TYPEValue_Label = new Label(ReadXML.getElementLocator(getPageName(), "ACCOUNT_TYPEValue"), getPageName(), "ACCOUNT_TYPEValue");      
        ACCOUNT_STATUS_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "ACCOUNT_STATUS_Text"), getPageName(), "ACCOUNT_STATUS_Text");
        ACCOUNT_STATUS_Value_label = new Label(ReadXML.getElementLocator(getPageName(), "ACCOUNT_STATUS_Value"), getPageName(), "ACCOUNT_STATUS_Value");
        BRANCH_NAME_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_NAME_Text"), getPageName(), "BRANCH_NAME_Text");
        BRANCH_NAME_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_NAME_Value"), getPageName(), "BRANCH_NAME_Value");
        BRANCH_ID_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_ID_Text"), getPageName(), "BRANCH_ID_Text");
        BRANCH_ID_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_ID_Value"), getPageName(), "BRANCH_NAME_Value");
        BRANCH_ADDRESS_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_ADDRESS_Text"), getPageName(), "BRANCH_ADDRESS_Text");
        BRANCH_ADDRESS_value_Label = new Label(ReadXML.getElementLocator(getPageName(), "BRANCH_ADDRESS_value"), getPageName(), "BRANCH_ADDRESS_value");
        CREATED_DATE_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "CREATED_DATE_Text"), getPageName(), "CREATED_DATE_Text");
        CREATED_DATE_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "CREATED_DATE_Value"), getPageName(), "CREATED_DATE_Value");
        UPDATED_DATE_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "UPDATED_DATE_Text"), getPageName(), "UPDATED_DATE_Text");
        UPDATED_DATE_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "UPDATED_DATE_Value"), getPageName(), "UPDATED_DATE_Value");
        SEEDING_DESEEDING_STATUS_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "SEEDING_DESEEDING_STATUS_Text"), getPageName(), "SEEDING_DESEEDING_STATUS_Text");
        SEEDING_DESEEDING_STATUS_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "SEEDING_DESEEDING_STATUS_Value"), getPageName(), "SEEDING_DESEEDING_STATUS_Value");
        CONSENT_STATUS_text_Label = new Label(ReadXML.getElementLocator(getPageName(), "CONSENT_STATUS_text"), getPageName(), "CONSENT_STATUS_text");
        CONSENT_STATUS_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "CONSENT_STATUS_Value"), getPageName(), "CONSENT_STATUS_Value");
        NPCI_SEEDING_DATE_text_Label = new Label(ReadXML.getElementLocator(getPageName(), "NPCI_SEEDING_DATE_text"), getPageName(), "NPCI_SEEDING_DATE_text");
        NPCI_SEEDING_DATE_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "NPCI_SEEDING_DATE_Value"), getPageName(), "NPCI_SEEDING_DATE_Value");
        REJECT_REASON_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "REJECT_REASON_Text"), getPageName(), "REJECT_REASON_Text");
        REJECT_REASON_Value_label = new Label(ReadXML.getElementLocator(getPageName(), "REJECT_REASON_Value"), getPageName(), "REJECT_REASON_Value");
        Nominee_Label = new Label(ReadXML.getElementLocator(getPageName(), "Nominee"), getPageName(), "Nominee");
        NOMINEE_NAME_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_NAME_Text"), getPageName(), "NOMINEE_NAME_Text");
        NOMINEE_NAME_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_NAME_Value"), getPageName(), "NOMINEE_NAME_Value");
        NOMINEE_ID_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_ID_Text"), getPageName(), "NOMINEE_ID_Text");
        NOMINEE_ID_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_ID_Value"), getPageName(), "NOMINEE_ID_Value");
        RELATIONSHIP_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "RELATIONSHIP_Text"), getPageName(), "RELATIONSHIP_Text");
        RELATIONSHIP_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "RELATIONSHIP_Value"), getPageName(), "RELATIONSHIP_Value");        
        EMAIL_ID_text_Label = new Label(ReadXML.getElementLocator(getPageName(), "EMAIL_ID_text"), getPageName(), "EMAIL_ID_text");
        EMAIL_ID_value_Label = new Label(ReadXML.getElementLocator(getPageName(), "EMAIL_ID_value"), getPageName(), "EMAIL_ID_value");
        DOB_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "DOB_Text"), getPageName(), "DOB_Text");
        DOB_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "DOB_Value"), getPageName(), "DOB_Value");
        AADHAR_CARD_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "AADHAR_CARD_Text"), getPageName(), "AADHAR_CARD_Text");
        AADHAR_CARD_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "AADHAR_CARD_Value"), getPageName(), "AADHAR_CARD_Value");
        PERCENTAGE_text_Label = new Label(ReadXML.getElementLocator(getPageName(), "PERCENTAGE_text"), getPageName(), "PERCENTAGE_text");
        PERCENTAGE_value_Label = new Label(ReadXML.getElementLocator(getPageName(), "PERCENTAGE_value"), getPageName(), "PERCENTAGE_value");
        ADDRESS_Text_Label = new Label(ReadXML.getElementLocator(getPageName(), "ADDRESS_Text"), getPageName(), "ADDRESS_Text");
        ADDRESS_Value_Label = new Label(ReadXML.getElementLocator(getPageName(), "ADDRESS_Value"), getPageName(), "ADDRESS_Value");
        NOMINEE_NAME_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_NAME"), getPageName(), "NOMINEE_NAME");
        NOMINEE_ID_Label = new Label(ReadXML.getElementLocator(getPageName(), "NOMINEE_ID"), getPageName(), "NOMINEE_ID");
        RELATIONSHIP_Label = new Label(ReadXML.getElementLocator(getPageName(), "RELATIONSHIP"), getPageName(), "RELATIONSHIP");
        EMAIL_ID_Label = new Label(ReadXML.getElementLocator(getPageName(), "EMAIL_ID"), getPageName(), "EMAIL_ID");
        DOB_Label = new Label(ReadXML.getElementLocator(getPageName(), "DOB"), getPageName(), "DOB");
        Aadhar_Label = new Label(ReadXML.getElementLocator(getPageName(), "Aadhar"), getPageName(), "Aadhar");
        PERCENTAGE_Label = new Label(ReadXML.getElementLocator(getPageName(), "PERCENTAGE"), getPageName(), "PERCENTAGE");
        ADDRESS_Label = new Label(ReadXML.getElementLocator(getPageName(), "ADDRESS"), getPageName(), "ADDRESS");
        KYC_STATUS_Label = new Label(ReadXML.getElementLocator(getPageName(), "KYC_STATUS"), getPageName(), "KYC_STATUS");
        KYC_TYPE_label = new Label(ReadXML.getElementLocator(getPageName(), "KYC_TYPE"), getPageName(), "KYC_TYPE");
        USE_KYC_DETAILS_label = new Label(ReadXML.getElementLocator(getPageName(), "USE_KYC_DETAILS"), getPageName(), "USE_KYC_DETAILS");
        PAN_UPDATED_label = new Label(ReadXML.getElementLocator(getPageName(), "PAN_UPDATED"), getPageName(), "PAN_UPDATED");
        PAN_VERIFIED_label = new Label(ReadXML.getElementLocator(getPageName(), "PAN_VERIFIED"), getPageName(), "PAN_VERIFIED");
        AADHAAR_UPDATED_label= new Label(ReadXML.getElementLocator(getPageName(), "AADHAAR_UPDATED"), getPageName(), "AADHAAR_UPDATED");
        AADHAAR_VERIFIED_Label = new Label(ReadXML.getElementLocator(getPageName(), "AADHAAR_VERIFIED"), getPageName(), "AADHAAR_VERIFIED");
        FORM_60_Label = new Label(ReadXML.getElementLocator(getPageName(), "FORM_60"), getPageName(), "FORM_60");
        VDC_Number_Label=new Label(ReadXML.getElementLocator(getPageName(), "VDC_Number"), getPageName(), "VDC_Number");
        VDC_Alias_Label=new Label(ReadXML.getElementLocator(getPageName(), "VDC_Alias"), getPageName(), "VDC_Alias");
        VDC_Status_Label=new Label(ReadXML.getElementLocator(getPageName(), "VDC_Status"), getPageName(), "VDC_Status");
        VDC_VARIANT_Label=new Label(ReadXML.getElementLocator(getPageName(), "VDC_VARIANT"), getPageName(), "VDC_VARIANT");
        Actions_DropDown_Link=new Link(ReadXML.getElementLocator(getPageName(), "Actions_DropDown"), getPageName(), "Actions_DropDown");
        Reason_Dropdown_Link=new Link(ReadXML.getElementLocator(getPageName(), "Reason_Dropdown"), getPageName(), "Reason_Dropdown");
        Action_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_Link"), getPageName(), "Action_Link");
        Comment_Box_TextBox=new Label(ReadXML.getElementLocator(getPageName(), "Comment_Box"), getPageName(), "Comment_Box");
        Action_Block_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_Block"), getPageName(), "Action_Block");
        Loader_Label=new Label(ReadXML.getElementLocator(getPageName(), "Loader"), getPageName(), "Loader");
        Action_Submit_Button=new Button(ReadXML.getElementLocator(getPageName(), "Action_Submit"), getPageName(), "Action_Submit");
        Action_List_Unblock_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_Unblock"), getPageName(), "Action_List_Unblock");
        Action_List_Hotlist_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_Hotlist"), getPageName(), "Action_List_Hotlist");
        Action_List_Restrict_Block_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_Restrict_Block"), getPageName(), "Action_List_Restrict_Block");
        Action_List_Restrict_UnBlock_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_Restrict_UnBlock"), getPageName(), "Action_List_Restrict_UnBlock");
        Action_List_Totalunfreeze_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_Totalunfreeze"), getPageName(), "Action_List_Totalunfreeze");
        Action_List_TotalFreeze_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_TotalFreeze"), getPageName(), "Action_List_TotalFreeze");
        Action_List_DebitFreeze_Link=new Link(ReadXML.getElementLocator(getPageName(), "Action_List_DebitFreeze"), getPageName(), "Action_List_DebitFreeze");
	
        More_GENDER_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_GENDER"), getPageName(), "More_GENDER");
        More_DOB_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_DOB"), getPageName(), "More_DOB");
        More_MARITAL_STATUS_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_MARITAL_STATUS"), getPageName(), "More_MARITAL_STATUS");
        More_NATIONALITY_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_NATIONALITY"), getPageName(), "More_NATIONALITY");
        More_OCCUPATION_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_OCCUPATION"), getPageName(), "More_OCCUPATION");
        More_EMPLOYMENT_STATUS_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_EMPLOYMENT_STATUS"), getPageName(), "More_EMPLOYMENT_STATUS");
        More_PAN_CARD_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_PAN_CARD"), getPageName(), "More_PAN_CARD");
        More_ADDRESS_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_ADDRESS"), getPageName(), "More_ADDRESS");
       
        More_DOCUMENT_TYPE_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_DOCUMENT_TYPE"), getPageName(), "More_DOCUMENT_TYPE");
        More_ISSUANCE_COUNTRY_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_ISSUANCE_COUNTRY"), getPageName(), "More_ISSUANCE_COUNTRY");
        More_ISSUANCE_PLACE_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_ISSUANCE_PLACE"), getPageName(), "More_ISSUANCE_PLACE");
        More_DOCUMENT_NUMBER_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_DOCUMENT_NUMBER"), getPageName(), "More_DOCUMENT_NUMBER");
        More_ISSUANCE_DATE_Label= new Label(ReadXML.getElementLocator(getPageName(), "More_ISSUANCE_DATE"), getPageName(), "More_ISSUANCE_DATE");
        More_Details_Link= new Link(ReadXML.getElementLocator(getPageName(), "More_Details"), getPageName(), "More_Details");
<<<<<<< HEAD
        PRE_DELIVERY_STATUS_Label= new Label(ReadXML.getElementLocator(getPageName(), "PRE_DELIVERY_STATUS"), getPageName(), "PRE_DELIVERY_STATUS");
        VDC_Details_Label= new Label(ReadXML.getElementLocator(getPageName(), "VDC_Details"), getPageName(), "VDC_Details");
        
        // Table
        Total_FD_Link= new Link(ReadXML.getElementLocator(getPageName(), "Total_FD"), getPageName(), "Total_FD");
=======
        
        PRE_DELIVERY_STATUS_Label= new Label(ReadXML.getElementLocator(getPageName(), "PRE_DELIVERY_STATUS"), getPageName(), "PRE_DELIVERY_STATUS");
        
        
        // Table
        
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
        FD_NO_Link = new Link(ReadXML.getElementLocator(getPageName(), "FD_NO"), getPageName(), "FD_NO");
        BOOKING_TRAN_ID_Label = new Label(ReadXML.getElementLocator(getPageName(), "BOOKING_TRAN_ID"), getPageName(), "BOOKING_TRAN_ID");
        BOOKING_DATE_TIME_Label = new Label(ReadXML.getElementLocator(getPageName(), "BOOKING_DATE_TIME"), getPageName(), "BOOKING_DATE_TIME");
        EFFECTIVE_OPEN_DATE_Label = new Label(ReadXML.getElementLocator(getPageName(), "EFFECTIVE_OPEN_DATE"), getPageName(), "EFFECTIVE_OPEN_DATE");
        TENURE_Label= new Label(ReadXML.getElementLocator(getPageName(), "TENURE"), getPageName(), "TENURE");
        INTEREST_Label= new Label(ReadXML.getElementLocator(getPageName(), "INTEREST"), getPageName(), "INTEREST");
        AMOUNT_label= new Label(ReadXML.getElementLocator(getPageName(), "AMOUNT"), getPageName(), "AMOUNT");
        STATUS_Label= new Label(ReadXML.getElementLocator(getPageName(), "STATUS"), getPageName(), "STATUS");
        FixedDepositTable_Table= new Table(ReadXML.getElementLocator(getPageName(), "FixedDepositTable"), getPageName(), "FixedDepositTable");
	}
	
<<<<<<< HEAD
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void checkISATabClosed()
	{
		AccountInfoHeader_Label.assertIsNotPresent();
	}
	
	public void verifyVDCNotPresent()
	{
		VDC_Details_Label.assertIsNotPresent();
	}
	
	public void verifyPDCNotPresent()
	{
		PDC_Details_Label.assertIsNotPresent();
	}
	
	public void verifyKYCNotPresent()
	{
		KYC_STATUS_Label.assertIsNotPresent();
	}
	
	public void verifyMoreNotPresent()
	{
		More_Details_Link.assertIsNotPresent();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void clickAndCheckFDBalance(String fdbalance)
	{
		Total_FD_Link.click();
		pause(5);
		Total_FD_Link.assertText(fdbalance);
	}
	public void click_less_details()
	{
		Less_Details_Link.click();
		Basicinfo_Label.assertIsNotPresent();
		ProofOfID_Label.assertIsNotPresent();
	}
	
	public void noFDMessage()
	{
		System.out.println("Text is  : "+NoFD_Label.getText());
		NoFD_Label.assertIsPresent();
	}
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	public void verifyFixedDepoTable()
	{
		FD_NO_Link.assertText("FD NO.");
		BOOKING_TRAN_ID_Label.assertText("BOOKING TRANSACTION ID");
		BOOKING_DATE_TIME_Label.assertText("BOOKING DATE & TIME");
		EFFECTIVE_OPEN_DATE_Label.assertText("EFFECTIVE OPENING DATE");
		TENURE_Label.assertText("TENURE");
		INTEREST_Label.assertText("INTEREST");
		AMOUNT_label.assertText("AMOUNT");
		STATUS_Label.assertText("STATUS");
		System.out.println("Row count is : " +FixedDepositTable_Table.getBodyRowCount());
		
		int row_count = FixedDepositTable_Table.getBodyRowCount();
		
		for(int i =0;i<row_count;i++)
		{
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(i, 5, "body")).isEqualTo("7.15%");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(i, 6, "body")).isEqualTo("View");
		}
		
	}
	
	public void clickMoreDetails()
	{
		More_Details_Link.click();
	}
	
	public void verifyPreDelStatus(String predelstatus)
	{
		System.out.println("Checking ... ");
		System.out.println(PRE_DELIVERY_STATUS_Label.getLabelText());
		PRE_DELIVERY_STATUS_Label.assertText(predelstatus);
		
	}
	
<<<<<<< HEAD
	public void presenceOfCardBlockHistTable()
	{
		CardBlockHistTable.assertIsPresent();
	}

	public void verifyHeaderBlockHistTable()
	{	
		BlockHist_Popup_Label.assertContainsText("Card Block History");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(0, 0, "HEADER")).isEqualTo("DATE");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(0, 1, "HEADER")).isEqualTo("ACTION");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(0, 2, "HEADER")).isEqualTo("REASON");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(0, 3, "HEADER")).isEqualTo("COMMENT");
		Assertions.assertThat(FixedDepositTable_Table.getCellTextAtIndex(0, 4, "HEADER")).isEqualTo("ACTION TAKEN BY");
	}
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	
	public void verifyProofOfIdentity(String doctype , String issuanceCountry , String issuanceplace ,String docnumber , String issuedate)
	{
		More_DOCUMENT_TYPE_Label.assertText(doctype);
		More_ISSUANCE_COUNTRY_Label.assertText(issuanceCountry);
		More_ISSUANCE_PLACE_Label.assertText(issuanceplace);
		More_DOCUMENT_NUMBER_Label.assertText(docnumber);
		More_ISSUANCE_DATE_Label.assertText(issuedate);
	}
	
	public void verifyBasicInformation(String gender ,String dob , String marital_status, String nationality ,String occupation,
			 String empstatus ,String pan_card ,String address)
	{
		More_GENDER_Label.assertText(gender);
		More_DOB_Label.assertText(dob);
		More_MARITAL_STATUS_Label.assertText(marital_status);
		More_NATIONALITY_Label.assertText(nationality);
		More_OCCUPATION_Label.assertText(occupation);
		More_EMPLOYMENT_STATUS_Label.assertText(empstatus);
		More_PAN_CARD_Label.assertText(pan_card);
		More_ADDRESS_Label.assertText(address);
	}
	
	public void selectrestrictUnblock()
	{
		Action_List_Restrict_UnBlock_Link.click();
	}
	
	public void selectrestrictBlock()
	{
		Action_List_Restrict_Block_Link.click();
	}
	
	public void selectUnblock()
	{
		Action_List_Unblock_Link.click();
	}
	public void verifyHeaderDetails() 
    {		
		CustomerNameHeaderLabel.assertText("FULL NAME");
		CustomerCustIDHeaderLabel.assertText("CUSTOMER ID");
		MobileNumberHeaderLabel.assertText("MOBILE NUMBER");
		EMAIL_ID_HeaderLabel.assertText("EMAIL ID");
	}
	public void verifyCustomerDetails(String custid ,  String mobile , String emailid)
	{
		System.out.println(custid+" "+mobile+" "+emailid);
		CustomerCustIDValueLabel.assertText("1107230423");
		MobileNumberValueLabel.assertText("5115111044");
		EMAIL_ID_ValueLabel.assertText("sachin.dwivedi@paytm.com");
	}
	
	public void verifySavingAccountHeader() {
		System.out.println(AccountInfoHeader_Label.getLabelText()+" "+AccountNumber_Label.getLabelText()
		+" "+BALANCE_Text_Label.getLabelText()+" "+ACCOUNT_TYPEText_Label.getLabelText()
		+" "+ACCOUNT_TYPEText_Label.getLabelText()+" "+ACCOUNT_STATUS_Text_Label.getLabelText()+" "
		+BRANCH_NAME_Text_Label.getLabelText()+" "+BRANCH_ID_Text_Label.getLabelText()
		+" "+BRANCH_ADDRESS_Text_Label.getLabelText()+" "+CREATED_DATE_Text_Label.getLabelText()+" "
		+UPDATED_DATE_Text_Label.getLabelText()+" "+SEEDING_DESEEDING_STATUS_Text_Label.getLabelText()+" "
		+CONSENT_STATUS_text_Label.getLabelText()+" "+NPCI_SEEDING_DATE_text_Label.getLabelText()
		+" "+REJECT_REASON_Text_Label.getLabelText()+" "
		+Nominee_Label.getLabelText()+" "+NOMINEE_NAME_Text_Label.getLabelText()+" "
		+NOMINEE_ID_Text_Label.getLabelText()+" "
		+RELATIONSHIP_Text_Label.getLabelText()+" "+EMAIL_ID_text_Label.getLabelText()+" "
		+EMAIL_ID_text_Label.getLabelText()+" "+DOB_Text_Label.getLabelText()+" "+AADHAR_CARD_Text_Label.getLabelText()+" "
		+PERCENTAGE_text_Label.getLabelText()+" "+ADDRESS_Text_Label.getLabelText());
		
		
		AccountInfoHeader_Label.assertText("Account Information");
		AccountNumber_Label.assertText("ACCOUNT NUMBER");
		BALANCE_Text_Label.assertText("BALANCE");
		ACCOUNT_TYPEText_Label.assertText("ACCOUNT TYPE");
		ACCOUNT_STATUS_Text_Label.assertText("ACCOUNT STATUS");
		BRANCH_NAME_Text_Label.assertText("BRANCH NAME");
		BRANCH_ID_Text_Label.assertText("BRANCH ID");
		BRANCH_ADDRESS_Text_Label.assertText("BRANCH ADDRESS");
		CREATED_DATE_Text_Label.assertText("CREATED DATE");
		UPDATED_DATE_Text_Label.assertText("UPDATED DATE");
		SEEDING_DESEEDING_STATUS_Text_Label.assertText("SEEDING/DESEEDING STATUS");
		CONSENT_STATUS_text_Label.assertText("CONSENT STATUS");
		NPCI_SEEDING_DATE_text_Label.assertText("NPCI SEEDING DATE");
		REJECT_REASON_Text_Label.assertText("REJECT REASON");
		Nominee_Label.assertText("Nominee");
		NOMINEE_NAME_Text_Label.assertText("NOMINEE NAME");
		NOMINEE_ID_Text_Label.assertText("NOMINEE ID");
		RELATIONSHIP_Text_Label.assertText("RELATIONSHIP");
		EMAIL_ID_text_Label.assertText("EMAIL ID");
		DOB_Text_Label.assertText("DATE OF BIRTH");
		AADHAR_CARD_Text_Label.assertText("AADHAR CARD NUMBER");
		PERCENTAGE_text_Label.assertText("PERCENTAGE");
		ADDRESS_Text_Label.assertText("ADDRESS");
		
	}
	
<<<<<<< HEAD
	public void clickBlockHist()
	{
		View_Block_History_Link.click();
	}
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	public void verifyKYCDetailACMANPage(String kyc_stat ,String kyc_type ,String kyc_details , String pan_updated
			, String pan_verified, String aadhar_updated , String aadhar_verified , String form_60) 
	{
		System.out.println("Inside page");
		System.out.println("kyc_stat : "+kyc_stat);
		System.out.println("kyc_type : "+kyc_type);
		System.out.println("kyc_details : "+kyc_details);
		System.out.println("pan_updated : "+pan_updated);
		System.out.println("pan_verified : "+pan_verified);
		System.out.println("aadhar_updated : "+aadhar_updated);
		System.out.println("aadhar_verified : "+aadhar_verified);
		System.out.println("form_60 : "+form_60);

		KYC_STATUS_Label.assertText(kyc_stat);
		KYC_TYPE_label.assertText(kyc_type);
		USE_KYC_DETAILS_label.assertText(kyc_details);
		PAN_UPDATED_label.assertText(pan_updated);
		PAN_VERIFIED_label.assertText(pan_verified);
		AADHAAR_UPDATED_label.assertText(aadhar_updated);
		AADHAAR_VERIFIED_Label.assertText(aadhar_verified);
		FORM_60_Label.assertText(form_60);
	}
	
	
<<<<<<< HEAD
	
	
	
	public void verifySavingCustDetails(String username , String custid ,String mobile , String emailid ,
			String AC_No,String bal ,String AC_Type,String status
			,String branchname , String branchid , String branch_add,String create_date,
			String update_date ,String see_deessed,String consent_stat,String NPCI_seed_date,String reject) 
=======
	public void verifySavingCustDetails(String username , String custid ,String mobile , String emailid ,
			String AC_No,String bal ,String AC_Type,String status
			,String branchname , String branchid , String branch_add,String create_date,
			String update_date ,String see_deessed,
			String consent_stat,String NPCI_seed_date,String reject) 
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	{
		
		System.out.println("Username"+username+"custid : "+custid+"mobile : "+mobile+" "+"emailid : "+emailid
		+ "Ac  no : "+AC_No+"bal : "+bal+"Type :  "+AC_Type+"Status :  "+status
		+"BranchName :  "+branchname+"Branch id :  "+branchid+"Branch add :  "
		+" "+branch_add+"Create date :  "+create_date+"Update date :  "+update_date+"Seed dessed :  "
		+see_deessed+"Consent stat :  "+consent_stat+"NPCI : "+NPCI_seed_date+"Reject : "+reject);
<<<<<<< HEAD
		
		
		
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
		CustomerID_Value_Label.assertText(custid);
		MobileNumber_Value_Label.assertText(mobile);
		AccountNumber_Value_Label.assertText(AC_No);
		BALANCE_Value_Label.assertText(bal);
		ACCOUNT_TYPEValue_Label.assertText(AC_Type);
		ACCOUNT_STATUS_Value_label.assertText(status);
		BRANCH_NAME_Value_Label.assertText(branchname);
		BRANCH_ID_Value_Label.assertText(branchid);
		BRANCH_ADDRESS_value_Label.assertText(branch_add);
		CREATED_DATE_Value_Label.assertText(create_date);
		UPDATED_DATE_Value_Label.assertText(update_date);
		SEEDING_DESEEDING_STATUS_Value_Label.assertText(see_deessed);
		CONSENT_STATUS_Value_Label.assertText(consent_stat);
		NPCI_SEEDING_DATE_Value_Label.assertText(NPCI_seed_date);
		REJECT_REASON_Value_label.assertText(reject);	
	}
	
	public void verifyNomineeDetail(String nomineename , String nominee_id , String relation , String email,
			String DOB , String aadhar , String percent , String address)
	{
		System.out.println("Nomineename : "+nomineename+"Nomineeid : "+nominee_id+"Relation  : "+relation+"Emaild :  "
		+email+" "+"DOB : "+DOB+"Aadhar :  "+aadhar+"Percent : "+percent+" address : "+address);
		
		System.out.println("*********************************************");
		
		System.out.println("Nomineename : "+NOMINEE_NAME_Label.getLabelText()+"Nomineeid : "+NOMINEE_ID_Label.getLabelText()
		+"Relation  : "+RELATIONSHIP_Label.getLabelText()+"Emaild :  "+EMAIL_ID_Label.getLabelText()
		+"Email id : "+EMAIL_ID_Label.getLabelText()+" "+"DOB : "+DOB_Label.getLabelText()+"Aadhar :  "
		+Aadhar_Label.getLabelText()+"Percent : "+PERCENTAGE_Label.getLabelText()+" address : "+ADDRESS_Label.getLabelText());
		
		
		NOMINEE_NAME_Label.assertText(nomineename);
		NOMINEE_ID_Label.assertText(nominee_id);
		RELATIONSHIP_Label.assertText(relation);
		EMAIL_ID_Label.assertText(email);
		DOB_Label.assertText(DOB);
		Aadhar_Label.assertText(aadhar);
		PERCENTAGE_Label.assertText(percent);
		ADDRESS_Label.assertText(address);
	}
	
	
	public void verifyingVDCDetails(String debitcard_number ,String alias_number ,String stat ,String variant)
	{
		VDC_Number_Label.assertText(debitcard_number);
		VDC_Alias_Label.assertText(alias_number);
		VDC_Status_Label.assertContainsText(stat);
		VDC_VARIANT_Label.assertText(variant);
	}
	
	public void typeInCommentSection()
	{
		Comment_Box_TextBox.sendKeys("Length of this sentence is 150 characters.Length "
				+ "of this sentence is 150 characters.\n" + "Length of this sentence "
						+ "is 150 characters.Length of this sentence");
<<<<<<< HEAD
		
		System.out.println(Comment_Box_TextBox.getText());
	}
	
	public void verifycommentSectionISEmpty()
	{
		System.out.println(Comment_Box_TextBox.getText());
		Comment_Box_TextBox.assertText("");
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	}
	
	public void waitLoaderToAppear()
	{
		Loader_Label.waitUntilVisible();
	}
	
	public void waitLoaderToDisppear()
	{
		Loader_Label.waitUntilNotVisible();
	}
	
	public void clickActionLink()
	{
		Action_Link.click();
	}
	
	public void openActionsDropdown()
	{
		Actions_DropDown_Link.click();
	}
	
	public void openReasonDropDown()
	{
		Reason_Dropdown_Link.click();
	}
	
	public void clickOnSubmit_Debit_SavingActions()
	{
		Action_Submit_Button.click();
	}
	
	public void clickSavingAccountTab()
	{
		SavingsAccount_TabButton.click();
	}
	
	public void clickKYCDetailsTab()
	{
		KYC_Details_TabButton.click();
	}
	
	public void clickDebitCardTab()
	{
		DebitCard_TabButton.click();
	}
	
	public void clickFixedDepositTab()
	{
		Fixed_Deposit_TabButton.click();
		
		pause(5);
	}
	
	public void waitfor(int time)
	{
		pause(time);
	}
	
	public void selectTotalunfreeze()
	{
		Action_List_Totalunfreeze_Link.click();
	}
	
	public void selectDebitFreeze()
	{
		Action_List_DebitFreeze_Link.click();
	}
	
	public void selectTotalFreeze()
	{
		Action_List_TotalFreeze_Link.click();
	}
	
	public void verifyingSavingAccountStatus(String status)
	{
		System.out.println("Expected : "+status);
		System.out.println("Actual : "+ACCOUNT_STATUS_Value_label.getLabelText());
		ACCOUNT_STATUS_Value_label.assertText(status);
	}
}