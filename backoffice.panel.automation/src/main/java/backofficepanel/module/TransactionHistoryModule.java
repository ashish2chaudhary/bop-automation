package backofficepanel.module;

import org.fest.assertions.api.Assertions;
import org.openqa.selenium.InvalidArgumentException;

import backofficepanel.pages.TransactionHistoryPage;


public class TransactionHistoryModule {
	
	TransactionHistoryPage transactionHistoryPage;
	
	public TransactionHistoryModule() {
		transactionHistoryPage = new TransactionHistoryPage();
	}
	
	public void clickBalanceAndFDAmount() {
		transactionHistoryPage.clickViewBalanceLink();
		transactionHistoryPage.clickViewFDAmountLink();
	}
	
	public void viewBalanceAndFDAmount() {
		transactionHistoryPage.assertSavingsAccountBalanceLabel("₹");
		transactionHistoryPage.assertFDAmountLabel("₹");
	}
	
	public void clickFilterButton() {
		transactionHistoryPage.clickFilterButton();
	}
	
	public void reloadTransactionHistoryPage() throws InterruptedException {
		transactionHistoryPage.refresh();
	}
	
	public void verifyAccountDetails(String fullName, String customerID, String mobileNumber, String emailID) {
		transactionHistoryPage.assertFullNameLabelText(fullName);
		transactionHistoryPage.assertCustomerIDLabelText(customerID);
		transactionHistoryPage.assertMobileNumberLabelText(mobileNumber);
		transactionHistoryPage.assertEmailIDLabelText(emailID);
	}
	
	public void assertTransactionTableDoesNotContain(String text) {
		transactionHistoryPage.assertTransactionsTableDoesNotContainText(0, 0, "BODY", text);		
	}
		
	public void assertTransactionTableContains(String text) {
		transactionHistoryPage.assertTransactionsTableContainsText(0, 0, "BODY", text);		
	}
	
	public void verifyTransactionTableHeaderForGeneralSearch(String id, String serialNo, String date, String remark, String amount, String mode, 
			String externalId, String status) {
		transactionHistoryPage.assertTransactionsTableText(0, 0, "header", id);
		transactionHistoryPage.assertTransactionsTableText(0, 1, "header", serialNo);
		transactionHistoryPage.assertTransactionsTableText(0, 2, "header", date);
		transactionHistoryPage.assertTransactionsTableText(0, 3, "header", remark);
		transactionHistoryPage.assertTransactionsTableText(0, 4, "header", amount);
		transactionHistoryPage.assertTransactionsTableText(0, 5, "header", mode);
		transactionHistoryPage.assertTransactionsTableText(0, 6, "header", externalId);
		transactionHistoryPage.assertTransactionsTableText(0, 7, "header", status);
	}
	
	public void verifyTransactionTableHeaderForRRNSearch(String customerName, String customerID, String id, String serialNo, 
			String date, String remark, String amount, String mode, String externalId, String status) {
		transactionHistoryPage.assertTransactionsTableText(0, 0, "header", customerName);
		transactionHistoryPage.assertTransactionsTableText(0, 1, "header", customerID);
		transactionHistoryPage.assertTransactionsTableText(0, 2, "header", id);
		transactionHistoryPage.assertTransactionsTableText(0, 3, "header", serialNo);
		transactionHistoryPage.assertTransactionsTableText(0, 4, "header", date);
		transactionHistoryPage.assertTransactionsTableText(0, 5, "header", remark);
		transactionHistoryPage.assertTransactionsTableText(0, 6, "header", amount);
		transactionHistoryPage.assertTransactionsTableText(0, 7, "header", mode);
		transactionHistoryPage.assertTransactionsTableText(0, 8, "header", externalId);
		transactionHistoryPage.assertTransactionsTableText(0, 9, "header", status);
	}
	
	public void verifyTransactionTableHeaderForTransactionIDSearch(String accountID, String id, String serialNo, 
			String date, String remark, String amount, String mode, String externalId, String status) {
		transactionHistoryPage.assertTransactionsTableText(0, 0, "header", accountID);
		transactionHistoryPage.assertTransactionsTableText(0, 1, "header", id);
		transactionHistoryPage.assertTransactionsTableText(0, 2, "header", serialNo);
		transactionHistoryPage.assertTransactionsTableText(0, 3, "header", date);
		transactionHistoryPage.assertTransactionsTableText(0, 4, "header", remark);
		transactionHistoryPage.assertTransactionsTableText(0, 5, "header", amount);
		transactionHistoryPage.assertTransactionsTableText(0, 6, "header", mode);
		transactionHistoryPage.assertTransactionsTableText(0, 7, "header", externalId);
		transactionHistoryPage.assertTransactionsTableText(0, 8, "header", status);
	}
	
	public void clickAdvanceSearch() {
		transactionHistoryPage.clickAdvanceSearchLink();
	}
	
	public void verifyTransactionDateHeader(String text) {
		transactionHistoryPage.assertTransactionDateLabelText(text);
	}
	
	public void verifyTransactionTypesSection(String transactionType, String all, String credit, String debit) {
		transactionHistoryPage.assertTransactionTypesLabelText(transactionType);
		transactionHistoryPage.assertTransactionType_AllButtonText(all);
		transactionHistoryPage.assertTransactionType_CreditButtonText(credit);
		transactionHistoryPage.assertTransactionType_DebitButtonText(debit);
		transactionHistoryPage.selectTransactionType_CreditButton();		
		transactionHistoryPage.selectTransactionType_DebitButton();	
		transactionHistoryPage.selectTransactionType_AllButton();
	}
	
	public void verifyTransactionModesSection(String transactionModes, String debitCard, String netBanking, String imps, String neft,  
			String rtgs, String upi, String slfd, String bcApp, String pos, String atmRecycler, String nach, String apy) {
		transactionHistoryPage.assertTransactionModesLabelText(transactionModes);
		selectTransactionModes("ALL FILTERS");
		transactionHistoryPage.assertTransactionModes_DebitCardButtonText(debitCard);
		transactionHistoryPage.assertTransactionModes_NetBankingButtonText(netBanking);
		transactionHistoryPage.assertTransactionModes_IMPSButtonText(imps);
		transactionHistoryPage.assertTransactionModes_NEFTButtonText(neft);
		transactionHistoryPage.assertTransactionModes_RTGSButtonText(rtgs);
		transactionHistoryPage.assertTransactionModes_UPIButtonText(upi);
		transactionHistoryPage.assertTransactionModes_SLFDButtonText(slfd);
		transactionHistoryPage.assertTransactionModes_BCAppButtonText(bcApp);
		transactionHistoryPage.assertTransactionModes_POSButtonText(pos);
		transactionHistoryPage.assertTransactionModes_ATMRecyclerButtonText(atmRecycler);
		transactionHistoryPage.assertTransactionModes_NACHButtonText(nach);
		transactionHistoryPage.assertTransactionModes_APYButtonText(apy);
	}
	
	public void selectTransactionModes(String filter) {
		switch(filter.toUpperCase()) {
		case "DEBIT CARD" : 
			transactionHistoryPage.selectTransactionModes_DebitCardButton();
			break;
		case "NET BANKING" : 
			transactionHistoryPage.selectTransactionModes_NetBankingButton();
			break;
		case "IMPS" : 
			transactionHistoryPage.selectTransactionModes_IMPSButton();
			break;
		case "NEFT" : 
			transactionHistoryPage.selectTransactionModes_NEFTButton();
			break;
		case "RTGS" : 
			transactionHistoryPage.selectTransactionModes_RTGSButton();
			break;
		case "UPI" : 
			transactionHistoryPage.selectTransactionModes_UPIButton();
			break;
		case "SLFD" : 
			transactionHistoryPage.selectTransactionModes_SLFDButton();
			break;
		case "BC APP" : 
			transactionHistoryPage.selectTransactionModes_BCAppButton();
			break;
		case "POS" : 
			transactionHistoryPage.selectTransactionModes_POSButton();
			break;
		case "ATM/RECYCLER" : 
			transactionHistoryPage.selectTransactionModes_ATMRecyclerButton();
			break;
		case "NACH" : 
			transactionHistoryPage.selectTransactionModes_NACHButton();
			break;
		case "APY" : 
			transactionHistoryPage.selectTransactionModes_APYButton();
			break;
		case "ALL FILTERS" :
			transactionHistoryPage.selectTransactionModes_DebitCardButton();
			transactionHistoryPage.selectTransactionModes_NetBankingButton();
			transactionHistoryPage.selectTransactionModes_IMPSButton();
			transactionHistoryPage.selectTransactionModes_NEFTButton();
			transactionHistoryPage.selectTransactionModes_RTGSButton();
			transactionHistoryPage.selectTransactionModes_UPIButton();
			transactionHistoryPage.selectTransactionModes_SLFDButton();
			transactionHistoryPage.selectTransactionModes_BCAppButton();
			transactionHistoryPage.selectTransactionModes_POSButton();
			transactionHistoryPage.selectTransactionModes_ATMRecyclerButton();
			transactionHistoryPage.selectTransactionModes_NACHButton();
			transactionHistoryPage.selectTransactionModes_APYButton();
			break;
		default : 
			throw new InvalidArgumentException("Invalid filter: " +filter);			
		}
	}
	
	public void verifyAdHocReportCodesSection(String adHocReportCodes, String serviceTax, String tds, String gst, String interestReceived,
			String neftIncome, String rtgsCharges, String salary, String flexiSalary, String reimbursement) {
		transactionHistoryPage.assertAdHocReportCodesLabelText(adHocReportCodes);
		selectAdHocReportCodes("ALL FILTERS");
		transactionHistoryPage.assertAdHocReportCodes_ServiceTaxButtonText(serviceTax);
		transactionHistoryPage.assertAdHocReportCodes_TDSButtonText(tds);
		transactionHistoryPage.assertAdHocReportCodes_GSTButtonText(gst);
		transactionHistoryPage.assertAdHocReportCodes_InterestReceivedButtonText(interestReceived);
		transactionHistoryPage.assertAdHocReportCodes_NEFTIncomeButtonText(neftIncome);
		transactionHistoryPage.assertAdHocReportCodes_RTGSChargesButtonText(rtgsCharges);
		transactionHistoryPage.assertAdHocReportCodes_SalaryButtonText(salary);
		transactionHistoryPage.assertAdHocReportCodes_FlexiSalaryButtonText(flexiSalary);
		transactionHistoryPage.assertAdHocReportCodes_ReimbursementButtonText(reimbursement);
	}
	
	public void selectAdHocReportCodes(String filter) {
		switch(filter.toUpperCase()) {
		case "SERVICE TAX" : 
			transactionHistoryPage.selectAdHocReportCodes_ServiceTaxButton();
			break;
		case "TDS" : 
			transactionHistoryPage.selectAdHocReportCodes_TDSButton();
			break;
		case "GST" : 
			transactionHistoryPage.selectAdHocReportCodes_GSTButton();
			break;
		case "INTEREST RECEIVED" : 
			transactionHistoryPage.selectAdHocReportCodes_InterestReceivedButton();
			break;
		case "NEFT INCOME" : 
			transactionHistoryPage.selectAdHocReportCodes_NEFTIncomeButton();
			break;
		case "RTGS CHARGES" : 
			transactionHistoryPage.selectAdHocReportCodes_RTGSChargesButton();
			break;
		case "SALARY" : 
			transactionHistoryPage.selectAdHocReportCodes_SalaryButton();
			break;
		case "FLEXI SALARY" : 
			transactionHistoryPage.selectAdHocReportCodes_FlexiSalaryButton();
			break;
		case "REIMBURSEMENT" : 
			transactionHistoryPage.selectAdHocReportCodes_ReimbursementButton();
			break;
		case "ALL FILTERS" :
			transactionHistoryPage.selectAdHocReportCodes_ServiceTaxButton(); 
			transactionHistoryPage.selectAdHocReportCodes_TDSButton();
			transactionHistoryPage.selectAdHocReportCodes_GSTButton();
			transactionHistoryPage.selectAdHocReportCodes_InterestReceivedButton();
			transactionHistoryPage.selectAdHocReportCodes_NEFTIncomeButton();
			transactionHistoryPage.selectAdHocReportCodes_RTGSChargesButton();
			transactionHistoryPage.selectAdHocReportCodes_SalaryButton();
			transactionHistoryPage.selectAdHocReportCodes_FlexiSalaryButton();
			transactionHistoryPage.selectAdHocReportCodes_ReimbursementButton();
			break;
		default : 
			throw new InvalidArgumentException("Invalid filter: " +filter);			
		}
	}
	
	public void clickRearrangeTableColumns() {
		transactionHistoryPage.clickRearrangeTableColumnsButton();
	}
	
	public void verifyRearrangeTableColumnsSection(String id, String serialNo, String date, String remark, String amount, String mode, 
			String externalID, String status, String errorText) {
		transactionHistoryPage.clickRearrangeTableColumnsButton();
		
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(id);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(serialNo);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(date);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(remark);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(amount);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(mode);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(externalID);
		transactionHistoryPage.assertRearrangeTableColumnsOptionsText(status);
		
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(id);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(serialNo);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(date);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(remark);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(amount);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(mode);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(externalID);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(status);
		
		transactionHistoryPage.assertRearrangeTableColumnsErrorLabelText(errorText);
		transactionHistoryPage.clickRearrangeTableColumnsResetButton();
		transactionHistoryPage.clickRearrangeTableColumnsApplyButton();		
	}
	
	public void verifyRearrangeTableFewColumnsSection(String id, String serialNo, String date, String remark) {
		transactionHistoryPage.clickRearrangeTableColumnsButton();
		
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(id);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(serialNo);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(date);
		transactionHistoryPage.clickRearrangeTableColumnsOptionsByText(remark);
		
		transactionHistoryPage.clickRearrangeTableColumnsApplyButton();
	}
	
	public void verifyTransactionTableHeader(String amount, String mode, String externalID, String status) {
		transactionHistoryPage.assertTransactionsTableText(0, 0, "header", amount);
		transactionHistoryPage.assertTransactionsTableText(0, 1, "header", mode);
		transactionHistoryPage.assertTransactionsTableText(0, 2, "header", externalID);
		transactionHistoryPage.assertTransactionsTableText(0, 3, "header", status);
	}
	
	public void assertDownloadButtonIsNotPresent() {		
		transactionHistoryPage.assertDownloadButtonIsNotPresent();
	}
	
	public void assertDownloadButtonIsPresent() {		
		transactionHistoryPage.assertDownloadButtonIsPresent();
	}
	
	public void verifyModeOfTransactionsInColumn(int colIdx, String text) {
		for(int i=0; i<transactionHistoryPage.getTransactionsTableRowCount(); i++) {
			transactionHistoryPage.assertTransactionsTableContainsText(i, colIdx, "BODY", text);
		}		
	}
	
	public void clickTransactionID(int rowIdx, int colIdx) {
		transactionHistoryPage.clickTransactionTableCell(rowIdx, colIdx, "BODY");
	}
	
	public void verifyTransactionDetails(String transactionDetailsHeader, String id, String postedDate, String valueDate,
			String description, String rrn, String utr, String cifID, String cbsPartTranType, String reportCode, String userDetailsHeader,
			String remitterName, String remitterAccountNumber, String referenceNumber, String remitterIFSC, String beneficiaryIFSC, 
			String beneficiaryAccountNumber, String beneficiaryName, String merchantDetailsHeader, String merchantName, String merchantID,
			String remittanceMMID, String dccID, String merchantLocation, String dcMerchantID, String idValue, String postedDateValue, 
			String valueDateValue, String descriptionValue, String rrnValue, String utrValue, String cifIDValue, String cbsPartTranTypeValue, 
			String reportCodeValue, String remitterNameValue, String remitterAccountNumberValue, String referenceNumberValue, 
			String remitterIFSCValue, String beneficiaryIFSCValue, String beneficiaryAccountNumberValue, String beneficiaryNameValue, 
			String merchantNameValue, String merchantIDValue, String remittanceMMIDValue, String dccIDValue, String merchantLocationValue, 
			String dcMerchantIDValue, String ok) {
	
		transactionHistoryPage.assertTransaction_TransactionDetailsHeaderLabelText(transactionDetailsHeader);		
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(0, id);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(0, idValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(1, postedDate);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(1, postedDateValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(2, valueDate);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(2, valueDateValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(3, description);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(3, descriptionValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(4, rrn);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(4, rrnValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(5, utr);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(5, utrValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(6, cifID);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(6, cifIDValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(7, cbsPartTranType);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(7, cbsPartTranTypeValue);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(8, reportCode);
		transactionHistoryPage.assertTransaction_TransactionDetailsElementListElementContainsText(8, reportCodeValue);
		
		transactionHistoryPage.assertTransaction_UserDetailsHeaderLabelText(userDetailsHeader);	
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(0, remitterName);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(0, remitterNameValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(1, remitterAccountNumber);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(1, remitterAccountNumberValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(2, referenceNumber);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(2, referenceNumberValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(3, remitterIFSC);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(3, remitterIFSCValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(4, beneficiaryIFSC);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(4, beneficiaryIFSCValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(5, beneficiaryAccountNumber);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(5, beneficiaryAccountNumberValue);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(6, beneficiaryName);
		transactionHistoryPage.assertTransaction_UserDetailsElementListElementContainsText(6, beneficiaryNameValue);
		
		transactionHistoryPage.assertTransaction_MerchantDetailsHeaderLabelText(merchantDetailsHeader);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(0, merchantName);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(0, merchantNameValue);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(1, merchantID);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(1, merchantIDValue);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(2, remittanceMMID);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(2, remittanceMMIDValue);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(3, dccID);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(3, dccIDValue);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(4, merchantLocation);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(4, merchantLocationValue);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(5, dcMerchantID);
		transactionHistoryPage.assertTransaction_MerchantDetailsElementListElementContainsText(5, dcMerchantIDValue);
		
		transactionHistoryPage.assertTransaction_OkButtonText(ok);
		transactionHistoryPage.clickTransaction_OkButton();	
	}
	
	public void clickViewAmount() {
		transactionHistoryPage.clickTransactionTableCell(0, 5, "BODY");
	}
	
	public void checkViewAmount() {
		transactionHistoryPage.assertTransactionsTableContainsText(0, 5, "BODY", "₹");
		String text = transactionHistoryPage.getTransactionsTableCellText(0, 5, "BODY");
		if((text.contains("Cr.")) | (text.contains("Dr."))) {
			Assertions.assertThat(true);
		} else {
			Assertions.assertThat(false);
			}
	}
	
	public void verifyTransactionsTableAmountColumn(String text) {
		for(int i=0; i<transactionHistoryPage.getTransactionsTableRowCount(); i++) {
			transactionHistoryPage.assertTransactionsTableText(i, 4, "BODY", text);
		}
	}
	
	public int getTransactionsTableRowCount() {
		return transactionHistoryPage.getTransactionsTableRowCount();
	}
	
	public int getTransactions_TotalNoOfSearchResults() {		
		String text = transactionHistoryPage.getTransactions_ResultsFoundLabelText();
		return Integer.parseInt(text.substring(0, text.indexOf(" ")));
	}
	
	public int getFailedPendingTransactions_TotalNoOfSearchResults() {		
		String text = transactionHistoryPage.getFailedPendingTransactions_ResultsFoundLabelText();
		return Integer.parseInt(text.substring(0, text.indexOf(" ")));
	}
	
	public void verifyFailedPendingTransactions_ResultsFoundLabelText() {
		transactionHistoryPage.assertFailedPendingTransactions_ResultsFoundLabelText(transactionHistoryPage.getTransactionsTableRowCount() + " Results Found");
	}
	
	public void clickOnTransactionsTab() {
		transactionHistoryPage.clickTransactionsTabButton();
	}
	
	public void clickOnFailedPendingTransactionsTab() {
		transactionHistoryPage.clickFailedPendingTransactionsTabButton();
	}
	
	public void verifyFailedPendingTransactionTableHeader(String customerId, String externalID, String internalID, String transactionType, 
			String subTransactionType, String amount, String responseCode, String transactionStatus, String creationDate, String modifiedDate, 
			String settledDate, String otherDetails) {
		transactionHistoryPage.assertTransactionsTableText(0, 1, "header", customerId);
		transactionHistoryPage.assertTransactionsTableText(0, 2, "header", externalID);
		transactionHistoryPage.assertTransactionsTableText(0, 3, "header", internalID);
		transactionHistoryPage.assertTransactionsTableText(0, 4, "header", transactionType);
		transactionHistoryPage.assertTransactionsTableText(0, 5, "header", subTransactionType);
		transactionHistoryPage.assertTransactionsTableText(0, 6, "header", amount);
		transactionHistoryPage.assertTransactionsTableText(0, 7, "header", responseCode);
		transactionHistoryPage.assertTransactionsTableText(0, 8, "header", transactionStatus);
		transactionHistoryPage.assertTransactionsTableText(0, 9, "header", creationDate);
		transactionHistoryPage.assertTransactionsTableText(0, 10, "header", modifiedDate);
		transactionHistoryPage.assertTransactionsTableText(0, 11, "header", settledDate);
		transactionHistoryPage.assertTransactionsTableText(0, 12, "header", otherDetails);
	}
	
	public void checkFailedPendinTransactionsAmount() {
		for(int i=0; i<transactionHistoryPage.getTransactionsTableRowCount(); i++) {
			transactionHistoryPage.assertTransactionsTableContainsText(i, 6, "BODY", "₹");
		}
		
		
	}

	
}
