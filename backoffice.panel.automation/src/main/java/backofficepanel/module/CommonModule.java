package backofficepanel.module;

<<<<<<< HEAD
import backofficepanel.pages.CommonPage;
=======

import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import backofficepanel.pages.CommonPage;
import backofficepanel.utilities.Utility;
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282

public class CommonModule {
	
	CommonPage commonPage;
	
	public CommonModule() {
		commonPage = new CommonPage();
	}
	
	public String confirmLoginStatus() {		
		if(commonPage.getLogout().size() == 0) return "Invalid";
		else return "Valid";
	}
	public void logout() {
		commonPage.getLogout();
		commonPage.clicklogoutButton();
	}
	
<<<<<<< HEAD
	public void paginationForward()
	{
		commonPage.paginationForward();
	}

	
	public void paginationBackward()
	{
		commonPage.paginationBackward();
	}
	
	public void navigateToAccountmanagement() 
	{
		commonPage.goToAccountmanagement();
	}

	public void clickDropdown() 
	{
		System.out.println("Opening dropdown");
		commonPage.openDropdown();
	}
	
	
	public void verifyErrorMessage(String error)
	{
		commonPage.verifyErrorMessage(error);
	}

	
	public void search(String searchBy , String value)   
	{
		commonPage.openDropdown();
	      switch(searchBy) {
	         case "Customer ID" :
	        	 commonPage.selectCustomerId();
	        	 break;
	         case "Account Number" :
	        	 commonPage.selectAccountNumber();
	        	 break;
	         case "Mobile Number" :
	        	 commonPage.selectMobileNumber();
	        	 break;
	         case "Email ID" :
	        	 commonPage.selectEmailid();
	        	 break;
	         case "RRN" :
	        	 commonPage.selectRRN();
	            break;
	      }
	      System.out.println("Printing just before value : "+value+" & Search By : "+searchBy);
	      commonPage.typeInsearchBox(value);
	      commonPage.enterSearchBar();
	   }
	
	
=======
	public void clickTransactionHistoryLink() {
		commonPage.clickTransactionHistoryLink();
		commonPage.refresh();
	}
	
	public void clickAccountManagementLink() {
		commonPage.clickAccountManagementLink();
		commonPage.refresh();
	}
	
	public void clickReconcillationLink() {
		commonPage.clickReconcillationLink();
		commonPage.refresh();
	}
	
	public void clickUploadsLink() {
		commonPage.clickUploadsLink();
		commonPage.refresh();
	}
	
	public void clickclickPendingProductsLink() {
		commonPage.clickPendingProductsLink();
		commonPage.refresh();
	}
	
	public void clickclickcentralOperationsLink() {
		commonPage.clickcentralOperationsLink();
		commonPage.refresh();
	}
	
	public void reloadPage() {
		commonPage.refresh();
	}
	
	public void clickDropdown() {
		commonPage.openDropdown();
	}
	
	public void searchBy(String searchBy , String value)   
	{
		commonPage.openDropdown();
	    switch(searchBy) {
	    	case "Customer ID" :
	    		commonPage.selectCustomerId();	        
	        	break;
	        case "Account Number" :
	        	commonPage.selectAccountNumber();	       
	        	break;
	        case "Mobile Number" :
	        	commonPage.selectMobileNumber();
	        	break;	   	      
	        case "Email ID" :
	        	commonPage.selectEmailid();
	        	break;
	        case "RRN" :
	        	commonPage.selectRRN();
	        	break;
	        default : throw new InvalidArgumentException("Invalid keyword: " +searchBy);
	    }
	    commonPage.typeInSearchBox(value);
	    commonPage.typeInSearchBox(Keys.ENTER);	      
	}
	
	public void searchByTransactionID(String transactionID , String serialNo, String date) {
		commonPage.openDropdown();
		commonPage.selectTransactionID();
		commonPage.typeInTranIDTextBox(transactionID);
		commonPage.typeInSerialNoTextBox(serialNo);
		if(!((date == null) | (date.length() == 0))) {
			String day = Utility.getDay(date);
			String monthYear = Utility.getMonthYear(date);
			setSingleDateFilter(monthYear, day);
		}
		commonPage.typeInTranIDTextBox(Keys.ENTER);
	}
	
	public void setSingleDateFilter(String monthYear, String date) {
		commonPage.clickDateFilterButton();	
		
		long nextTick = System.currentTimeMillis() + 1000 ;
			
		while (!commonPage.getCurrentMonthLabelText().equalsIgnoreCase(monthYear)) {    
		    if (System.currentTimeMillis() >= nextTick) {
		    	commonPage.clickPreviousMonthButton();		    	
		        nextTick += 1000;     
		        }
		    }
	
		if(commonPage.getCurrentMonthLabelText().equalsIgnoreCase(monthYear)) {
			commonPage.clickCurrentMonthDatesElementListElement(date);
		}
	}
	
	public void setDateFilter(String startDate, String endDate) {
		String startDay = Utility.getDay(startDate);
		String startMonthYear = Utility.getMonthYear(startDate);
		String endDay = Utility.getDay(endDate);
		String endMonthYear = Utility.getMonthYear(endDate);
		
		commonPage.clickDateFilterButton();	
		
		long nextTick = System.currentTimeMillis() + 1000 ;
			
		while (!commonPage.getCurrentMonthLabelText().equalsIgnoreCase(startMonthYear)) {    
		    if (System.currentTimeMillis() >= nextTick) {
		    	commonPage.clickPreviousMonthButton();		    	
		        nextTick += 1000;     
		        }
		}
	
		if(commonPage.getCurrentMonthLabelText().equalsIgnoreCase(startMonthYear)) {
			commonPage.clickCurrentMonthDatesElementListElement(startDay);
		} 
		
		long nexttTick = System.currentTimeMillis() + 1000 ;
		
		while (!commonPage.getCurrentMonthLabelText().equalsIgnoreCase(endMonthYear)) {    
		    if (System.currentTimeMillis() >= nexttTick) {
		    	commonPage.clickNextMonthButton();		    	
		    	nexttTick += 1000;     
		        }
		}
		
		if(commonPage.getCurrentMonthLabelText().equalsIgnoreCase(endMonthYear)) {
			commonPage.clickCurrentMonthDatesElementListElement(endDay);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void NavigateToTransactionHistoryPage() {
		commonPage.setPageURLAsTransactionHistoryPage();	
		commonPage.launch();
	}
	
	public void verifyErrorMessage(String text) {
		commonPage.assertErrorMessageLabelText(text);
	}
	
	
	public void selectSearchBy(String searchBy)   
	{
		commonPage.openDropdown();
	    switch(searchBy) {
	    	case "Customer ID" :
	    		commonPage.selectCustomerId();	        
	        	break;
	        case "Account Number" :
	        	commonPage.selectAccountNumber();	       
	        	break;
	        case "Mobile Number" :
	        	commonPage.selectMobileNumber();
	        	break;	   	      
	        case "Email ID" :
	        	commonPage.selectEmailid();
	        	break;
	        case "RRN" :
	        	commonPage.selectRRN();
	        	break;
	        case "Transaction ID" :
	        	commonPage.selectTransactionID();
	        	break;
	        default : throw new InvalidArgumentException("Invalid keyword: " +searchBy);
	    }      
	}
	
	public void checkAdvanceSearchIsVisible(String text) {
		commonPage.assertAdvanceSearchLinkText(text);	
	}
	
	public void checkSearchBarPlaceholderText(String text) {
		commonPage.assertSearchbarTextBoxAttributeValue("placeholder", text);		
	}
	
	public void checkTransactionIDSearchBarsPlaceholdersText(String transactionIDPlaceholder, String serialNoPlaceholder, String datePlaceholder) {
		commonPage.assertTranIDTextBoxAttributeValue("placeholder", transactionIDPlaceholder);	
		commonPage.assertserialNoTextBoxAttributeValue("placeholder", serialNoPlaceholder);
		commonPage.assertSelectDateButtonAttributeValue("placeholder", datePlaceholder);
	}
	
	public void verifyDateFilter(String startDate, String endDate) {
		setDateFilter(startDate, endDate);
		commonPage.assertStartDateLabelText(startDate);
		commonPage.assertEndDateLabelText(endDate);
	}
	
	public void waitForLoaderToDisappear() {
		commonPage.waitForLoaderToDisappear();
	}
	
	public void verifyPageNo(String pageNo) {
		commonPage.assertPagination_PageNoLabelText(pageNo);
	}
	
	public void clickNextPage() {
		commonPage.clickPagination_NextPageButton();
	}
	
	public void clickPreviousPage() {
		commonPage.clickPagination_PreviousPageButton();
	}
	
	public void assertTotalPagesCount(int no) {
		commonPage.assertPagination_TotalPagesLabelTextContains(String.valueOf(no));
	}
	
	public void clearSearchBoxAndType(String text) {
		commonPage.clearSearchBox();
		commonPage.typeInSearchBox(text);
		commonPage.typeInSearchBox(Keys.ENTER);	   
	}
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	
	public void cancelSearch() {
		commonPage.clickSearchCancelButton();
	}
<<<<<<< HEAD
=======
	
	
	
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
}
