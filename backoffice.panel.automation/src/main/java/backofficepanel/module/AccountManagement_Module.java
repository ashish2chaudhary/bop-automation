package backofficepanel.module;

import com.paytm.framework.ui.element.Button;

import backofficepanel.pages.AccountManagement_Page;
import backofficepanel.pages.LoginPage;

public class AccountManagement_Module {
	AccountManagement_Page ACMAN_Page ;
	private Button loginButton;
	public AccountManagement_Module() {
		ACMAN_Page = new AccountManagement_Page();
	}
	public void verifyAccountInfo(String custid ,  String mobile , String emailid) {		
		ACMAN_Page.verifyCustomerDetails(custid,mobile,emailid);
		ACMAN_Page.verifyHeaderDetails();
		ACMAN_Page.clickSavingAccountTab();
		ACMAN_Page.clickKYCDetailsTab();
		ACMAN_Page.clickDebitCardTab();
		ACMAN_Page.clickFixedDepositTab();
    }
	
<<<<<<< HEAD
	public void clickAndCheckFDBalance(String fdbalance)
	{
		ACMAN_Page.clickAndCheckFDBalance(fdbalance);
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void click_less_details()
	{
		ACMAN_Page.click_less_details();
=======
	public void verifyFixedDepositTable()
	{
		ACMAN_Page.verifyFixedDepoTable();
	}
	
	public void verifyPreDelStatus(String predelstatus)
	{
		ACMAN_Page.verifyPreDelStatus(predelstatus);
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	}
	
	public void clickMoreDetails()
	{
		ACMAN_Page.clickMoreDetails();
	}
	
<<<<<<< HEAD
	public void clickSavingAccountTab()
	{
		ACMAN_Page.clickSavingAccountTab();
	}
	
	public void clickFixedDepositTab()
	{
		ACMAN_Page.clickFixedDepositTab();
	}
	
	public void clickDebitCardTab()
	{
		ACMAN_Page.clickDebitCardTab();
	}
	
	public void clickKYCDetailsTab()
	{
		ACMAN_Page.clickKYCDetailsTab();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void openAllpanel()
	{
		clickDebitCardTab();
		pause(3);
		clickMoreDetails();
		pause(3);
		clickSavingAccountTab();
		pause(3);
		clickFixedDepositTab();	
		pause(3);
//		clickKYCDetailsTab();
//		ACMAN_Page.waitLoaderToAppear();
//		ACMAN_Page.waitLoaderToDisppear();
	}
	
	public void verifyAllPanelAreClosed()
	{
		ACMAN_Page.checkISATabClosed();
		ACMAN_Page.verifyVDCNotPresent();
		ACMAN_Page.verifyPDCNotPresent();
		ACMAN_Page.verifyKYCNotPresent();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	public void noFDMessage()
	{
		ACMAN_Page.noFDMessage();
	}
	
	public void clickBlockHist()
	{
		ACMAN_Page.clickBlockHist();
	}
	
	public void verifyHeaderBlockHistTable()
	{
		ACMAN_Page.verifyHeaderBlockHistTable();
	}
	
	public void presenceOfCardBlockHistTable()
	{
		ACMAN_Page.presenceOfCardBlockHistTable();
	}
	
	public void checkErrorMessage(String errormessage)
	{
		
	}
	public void verifyPDCnotPresent()
	{
		ACMAN_Page.verifyVDCNotPresent();
	}
	
	public void verifyFixedDepositTable()
	{
		ACMAN_Page.verifyFixedDepoTable();
	}
	
	public void verifyPreDelStatus(String predelstatus)
	{
		ACMAN_Page.verifyPreDelStatus(predelstatus);
	}
	
	public void verifyBasicInformation(String gender ,String dob , String marital_status, String nationality ,String occupation,
			 String empstatus ,String pan_card ,String address)
	{
		ACMAN_Page.verifyBasicInformation( gender , dob ,  marital_status,  nationality , occupation,
				  empstatus , pan_card , address);
	}
	
	public void verifyProofOfIdentity(String doctype , String issuanceCountry , String issuanceplace ,
			 String docnumber , String issuedate)
	{
		ACMAN_Page.verifyProofOfIdentity(doctype, issuanceCountry, issuanceplace, docnumber, issuedate);
=======
	public void verifyBasicInformation(String gender ,String dob , String marital_status, String nationality ,String occupation,
			 String empstatus ,String pan_card ,String address)
	{
		ACMAN_Page.verifyBasicInformation( gender , dob ,  marital_status,  nationality , occupation,
				  empstatus , pan_card , address);
	}
	
	public void verifyProofOfIdentity(String doctype , String issuanceCountry , String issuanceplace ,
			 String docnumber , String issuedate)
	{
		ACMAN_Page.verifyProofOfIdentity(doctype, issuanceCountry, issuanceplace, docnumber, issuedate);
	}
	
	public void clickSavingAccountTab()
	{
		ACMAN_Page.clickSavingAccountTab();
	}
	
	public void clickFixedDepositTab()
	{
		ACMAN_Page.clickFixedDepositTab();
	}
	
	public void clickDebitCardTab()
	{
		ACMAN_Page.clickDebitCardTab();
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	}
	
	public void selectTotalunfreeze()
	{
		ACMAN_Page.selectTotalunfreeze();
	}
	
	public void selectTotalFreeze()
	{
		ACMAN_Page.selectTotalFreeze();
	}
	
	public void selectDebitFreeze()
	{
		ACMAN_Page.selectDebitFreeze();
	}
	
	public void verifyingSavingAccountStatus(String status)
	{
		ACMAN_Page.verifyingSavingAccountStatus(status);
	}
	
	public void verifySAINFAcc_Details(String username , String custid ,String mobile , String emailid ,
			String AC_No,String bal ,String AC_Type,String status,
			String branchname , String branchid , String branch_add,String create_date,
			String update_date ,String see_deessed,
			String consent_stat,String NPCI_seed_date,String reject)
	{
		ACMAN_Page.clickSavingAccountTab();
		ACMAN_Page.verifySavingAccountHeader();
		ACMAN_Page.verifySavingCustDetails(username ,custid ,mobile ,emailid,AC_No,bal ,AC_Type, status,
				 branchname ,branchid , branch_add, create_date ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	}
	public void verifyNomineeDetail(String nomineename , String nominee_id , String relation , String email,
			String DOB , String aadhar , String percent , String address)
	{
		ACMAN_Page.clickSavingAccountTab();
		ACMAN_Page.verifyNomineeDetail(nomineename, nominee_id, relation, email, DOB, aadhar, percent , address);
	}
	public void verifyKYC_DetailsACMANMod(String kyc_stat, String kyc_type, String kyc_details, String pan_updated
			,String pan_verified ,String aadhar_updated, String aadhar_verified, String form_60) {
		ACMAN_Page.clickKYCDetailsTab();
		ACMAN_Page.verifyKYCDetailACMANPage(kyc_stat ,kyc_type ,kyc_details,pan_updated ,pan_verified ,aadhar_updated ,aadhar_verified ,form_60);
	}
	
	public void typeInCommentSection_DebitActions()
	{
		ACMAN_Page.typeInCommentSection();
	}
<<<<<<< HEAD
	
	
	public void verifycommentSectionISEmpty()
	{
		ACMAN_Page.verifycommentSectionISEmpty();
	}
	
	
=======
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
	public void clickOnSubmit_Debit_SavingActions()
	{
		ACMAN_Page.waitfor(5);
		ACMAN_Page.clickOnSubmit_Debit_SavingActions();
		ACMAN_Page.waitLoaderToAppear();
		ACMAN_Page.waitLoaderToDisppear();
	}
	
	public void selectUnblock()
	{
		ACMAN_Page.selectUnblock();
	}
	
	public void selectrestrictBlock()
	{
		ACMAN_Page.selectrestrictBlock();
	}
	
	public void selectrestrictUnblock()
	{
		ACMAN_Page.selectrestrictUnblock();
	}
	
	public void verifyVDCDetails(String debitcard_number,String alias_number ,String stat,String variant)
	{
		ACMAN_Page.clickDebitCardTab();
		ACMAN_Page.verifyingVDCDetails(debitcard_number , alias_number , stat , variant);
	}
	
	public void clickActionLink()
	{
		ACMAN_Page.clickActionLink();
	}
	
	public void openActionsDropdown()
	{
		ACMAN_Page.waitfor(5);
		ACMAN_Page.openActionsDropdown();
	}
	
	public void openReasonDropdown()
	{
		ACMAN_Page.waitfor(5);
		ACMAN_Page.openReasonDropDown();
	}
	
	public void pause(int time)
	{
		ACMAN_Page.waitfor(time);
	}
	public void tempBlockCard()
	{
		
	}
	public void verifyPDCDetails()
	{
		
	}
<<<<<<< HEAD
=======
	
>>>>>>> 41016343d241e09033c9cf56a13948d32b128282
}